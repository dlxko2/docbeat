var header =
`<li id="chapter` + chapterNumber + `">
 <div class="collapsible-header">
 <input type="hidden" name="iID[]" value="` + dId + `">
 <i class="material-icons" id="chapterIcon` + chapterNumber + `" style="color:red">font_download</i>
 <span id="name` + chapterNumber + `">` + dName + `</span>`;

var secHeader =
`<span class="secondary-content">
 <input type="checkbox" id="mood` + chapterNumber + `"/>
 <label for="mood` + chapterNumber + `">Hidden</label>
 <input type="hidden" name="iHidden[]" value="` + dHidden + `" id="iHidden` + chapterNumber + `">
 <i class="material-icons" onclick="aDeleteChapter(` + chapterNumber + `)">block</i>
 </span>
 </div>`;

 var secHeaderL =
`<span class="secondary-content">
 <input type="checkbox" id="mood` + chapterNumber + `"/>
 <label for="mood` + chapterNumber + `">Hidden</label>
 <input type="hidden" name="iHidden[]" value="` + dHidden + `" id="iHidden` + chapterNumber + `">
 </span>
 </div>`;

 var bodyName =
`<div class="collapsible-body">
 <div class="row chaptersNoBottomMargin">
 <div class="input-field col s12 chaptersNoTopMargin">
 <input placeholder="{ci_language line="Chapter name"}"
    id="iChapterName` + chapterNumber + `" type="text" class="validate"
    name="iChapterName[]" value="` + dName + `">
 </div></div>`;

 var bodyExpect =
`<div class="row">
 <div class="input-field col s6 chaptersNoTopMargin">
 <p class="range-field rangeMargin moveLeftMargin">
 <label for="iSliderWords` + chapterNumber + `">{ci_language line="Words"}</label><br>
 <input type="range" id="iSliderWords` + chapterNumber + `" min="0" max="5000"
    name="iSliderWords[]" value="` + dWords + `"/>
 </p>
 </div>
 <div class="input-field col s6 chaptersNoTopMargin">
 <p class="range-field rangeMargin">
 <label for="iSliderCites` + chapterNumber + `">{ci_language line="Cites"}</label><br>
 <input type="range" id="iSliderCites` + chapterNumber + `" min="0" max="30"
 name="iSliderCites[]" value="` + dCites + `"/>
 </p>
 </div>`;

 var bodyExtra =
`<div class="row noBottomMargin">
 <div class="input-field col s6 chaptersNoTopMargin">
 <input id="iMetricName` + chapterNumber + `" placeholder="{ci_language line="Metric"}"
    type="text" class="validate" name="iMetricName[]" value="` + dExtra + `">
 </div>
 <div class="input-field col s6 " style="margin-top: 7px">
 <p class="range-field rangeMargin">
 <label for="iSliderMetric` + chapterNumber + `">{ci_language line="How many"}</label><br>
 <input type="range" id="iSliderMetric` + chapterNumber + `" min="0" max="50"
    name="iSliderMetric[]" value="` + dExtraValue + `"/>
 </p></div></div>`;

 var bodyContent =
`<div class="row noBottomMargin">
 <div class="input-field col s12 chaptersNoTopMargin">
 <textarea placeholder="{ci_language line="Content"}"
    id="iAreaContent` + chapterNumber + `" class="materialize-textarea chaptersNoPadding"
    name="iAreaContent[]">` + dContent + `</textarea>
 </div>
 </div>
 </div>
 </div>
 </li>`;