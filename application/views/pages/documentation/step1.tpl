<div id="step1">

    <!-- NAME -->
    <div class="row">
        <div class="input-field col s12">
            <input type="hidden" name="iProjectID" value="{$project.id}" id="iProjectID">
            <input id="iProjectName" type="text" class="validate" name="iProjectName" value="{$project.name}">
            <label for="iProjectName">{ci_language line="Project name"}</label>
        </div>
    </div>

    <!-- DESCRIPTION -->
    <div class="row">
        <div class="input-field col s12">
            <input id="iProjectDescription" type="text" class="validate"
                   name="iProjectDescription" value="{$project.description}">
            <label for="iProjectDescription">{ci_language line="Project description"}</label>
        </div>
    </div>

    <!-- FILES -->
    <div class="file-field input-field">
        <div class="btn fileMargin">
            <span>{ci_language line="File"}</span>
            <input type="file" name="iProjectFiles[]" multiple>
        </div>
        <div class="file-path-wrapper">
        {if $files|count_characters > 0}
            <input class="file-path validate" type="text" placeholder="{$files}">
        {else}
            <input class="file-path validate" type="text" placeholder="{ci_language line="Upload one or more files"}">
        {/if}
        </div>
    </div><br>

    <!-- PROJECT START -->
    <div style="margin-top: 10px; margin-left: 12px;">
        <label for="projectStart">{ci_language line="Project start date"}</label>
        <input name="iProjectStart" type="date" class="datepicker" id="projectStart" value="{$project.started}">
    </div><br>

    <!-- VISIBILITY -->
    <div id="visibilityBox">
        <input type="checkbox" class="filled-in" id="iProjectVisible" {if $project.visible == 1}checked="checked"{/if}
               name="iProjectVisible" {if $project.locked == 1}disabled="disabled"{/if}/>
        <label for="iProjectVisible">{ci_language line="Visible by default"}</label>
    </div>
</div>
