<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset={$charset}" />
    <title>{$subject}</title>

    <style type="text/css">
        body
        {
            word-break: break-all;
            font-family: HelveticaNeue-Light, Helvetica Neue, Helvetica, Arial, sans-serif;
            color: #333;
            white-space: normal;
            font-weight: 300;
            padding-top: 18px;
            font-size: 15px;
            line-height: 20px;
        }
    </style>
</head>
<body>
<p>Dobý deň</p>
<p>Boli ste zaregistrovaný do služby <b>ProjectCreator</b></p>
<p>Vaše prihlasovacie meno je <b>{$login}</b> a heslo <b>{$password}</b></p>
</body>
</html>