/**
 * Function to open setting modal
 */
function openSettings() {
	$('#mSettings').openModal();
	$('#languageSelector').material_select();
}

/**
 * Function to send settings dialog
 */
function aSendSettings(index, text) {

	var language = $('input[name=language]:checked').val();
	var visibility = $('input[name=visibility]:checked').val();

	if (language == null) language = '';
	if (visibility == null) visibility = '';

	var url = index + '/welcome/settings/';

	$.post( url, { language: language, visibility: visibility }).done(function( data ) {
		if (data == 'OK') {
			alert(text);
			$('#mSettings').closeModal();
		}
	});
}

/**
 * Lock current project settings
 * @param index - string : path to index
 * @param chapterNumber - int : number of chapters
 */
function aLock(index) {

	// While locked allways allow unlock
	var lockStat = $('#docLockVal').val();

	$.ajax
	({
		url: index + '/documentation/lock',
		dataType: 'json',
		type: 'GET',
		error: function() {alert("Can't modify the lock!")},
		success: function(data)
		{
			// If lock failed
			if (data.result == false) alert("Can't lock the project!");

			// If locked
			if (data.state == true) {
				$('#documentationLock').css('color', 'red');
				$('#docLockVal').val(1);
			}

			// If not locked
			else {
				$('#documentationLock').css('color', 'white');
				$('#docLockVal').val(0);
			}
		}
	});
}

/**
 * Function to update the lock color
 */
function updateLock() {

	if ($('#docLockVal').val() == 0) $('#documentationLock').css('color', 'white');
	else $('#documentationLock').css('color', 'red');
}