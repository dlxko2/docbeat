{literal}
<script>

    /**
     * Classe page constructor
     */
    $(document).ready(function() {

        // Update topbar
        $('#topStudents').css('color', 'red');

        // Table for students
        $('#studentsTable').dataTable({
            "paging":   false,
             bAutoWidth: false,
            "pageLength": 1000,
            "oLanguage": {
                "sInfoEmpty": {/literal}"{ci_language line="Empty table!"}"{literal},
                "sStripClasses": "",
                "sSearch": "",
                "sSearchPlaceholder": {/literal}"{ci_language line="Enter name here"}"{literal},
                "sInfo": "_TOTAL_ students",
                "sEmptyTable": {/literal}"{ci_language line="No students in system!"}"{literal},
            },
        });

        // Table for graph
        $('#graphTable').dataTable({
            "oLanguage": {
                "sStripClasses": "",
                "sSearch": "",
                "sSearchPlaceholder": {/literal}"{ci_language line="Enter name here"}"{literal},
                "sInfo": "",
                "sLengthMenu": ''
            },
            "pageLength": 7
        });

        // Update the lock
        updateLock();

        // Container modifier for preview
        window_size = $(window).height();
        $('#pdfContainer').height(window_size);
    });

    /*

     "oLanguage": {
     "sStripClasses": "",
     "sSearch": "",
     "sSearchPlaceholder": {/literal}"{ci_language line="Enter name here"}"{literal},
     "sInfo": ""
     },
     */
{/literal}
</script>
