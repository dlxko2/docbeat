<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Reviews
 */
class Reviews extends CI_Controller {

	/**
	 * $userData variable.
	 * Stores session data for class
	 */
	private $userData;

	/**
	 * Reviews constructor.
	 * Get session, set gettext and models
	 * Check if user is logged and project selected
	 */
    public function __construct() {

        parent::__construct();

	    $this->load->model("welcomemodel");
	    $this->load->model("documentationget");
	    $this->load->model("reviewssave");
	    $this->load->model("reviewsfunc");
	    $this->load->model("reviewsget");

	    $this->userData = $this->ss->userdata();
	    if (!array_key_exists('id', $this->userData) || !array_key_exists('selectedProject', $this->userData))
		    redirect('/welcome/index', 'location');
    }

	/**
	 * Main view of review page
	 * Gets the data and save them in template
	 */
    public function index() {

    	// Get data for the documentation used in locked
	    $documentationData = $this->documentationget->getInfoById($this->userData['selectedProject']);
	    // Get data for the chapters
	    $chaptersData = $this->documentationget->getChaptersByProject($this->userData['selectedProject']);
	    // Get reviews data
	    $questionsData = $this->reviewsget->getQuestionsByProject($this->userData['selectedProject']);

	    // Process by Smarty
	    $this->sm->assign('project',$documentationData);
	    $this->sm->assign('questionsdata',$questionsData);
	    $this->sm->assign('chapterdata',$chaptersData);
	    $this->sm->assign('user',$this->userData);
        $this->sm->assign('index',base_url('index.php/'));
        $this->sm->assign('assets',base_url('assets/'));
        $this->sm->display('pages/reviews/reviews.tpl');
    }

	/**
	 * Function to save review form
	 * Gets data from post input and save them into DB
	 */
    public function save() {

	    $postData = $this->input->post();
	    $this->reviewssave->saveReviews($postData, $this->userData['selectedProject']);
	    redirect('/reviews/index', 'location');
    }

	/**
	 * Get questions function
	 * Receive data from DB about questions and send them as JSON
	 */
	public function getImportQuestions(){

	    $questionsData = $this->reviewsget->getQuestionsByProject($this->userData['selectedProject']);
	    echo json_encode($questionsData);
    }
}
