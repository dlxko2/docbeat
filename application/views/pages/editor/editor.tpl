{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/packages/pkg_codemirror.tpl'}
{include file='includes/packages/pkg_progress.tpl'}
{include file='includes/page_content/settings.tpl'}


<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/editor.css">

<div class="row" id="mainRow">

    <div class="col s6" id="editContainer">
        {include file='pages/editor/toolbox.tpl'}
        {include file='pages/editor/paper.tpl'}
        {include file='pages/editor/footer.tpl'}
    </div>

    <div class="col s6" id="pdfContainer" style="background-color: #525659">
        {include file='pages/editor/preview.tpl'}
    </div>

    {include file='pages/editor/upload.tpl'}
    {include file='pages/editor/pictures.tpl'}



</div>

{include file='pages/editor/script.tpl'}
<script src="{$assets}/{$smarty.const.scripts}/editor.js"></script>
{include file='includes/page_content/finish.tpl'}
