<?php

/**
 * Class DeadlinesGet
 */
class DeadlinesGet extends CI_Model
{
	/**
	 * DeadlinesGet constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->load->model("logmodel");
	}

	/**
	 * Get deadlines depending on project
	 * @param $projectID - int : id of the project
	 * @return array - deadlines data or empty
	 */
	public function getDeadlinesByProject($projectID) {

		// Check the input
		if (empty($projectID)) {
			$this->logmodel->lE("Wrong project ID:" . $projectID);
			return [];
		}

		// Look for deadlines depending on project id
		$this->db->order_by('snapshot', 'ASC');
		$this->db->where('project', $projectID);
		$queryDeadlines = $this->db->get('deadlines');
		$result = [];

		// For each deadline generate special informations
		foreach ($queryDeadlines->result() as $rowDeadline) {

			// Look into chapters for additional data
			$resultDeadline = [];
			$this->db->where('deadline', $rowDeadline->id);
			$queryChapters = $this->db->get('deadline_chapter');

			// Create chapter string
			$resultChaptersArray = [];
			$resultChaptersString = "";
			foreach ($queryChapters->result() as $rowChapter) {
				array_push($resultChaptersArray,(array)$rowChapter);
				$resultChaptersString .= $rowChapter->chapter . ',';
			}

			// Create result array
			$resultDeadline['chapterArray'] = $resultChaptersArray;
			$resultDeadline['chapterString'] = $resultChaptersString;
			$resultDeadline['deadlineInfo'] = (array)$rowDeadline;
			array_push($result,$resultDeadline);
		}

		// Return the default value
		return $result;
	}

	/**
	 * Create the result for deadlines for reviews in project
	 * @param $userData - array : data for the logged user
	 * @return array - the special array with deadlines for review
	 * @TODO - fix the default return value
	 */
	public function getReviewDeadlinesByProject($userData) {

		// Check the input
		if (empty($userData)) {
			$this->logmodel->lE("Wrong project ID:" . implode('|', $userData));
			return [];
		}

		// Look for deadlines for project
		$this->db->where('project', $userData['selectedProject']);
		$query = $this->db->get('deadlines');

		// Create the result variables
		$review = [];
		$feedback = [];
		$final = [];
		$result = [];

		// For each deadline return the result
		foreach ($query->result() as $deadline) {
			if ($deadline->review_type == 'R') $review = (array)$deadline;
			if ($deadline->review_type == 'F') $feedback = (array)$deadline;
			if ($deadline->review_type == 'E') $final = (array)$deadline;
		}

		// Create the result array for review
		if (!empty($review)) $result['review'] = ['from' => $review['snapshot'], 'to' => $review['review_end']];
		else {
			$this->logmodel->lW("The review deadline is not set");
			$result['review'] = ['from' => '2016-01-01', 'to' => '2100-01-01'];
		}

		// Create the result array for feedback
		if (!empty($feedback)) $result['feedback'] = ['from' => $feedback['snapshot'], 'to' => $feedback['review_end']];
		else {
			$this->logmodel->lW("The feedback deadline is not set");
			$result['feedback'] = ['from' => '2016-01-01', 'to' => '2100-01-01'];
		}

		// Create the result array for final
		if (!empty($final)) $result['final'] = ['from' => $final['snapshot'], 'to' => $final['review_end']];
		else {
			$this->logmodel->lW("The final deadline is not set");
			$result['final'] = ['from' => '2016-01-01', 'to' => '2100-01-01'];
		}

		// Return the result
		return $result;
	}

	/**
	 * Function to get all chapters used in deadlines
	 * Used in integrity check in chapters in documentation
	 */
	public function getDeadlinesChaptersAll() {

		$result = [];
		$this->db->distinct();
		$this->db->select('chapter');
		$chapters = $this->db->get('deadline_chapter')->result();
		foreach ($chapters as $chapter) array_push($result, $chapter->chapter);
		return $result;
	}
}