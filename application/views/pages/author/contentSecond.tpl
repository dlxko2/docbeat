{if $targets == 2}

    <div class="col s6">
    {if $question.type == 'F'}

        <!-- HIDDEN INPUTS -->
        <input type="hidden" name="iQuestion[]" value="{$question.id}">
        <input type="hidden" name="iType[]" value="{$question.type}">
        <input type="hidden" name="iConnect[]" value="{$targetdata[1].connection}">
        <input type="hidden" name="iOwner[]" value="{$targetdata[1].id}">

        <!-- CONTENT INPUT -->
        <textarea id="textarea1" class="materialize-textarea"
                  placeholder="{ci_language line="Write feedback for"} {$targetdata[1].login}"
                  name="iReview[]">{$content2}</textarea>

        <!-- SLIDERS -->
        <p class="range-field step2SliderContainer">
            <!-- QUALITY -->
            {if $question.quality == 1}
                <label>{ci_language line="Result"}</label>
                <input type="range" min="0" max="10" name="iQuality[]" value="{$quality2}"/>
            {else}
                <input type="hidden" name="iQuality[]" value="0"/>
            {/if}

            <!-- QUANTITY LABEL -->
            <label>
                {assign var="extra2" value=0}
                {foreach from=$chaptersdata item=chapter}
                    {if $chapter.id == $question.quantity}
                        {assign var="extra2" value=$chapter.extra_value}
                        {$chapter.extra}
                    {/if}
                {/foreach}
            </label>

            <!-- QUANTITY -->
            {if $extra2 != 0}
                <input type="range" min="0" max="{$extra1}" name="iQuantity[]" value="{$quantity2}"/>
            {else}
                <input type="hidden" name="iQuantity[]" value="0"/>
            {/if}
        </p>
    </div>

    <!-- RESULT DATA VIEW -->
    {else}
        <div class="card blue-grey darken-1">
            <div class="card-content white-text minHeightContent">
                <p class="noContentPadding">
                    {$content2}
                </p>
            </div>
        </div>
    {/if}
{/if}