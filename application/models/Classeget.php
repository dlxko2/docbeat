<?php

/**
 * Class ClasseGet
 */
class ClasseGet extends CI_Model
{
	/**
	 * ClasseGet constructor.
	 * This is a bit hacky code but I need it
	 * Loads the another models for functionality
	 */
	function __construct() {
		parent::__construct();
		$this->load->model("reviewsget");
		$this->load->model("documentationget");
	}

	/**
	 * Function to count the basic parameters like Review, Feedback and Final
	 * @param $resultsData - array : the results data
	 * @param $connection - int : id of the connection
	 * @return array - the results from this function
	 */
	public function produceRewResults($resultsData, $connection){

		// Initialize variables to be used below
		$rSum = $fSum = $eSum = 0;
		$rCount = $fCount = $eCount = 0;
		$admin = $feedback = $final = $review = false;

		// For every result in array with our connection
		foreach ($resultsData as $result) {
			if ($result['connect'] == $connection) {

				// Get total Final
				if ($result['type'] == 'E'){
					$eSum += $this->resolveReviewMark($result);
					$eCount += 1;
				}

				// Get total Feedback
				if ($result['type'] == 'F') {
					$fSum += $this->resolveReviewMark($result);
					$fCount += 1;
				}

				// Get total Review
				if ($result['type'] == 'R') {
					$rSum += $this->resolveReviewMark($result);
					$rCount += 1;
				}
			}
		}

		// Calculate final values
		if ($rSum > 0 && $rCount > 0) $review = round($rSum/$rCount);
		if ($eSum > 0 && $eCount > 0) $final = round($eSum/$eCount);
		if ($fSum > 0 && $fCount > 0) $review = round($fSum/$fCount);

		// Create output array and return it
		return [
			"connect" => $connection,
			"admin" => $admin,
			"review" => $review,
			"feedback" => $feedback,
			"final" => $final
		];
	}

	/**
	 * Function to calculate total review result from quality and quantity
	 * @param $result - array : the input result line from DB
	 * @return integer - the total result for one line
	 */
	private function resolveReviewMark ($result) {

		// We need the parent question data
		$question = $this->reviewsget->getQuestionByID($result['question']);

		// If we have any quantity information we also need it
		$chapter = [];
		if ($question['quantity'] != 0) {
			$chapter = $this->documentationget->getChapterByID($question['quantity']);
			if (empty($chapter)) {
				$this->logmodel->lE("Question not found:" . $question['id']);
				return false;
			}
		}

		// Prepare variables
		$percent = $counter = 0;

		// Calculate quality if requested and make percent value
		if($question['quality'] == 1) {
			$percent = $result['quality']*10;
			$counter += 1;
		}

		// If requested quantity calculate it from result/reference and make percents
		if ($question['quantity'] > 0 && $chapter['extra_value'] > 0) {
			$percent += ($result['quantity']/$chapter['extra_value'])*100;
			$counter += 1;
		}

		// If nothing requested (only text) return 0
		if ($question['quality'] == 0 && $question['quantity'] == 0)
			return 0;

		// Calculate average as result
		return round($percent/$counter);
	}

	/**
	 * The mega function to get all students with all parameters at once
	 * This is the main function to display data in students
	 * @param $userData - array : data of logged user
	 * @return array - result with all the data
	 */
	public function getStudents($userData) {

		// Create the database query for all students at once - be more effective
		$this->db->cache_on();
		$this->db->select('users.id, user_project.points, user_project.visible, users.login, team_info.name as team_name');
		$this->db->select('team_info.project as team_project, student_marks.author, student_marks.reviewer');
		$this->db->select('student_marks.rew1, student_marks.rew2, student_marks.feed1, student_marks.feed2');
		$this->db->select('student_marks.fin1, student_marks.fin2, student_marks.admin, student_marks.done');
		$this->db->from('user_project');
		$this->db->join('users', 'user_project.user = users.id', 'inner');
		$this->db->join('team_info', 'user_project.team = team_info.id AND user_project.project = team_info.ref_project', 'inner');
		$this->db->join('student_marks', 'user_project.user = student_marks.user', 'left');
		$this->db->order_by('admin', 'DESC');
		$this->db->where('user_project.project', $userData['selectedProject']);
		$query = $this->db->get();

		// Now process every result to append additional data
		$result = [];
		foreach ($query->result() as $row) {

			// Get informations about reviewers
			$this->db->select('users.login, users.id, student_connect.id as connect');
			$this->db->from('student_connect');
			$this->db->join('users', 'student_connect.reviewer = users.id', 'inner');
			$this->db->where('student_connect.author', $row->id);
			$data = $this->db->get()->result();

			// Generate reviewers output
			$rew1 = ['login' => 'unknown', 'id' => 0, 'connect' => 0];
			$rew2 = ['login' => 'unknown', 'id' => 0, 'connect' => 0];
			if (isset($data[0])) $rew1 = (array)$data[0];
			if (isset($data[1])) $rew2 = (array)$data[1];

			// Generate feedbacks for first
			$feedResult1 = 0;
			if ($rew1['id'] != 0) {
				$this->db->where('user', $rew1['id']);
				$this->db->from('student_marks');
				$markData1 = $this->db->get()->result();

				// Find the correct position for feedback
				if (!empty($markData1)) {
					if (current($markData1)->author1 == $row->id)
						$feedResult1 = current($markData1)->feed1;
					else if (current($markData1)->author2 == $row->id)
						$feedResult1 = current($markData1)->feed2;
				}
			}

			// Generate feedbacks for second
			$feedResult2 = 0;
			if ($rew2['id'] != 0) {
				$this->db->where('user', $rew2['id']);
				$this->db->from('student_marks');
				$markData2 = $this->db->get()->result();

				// Find the correct position for feedback
				if (!empty($markData2)) {
					if (current($markData2)->author1 == $row->id)
						$feedResult2 = current($markData2)->feed1;
					else if (current($markData2)->author2 == $row->id)
						$feedResult2 = current($markData2)->feed2;
				}
			}

			// Get favorites information for the user
			$this->db->cache_off();
			$this->db->where('user', $userData['id']);
			$this->db->where('project', $userData['selectedProject']);
			$this->db->where('favorite_user', $row->id);
			$this->db->from('student_favorites');
			$favorite =  ($this->db->count_all_results() > 0) ? true : false;

			// Produce student result array with all the datas
			array_push($result, [
				"base"  => (array)$row,
				"rew1"  => $rew1,
				"rew2"  => $rew2,
				"fav"   => $favorite,
				'feed1' => $feedResult1,
				'feed2' => $feedResult2
			]);
		}
		return $result;
	}

	/**
	 * Function to generate datas for the graph view
	 * @param $projectData - array : data for the project
	 * @param $userID - integer : id of the user
	 * @return array - the graph datas
	 */
	public function getGraphDatas($projectData, $userID)
	{
		// Get the project start
		$start = $projectData['started'];

		// Initialize variables
		$Result = [];
		$PrevDone = 0;

		// Count every week for 13 weeks max
		for ($count = 0; $count <= 13; $count++)
		{
			// Create the week window
			$startDate = date('Y-m-d',strtotime($start. ' + ' . ($count * 7) . ' days'));
			$finishDate = date('Y-m-d',strtotime($start. ' + ' . (($count+1) * 7) . ' days'));

			// Get time and text informations
			$Time = $Text = 0;
			$this->db->select("count(*) count, type");
			$this->db->from('student_stucks');
			$this->db->where('user', $userID);
			$this->db->where('date >=', $startDate);
			$this->db->where('date <', $finishDate);
			$this->db->group_by('type');
			$QueryTT = $this->db->get()->result();

			// Get done informations
			$this->db->select("COALESCE(type,'D') as type, COALESCE(MAX(value),0) as value");
			$this->db->from('student_stucks');
			$this->db->where('user', $userID);
			$this->db->where('type', 'D');
			$this->db->where('date >=', $startDate);
			$this->db->where('date <', $finishDate);
			$QueryDone =  $this->db->get()->result();

			// Calculate done or use previous
			if (current($QueryDone)->value != 0)
			{
				$Done = current($QueryDone)->value;
				$PrevDone = $Done;
			}
			else $Done = $PrevDone;

			// If no data for current week use default and continue
			if (empty($QueryTT)) {
				array_push($Result, [
					"week"        => $count+1,
					"time_stucks" => 0,
					"char_stucks" => 0,
					"done_stucks" => $Done]);
				continue;
			}

			// If data found get the counts for them
			foreach ($QueryTT as $row) {
				if ($row->type == 'T') $Time = $row->count;
				else if ($row->type == 'C') $Text = $row->count;
			}

			// Create output array
			array_push($Result, [
				"week"        => $count+1,
				"time_stucks" => $Time,
				"char_stucks" => $Text,
				"done_stucks" => $Done]);
		}
		return $Result;
	}
}