<div class="row center-align">

    {if $project.locked == 0}
    <ul class="pagination center-align" >
        <li>
            <i class="small material-icons cursorPointer" onclick="aSaveChanges()">save</i>
            <b>&nbsp;{ci_language line="Save"}</b>
        </li>
    </ul>
    {/if}

</div>