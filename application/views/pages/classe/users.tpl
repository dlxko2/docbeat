<!-- MODAL WITH USER IMPORT -->
<div id="mAddUsers" class="modal">
    <div class="modal-content">

        <form role="form" id="fClasse" method="post" enctype="multipart/form-data"
              accept-charset="utf-8" action="{$index}/classe/addUsers">

            <div class="row">
                <form class="col s12">

                    <!-- TEAMS -->
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="iTeams" class="materialize-textarea" name="iTeams"
                                      placeholder="{ci_language line="Team format"}:[TEAM_ID],[TEAM_NAME],[PROJECT_NAME]"></textarea>
                            <label for="iTeams">{ci_language line="Teams CSV"}</label>
                        </div>
                    </div>

                    <!-- USERS -->
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="iUsers" class="materialize-textarea" name="iUsers"
                                      placeholder="{ci_language line="User format"}:[LOGIN],[TEAM]"></textarea>
                            <label for="iUsers">{ci_language line="Users CSV"}</label>
                        </div>
                    </div>

                </form>
            </div>
        </form>

    <!-- FOOTER -->
    <div class="modal-footer">
        <a href="#!" class=" modal-action waves-effect waves-green btn-flat" onclick="aProcess()">{ci_language line="Process"}</a>
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">{ci_language line="Close"}</a>
    </div>

</div>