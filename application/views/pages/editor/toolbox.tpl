<nav id="toolboxContainer">
    <div class="nav-wrapper">

        <div class="input-field col s4">
            <select id="chapterSelector">
                <option value="" disabled selected>{ci_language line="Choose chapter"}</option>
                {foreach from=$chapters item=chapter}
                    <option value="{$chapter.id}">{$chapter.name}</option>
                {/foreach}
                <option value="-1">{ci_language line="Literature"}</option>
            </select>
        </div>

        <ul id="nav-mobile" class="right hide-on-med-and-down">

            <li onclick="aPreview('{$index}', '{$assets}', {$user.id}, {$user.selectedProject}, 'P',
                    '{ci_language line="of chapter already done"}')">
                <i class="material-icons cursorPointer">cached</i>
            </li>
            <li onclick="aPreview('{$index}', '{$assets}', {$user.id}, {$user.selectedProject}, 'B')">
                <i class="material-icons cursorPointer">format_bold</i>
            </li>
            <li onclick="aSaveChapter('{$index}', '{ci_language line="of chapter already done"}')">
                <i class="material-icons cursorPointer">save</i>
            </li>
            <li>
                <i class="material-icons cursorPointer" onclick="aPictures()">insert_photo</i>
            </li>
            <li>
                <i class="material-icons cursorPointer" onclick="aUpload()">present_to_all</i>
            </li>
            <!--
            <li>
                <i class="material-icons">access_time</i>
            </li> -->
            <li>
                <input type="hidden" value="active" id="activityStore">
                <input type="hidden" value="0" id="characterStore">
            </li>
            <li><a href="{$assets}/datas/users/{$user.id}/{$user.selectedProject}/preview/main.pdf"><i class="material-icons">get_app</i></a></li>

        </ul>
    </div>
</nav>