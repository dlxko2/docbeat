{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/page_content/settings.tpl'}

<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/reviewer.css">

<div class="row" id="mainRow">

    <div class="col s6" id="editContainer">

        <form role="form" id="fReviewer" method="post" enctype="multipart/form-data"
              accept-charset="utf-8" action="{$index}/reviewer/save">

            {include file='pages/reviewer/toolbox.tpl'}
            {include file='pages/reviewer/pagination.tpl'}
            {include file='pages/reviewer/content.tpl'}

        </form>

    </div>

    <div class="col s6" id="pdfContainer">
        {include file='pages/reviewer/preview.tpl'}
    </div>

</div>

{include file='pages/reviewer/script.tpl'}
<script src="{$assets}/{$smarty.const.scripts}/reviewer.js"></script>
{include file='includes/page_content/finish.tpl'}
