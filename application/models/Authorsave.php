<?php
require_once APPPATH.'/models/Authorreviewer.php';

/**
 * Class AuthorSave
 */
class AuthorSave extends AuthorReviewer
{
	/**
	 * AuthorSave constructor.
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Update mark informations for the target
	 * @param $source - int : id of the source person
	 * @param $target - int : id of the target person
	 * @param $project - int : id of the current project
	 * @return bool - result of the operation
	 */
	public function updateMarks($source, $target, $project)
	{
		// Find persons who are reviewed by this target
		$this->db->where('reviewer', $target);
		$this->db->where('project', $project);
		$this->db->where('author !=', 0);
		$connections = $this->db->get('student_connect')->result();
		if (!isset($connections[0])) {
			$this->logmodel->lE("No connection but me:" . $target . '|' . $project);
			return false;
		}

		// Prepare variables
		$connection = 0;
		$position = 0;

		// Count the position if we have 2 connections found
		// We need to get information which is first and which second (depends on connection id)
		if (count($connections) > 1) {
			if ($connections[0]->author == $source){
				$connection = $connections[0]->id;
				$position = ($connections[0]->id > $connections[1]->id) ? 1 : 2;
			}
			else if ($connections[1]->author == $source){
				$connection = $connections[1]->id;
				$position = ($connections[1]->id > $connections[0]->id) ? 1 : 2;
			}
		}
		else {
			$connection = $connections[0]->id;
			$position = 1;
		}

		// Get mark from the feedbacks
		$feedback = $this->getMarkSpecified($connection, 'F');
		if ($feedback === false) return false;

		// Save mark into database
		if (!$this->saveMarkInfo($feedback, 0,  $project, $target, $source, $position)) return false;

		// Regenerate the total mark
		$total = $this->getMarkTotal($target, $project);

		// TODO save again with updated total mark
		if (!$this->saveMarkInfo($feedback, $total, $project, $target, $source, $position)) return false;
		return true;
	}

	/**
	 * Save specified mark into database
	 * @param $feedback - int : feedback mark
	 * @param $total - int : total mark
	 * @param $project - int : current project
	 * @param $target - int : affected user
	 * @param $source - int : source user
	 * @param $position - int : position
	 * @return boolean - result of function
	 */
	public function saveMarkInfo($feedback, $total, $project, $target, $source, $position) {

		// Prepare array with informations
		$dbArray = [
			'user'                => $target,
			'project'             => $project,
			'reviewer'            => $total,
			'feed' . $position    => $feedback,
			'author' . $position  => $source
		];

		// Look if result already exists
		$this->db->where('user', $target);
		$this->db->where('project', $project);

		// Update or insert data into DB
		if ($this->db->count_all_results('student_marks') == 0) {
			$this->db->insert('student_marks', $dbArray);
			if ($this->db->affected_rows() < 1) {
				$this->logmodel->lE("Can't insert:" . implode("|", $dbArray));
				return false;
			}
		} else {
			$this->db->where('user', $target);
			$this->db->where('project', $project);
			$this->db->update('student_marks', $dbArray);
		}
		return true;
	}

	/**
	 * Get total mark from the mark line
	 * Reviewer mark = avg(FeedX, FeedY)
	 * @param $target - int : affected user
	 * @param $project - int : current project
	 * @return int - total mark
	 */
	private function getMarkTotal($target, $project) {

		// Try to get mark
		$this->db->where('user', $target);
		$this->db->where('project', $project);
		$mark = $this->db->get('student_marks')->result();

		// If no mark just 0
		if (empty($mark)) return 0;

		// If two reviewers get the result from both
		if (!empty(current($mark)->author1) && !empty(current($mark)->author2))
			return round(($mark[0]->feed1 + $mark[0]->feed2)/2);

		// If just one count for one
		if (!empty(current($mark)->author1) && empty(current($mark)->author2))
			return round($mark[0]->feed1);
		if (!empty(current($mark)->author2) && empty(current($mark)->author1))
			return round($mark[0]->feed2);

		// If nobody just 0
		return 0;

	}
}