<nav id="footerContainer">
    <div class="nav-wrapper black">

        <div class="col s4">
            <div class="progress">
                <div id="prog1" class="progress-bar" role="progressbar" aria-valuenow="50"
                     aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    <span id="prog1Text" class="progressText">{ci_language line="Please select the chapter"}</span>
                </div>
            </div>

        </div>

        <div class="col s4">
            <div class="progress">
                <div id="prog2" class="progress-bar" role="progressbar" aria-valuenow="70"
                     aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    <span id="prog2Text" class="progressText">{ci_language line="Please select the chapter"}</span>
                </div>
            </div>
        </div>

        <div class="col s4">
            <div class="progress">
                <div id="prog3" class="progress-bar" role="progressbar" aria-valuenow="70"
                     aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    <span id="prog3Text" class="progressText">{ci_language line="Loading the data"}...</span>
                </div>
            </div>
        </div>

    </div>
</nav>