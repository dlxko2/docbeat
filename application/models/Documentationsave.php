<?php

/**
 * Class DocumentationSave
 */
class DocumentationSave extends CI_Model
{
	/**
	 * DocumentationSave constructor.
	 */
	function __construct() {
        parent::__construct();
		$this->load->model("logmodel");
    }

	/**
	 * Save new project in DB with default name
	 * @param $userID - int : id of the user
	 * @return integer - inserted id
	 */
	public function saveNew($userID){

		// Check the input parameters
		if (empty($userID)) {
			$this->logmodel->lE("Wrong userID:" . $userID);
			return 0;
		}

		// Prepare the name and insert into DB
        $newName = "P " . date('m.d.Y H:i');
        $this->db->insert('projects', ['user' => $userID,'name' => $newName]);

		// Check if anything inserted
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Can't insert:" . $newName);
			return 0;
		}

		// Return the inserted ID
        return intval($this->db->insert_id());
    }

	/**
	 * Function saves the updated project info
	 * @param $postData - array : data from post
	 * @return boolean - the result of the function
	 */
	public function saveInfo($postData){

		// Check the input data
		if (empty($postData)) {
			$this->logmodel->lE("Project info is empty");
			return false;
		}

		// Update project start date
		$projectStart = new DateTime($postData['iProjectStart']);

		// Convert project visibility and prepare the array
		$visibility = (isset($postData['iProjectVisible'])) ? true : false;
		$dbArray = [
			'name'         => $postData['iProjectName'],
			'description'  => $postData['iProjectDescription'],
			'visible'      => $visibility,
			'base'         => $postData['iProjectBase'],
			'started'      => $projectStart->format('Y-m-d H:i:s')
		];

		// Try to update the database row
		// Cant check update (can be 0 affected rows)
        $this->db->where('id', $postData['iProjectID']);
        $this->db->update('projects', $dbArray);

		// Return the default result
		return true;
    }

	/**
	 * Function to save all chapters into DB
	 * @param $postData - array : datas from the post
	 * @return boolean - the result of the function
	 */
	public function saveChapters($postData){

		// Check the input data
		if (empty($postData)) {
			$this->logmodel->lE("Chapter info is empty");
			return false;
		}

		// If nothing just delete all chapters and return
        if (!array_key_exists('iChapterName', $postData)){
            $this->db->delete('chapters', ['project' => $postData['iProjectID']]);
            return true;
        }

		// Delete existing old chapters
        $this->db->where_not_in('id', $postData["iID"]);
        $this->db->delete('chapters', ['project' => $postData['iProjectID']]);

		// Insert or update chapters
		foreach ($postData['iChapterName'] as $key => $name) {

			// Create the data array
			$dbArray =  [
				$postData['iProjectID'],
				$postData['iChapterName'][$key],
				$postData['iSliderWords'][$key],
				$postData['iSliderCites'][$key],
				$postData['iMetricName'][$key],
				$postData['iSliderMetric'][$key],
				$postData['iAreaContent'][$key],
				$postData['iHidden'][$key]];

			// Insert
			if ($postData["iID"][$key] == 0) {
				$sql = "INSERT INTO chapters(project, name, words, cites, extra, extra_value, content, hidden) ";
				$sql .= "VALUES (?,?,?,?,?,?,?,?);";
				$this->db->query($sql, $dbArray);
			}

			// Update
			else {
				$sql = "UPDATE chapters SET project =?, name =?, words =?, cites =?, extra =?, extra_value =?, ";
				$sql .= "content =?, hidden =? WHERE id =?;";
				array_push($dbArray, $postData["iID"][$key]);
				$this->db->query($sql, $dbArray);
			}
        }

        // Return the default result
		return true;
    }

	/**
	 * Update project lock info
	 * @param $projectID - int : id of the project
	 * @return array - the result of the function
	 */
	public function updateLock($projectID) {

		// Check the input
		if (empty($projectID)) {
			$this->logmodel->lE("Project ID is wrong" . $projectID);
			return ['result' => false, 'state' => false];
		}

		// Get the status
        $this->db->select('locked');
        $this->db->where('id', $projectID);
        $locked = current($this->db->get('projects')->result())->locked;

		// Update the status
        $this->db->where('id', $projectID);
        if ($locked == 1) {
            $this->db->update('projects', ['locked' => 0]);
	        $locked = false;
        }
        else {
            $this->db->update('projects', ['locked' => 1]);
	        $locked = true;
        }

		// Check the result
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Can't update lock:" . $projectID);
			return ['result' => false, 'state' => $locked];
		}

		// Return the default result
		return ['result' => true, 'state' => $locked];
    }
}