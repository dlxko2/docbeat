{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/packages/pkg_chartist.tpl'}
{include file='includes/page_content/settings.tpl'}

<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/classe.css">

<div class="row">

    <div class="col s6">
        {include file='pages/classe/class.tpl'}
    </div>

    <div class="col s6" id="pdfContainer">
        {include file='pages/classe/preview.tpl'}
    </div>

    {include file='pages/classe/help.tpl'}
    {include file='pages/classe/users.tpl'}

</div>

{include file='pages/classe/script.tpl'}
<script src="{$assets}/{$smarty.const.scripts}/base.js"></script>
<script src="{$assets}/{$smarty.const.scripts}/classe.js"></script>
{include file='includes/page_content/finish.tpl'}
{include file='includes/packages/pkg_datatables.tpl'}
