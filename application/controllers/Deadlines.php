<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deadlines extends CI_Controller {

	/**
	 * $userData variable.
	 * Stores session data for class
	 */
	private $userData;

	/**
	 * Deadlines constructor.
	 * Get session, set gettext and models
	 * Check if user is logged and project selected
	 */
    public function __construct() {

        parent::__construct();

	    $this->load->model("deadlinessave");
	    $this->load->model("deadlinesget");
	    $this->load->model("documentationget");

	    $this->userData = $this->ss->userdata();
	    if (!array_key_exists('id', $this->userData) || !array_key_exists('selectedProject', $this->userData))
		    redirect('/welcome/index', 'location');
    }

	/**
	 * Main view of deadlines page
	 * Gets the data and save them in template
	 */
    public function index() {

    	// Get data for the documentation - used in lock feature
	    $documentationData = $this->documentationget->getInfoById($this->userData['selectedProject']);
	    // Get data for the chapters
	    $chaptersData = $this->documentationget->getChaptersByProject($this->userData['selectedProject']);
	    // Get data for the deadlines
		$deadlinesData = $this->deadlinesget->getDeadlinesByProject($this->userData['selectedProject']);

	    // Process by Smarty
	    $this->sm->assign('project',$documentationData);
	    $this->sm->assign('deadlinesdata',$deadlinesData);
	    $this->sm->assign('chaptersdata',$chaptersData);
	    $this->sm->assign('user',$this->userData);
        $this->sm->assign('index',base_url('index.php/'));
        $this->sm->assign('assets',base_url('assets/'));
        $this->sm->display('pages/deadlines/deadlines.tpl');
    }

	/**
	 * Function to save deadlines
	 * Get data from the post input and save them into DB
	 */
	public function save() {

	    $postData = $this->input->post();
	    $this->deadlinessave->saveDeadlines($postData, $this->userData['selectedProject']);
	    redirect('/deadlines/index', 'location');
    }
}
