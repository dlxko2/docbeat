<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper black">
            <ul class="left hide-on-med-and-down">

                <!-- LOGO -->
                <li>
                    <a href="{$index}/welcome" class="brand-logo white-text left">
                        <img src="{$assets}/{$smarty.const.logo}" class="docbeat-logo">
                    </a>
                </li>

                <!-- LOCK -->
                {if 'login'|array_key_exists:$user}
                    {if $user.permissions == 'A' && isset($user.selectedProject) && isset($project.name)}
                            <li><a href="#"><span id="titleProjectName">{$project.name}</span></a></li>
                        <li>
                            <a href="#" onclick="aLock('{$index}')">
                                <i class="large material-icons" id="documentationLock">lock</i>
                                <input type="hidden" value="{$project.locked}" id="docLockVal">
                            </a>
                        </li>
                    {/if}
                {/if}

            </ul>

            <!-- MENU -->
            <ul class="right hide-on-med-and-down">

                <!-- ADMIN -->
                {if 'login'|array_key_exists:$user}
                    {if $user.permissions == 'A' && isset($user.selectedProject)}
                        <li>
                            <a href="{$index}/documentation" id="topDocumentation">
                                <i class="large material-icons left">assignment</i>{ci_language line="Documentation"}
                            </a>
                        </li>
                        <li>
                            <a href="{$index}/reviews" id="topReviews">
                                <i class="large material-icons left">question_answer</i>{ci_language line="Reviews"}
                            </a>
                        </li>
                        <li>
                            <a href="{$index}/deadlines" id="topDeadlines">
                                <i class="large material-icons left">schedule</i>{ci_language line="Deadlines"}
                            </a>
                        </li>
                     {/if}
                {/if}

                <!-- STUDENT -->
                {if 'login'|array_key_exists:$user}
                    {if $user.permissions == 'S' && isset($user.selectedProject)}
                        <li>
                            <a href="{$index}/editor" id="topEditor">
                                <i class="large material-icons left">border_color</i>{ci_language line="Editor"}
                            </a>
                        </li>
                        <li>
                            <a href="{$index}/author" id="topAuthor">
                                <i class="large material-icons left">person</i>{ci_language line="Author"}
                            </a>
                        </li>
                        <li>
                            <a href="{$index}/reviewer" id="topReviewer">
                                <i class="large material-icons left">format_clear</i>{ci_language line="Reviewer"}
                            </a>
                        </li>
                    {/if}
                {/if}

                <!-- NOBODY -->
                {if not 'login'|array_key_exists:$user}
                    <li onclick="aLoginOpen()">
                        <a>
                            <i class="large material-icons left">assignment_ind</i>{ci_language line="Log In"}
                        </a>
                    </li>
                {/if}

                <!-- EVERYBODY -->
                {if 'login'|array_key_exists:$user}
                    {if isset($user.selectedProject)}
                    <li>
                        <a href="{$index}/classe" id="topStudents">
                            <i class="large material-icons left">face</i>{ci_language line="Students"}
                        </a>
                    </li>
                    {/if}
                    <li onclick="openSettings()">
                        <a href="#">
                            <i class="large material-icons left">settings</i>{ci_language line="Settings"}
                        </a>
                    </li>
                    <li>
                        <a href="{$index}/welcome/logout">
                            <i class="large material-icons left">lock</i>{ci_language line="Log Out"}
                        </a>
                    </li>
                {/if}
            </ul>

        </div>
    </nav>
</div>