<?php

/**
 * Class DocumentationGet
 */
class DocumentationGet extends CI_Model
{
	/**
     * DocumentationGet constructor.
     */
    function __construct() {
        parent::__construct();
	    $this->load->model("logmodel");
    }

	/**
     * Get all projects from database
     * @return array - array of all projects
     */
    public function getProjectsAll() {

    	// Create the query for the all project info
	    $this->db->select('projects.id, projects.name, projects.user, projects.description, projects.visible');
	    $this->db->select('projects.base, projects.locked, projects.created, users.login, projects.started');
	    $this->db->from('projects');
	    $query = $this->db->join('users', 'projects.user = users.id')->get();
        $result = [];

	    // Create the output result
        foreach ($query->result() as $row) {

        	// Get students
	        $project = (array)$row;
	        $this->db->where('project', $row->id);
	        $this->db->from('user_project');
	        $project['students'] = $this->db->count_all_results();

	        // Update dates
	        $start = new DateTime($row->started);
	        $stripStart = $start->format('Y-m-d');
	        $create = new DateTime($row->created);
	        $stripCreate = $create->format('Y-m-d');
	        $project['started'] = $stripStart;
	        $project['created'] = $stripCreate;

	        // Push result
	        array_push($result, $project);
        }

        // Return the default result
        return $result;
    }

	/**
	 * Get project by its ID
     * @param $projectID - int : project id
     * @return array - project info or empty
     */
    public function getInfoById($projectID) {

    	// Check the input
	    if (empty($projectID)) {
		    $this->logmodel->lE("Wrong project ID:" . $projectID);
		    return [];
	    }

    	// Get from the database
        $this->db->where('id', $projectID);
        $query = $this->db->get('projects');
        $result = $query->result();

	    // Check the result
	    if (empty($result)) {
		    $this->logmodel->lE("Can't get project info:" . $projectID);
		    return [];
	    }

	    // Return the current line as array
        return (array)current($result);
    }

	/**
	 * Get all project chapters
     * @param $projectID - int : id of the project
     * @return array - all chapters for project or empty
     */
    public function getChaptersByProject($projectID) {

	    // Check the input
	    if (empty($projectID)) {
		    $this->logmodel->lE("Wrong project ID:" . $projectID);
		    return [];
	    }

    	// Get from the database
        $this->db->where('project', $projectID);
        $query = $this->db->get('chapters');
        $result = [];

	    // Create array
        foreach ($query->result() as $row)
            array_push($result,(array)$row);

	    // Return the default result
        return $result;
    }

	/**
	 * Get chapter info by the ID
	 * @param $chapterID - integer : id of the chapter
	 * @return array - the chapter info or empty
	 */
	public function getChapterByID($chapterID) {

		// Check the input
		if (empty($chapterID)) {
			$this->logmodel->lE("Wrong chapter ID:" . $chapterID);
			return [];
		}

		// Get from the database
		$this->db->where('id', $chapterID);
		$query = $this->db->get('chapters')->result();

		// Check the result
		if (empty($query)) {
			$this->logmodel->lE("Can't get chapter info:" . $chapterID);
			return [];
		}

		// Return the default
		return (array)current($query);
	}

	/**
	 * Return the first chapter to be shown
	 * @param $userData - data for the user
	 * @return int - id of the chapter
	 */
	public function getActualChapterID($userData) {

		$this->db->select('id');
		$this->db->where('project', $userData['selectedProject']);
		$this->db->where('hidden', 0);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$result = $this->db->get('chapters')->result();
		if (!empty($result)) return current($result)->id;
		else return 0;
	}
}