/**
 * Get list of project in system
 * @param index - string : path to index
 */
function getImportProjects(index) {

	// First clear the table
	$('.tRewProjects').dataTable().fnClearTable();

	$.ajax
	({
		url: index + '/documentation/getImportProjects',
		dataType: 'json',
		type: 'GET',
		error: function () {alert("Can't get projects for import!")},
		success: function (data) {

			// Append every row
			data.forEach(function (entry) {
				$('.tRewProjects').dataTable().fnAddData([
					entry.name,
					entry.user,
					'<i class="material-icons" onclick="aShowQuestions(\'' +
					index + '\',' + entry.id + ')">open_in_browser</i>']
				);
			});
		}
	});
}

/**
 * Get question to be in questions table
 * @param index - string : path to index
 * @param projectID - int : id of the project
 */
function getImportQuestions (index, projectID) {

	// First clear the table
	$('.tRewQuestions').dataTable().fnClearTable();

	$.ajax
	({
		url: index + '/reviews/getImportQuestions/' + projectID,
		dataType: 'json',
		type: 'GET',
		error: function () {alert("Can't get questions!")},
		success: function (data) {

			// Append every row
			data.forEach(function (entry) {
				$('.tRewQuestions').dataTable().fnAddData([
					entry.content,
					entry.type,
					'<i class="material-icons" onclick="aCreateQuestion('
					+ entry.id + ',\''
					+ entry.content + '\','
					+ entry.quality + ',0)">open_in_browser</i>']
				);
			});
		}
	});
}
