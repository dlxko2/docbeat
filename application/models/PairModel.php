<?php

class Pairmodel extends CI_Model
{
    private $studentsArray;
    private $tempStudents;
    private $actualStudent;

    function __construct() {

        parent::__construct();
        $this->tempStudents = array();
        $this->actualStudent = array();
    }

    public function setStudentsArray($studentsArray) {
        $this->studentsArray = $studentsArray;
    }

    public function getStudentsArray() {
        return $this->studentsArray;
    }

    private function notInTemp($v) {
        return !in_array($v['user'], $this->tempStudents);
    }

    private function notInTeam($v) {
        return $v['team'] != $this->actualStudent['team'];
    }

    private function notFeeded($v) {
        return $v['beFeeded'] != 2;
    }

    public function sorterPass() {

        foreach ($this->studentsArray as $key => $student) {

            if (empty($student['toRew1'])) $position = 'toRew1';
            else if (empty($student['toRew2'])) $position = 'toRew2';
            else break;

            // Ulozime seba do nedostupnych userov
            $this->actualStudent = $student;
            array_push($this->tempStudents, $student['user']);
            if ($position == 'toRew2') array_push($this->tempStudents, $student['toRew1']);

            while (true) {

                // Ziskame zoznam studentov (nie v temp, nie rovnaky team, nie feeded)
                $avaiableStudentsTemp = array_filter($this->studentsArray, array($this, 'notInTemp'));
                $avaiableStudentsTeam = array_filter($avaiableStudentsTemp, array($this, 'notInTeam'));
                $avaiableStudentsFeed = array_filter($avaiableStudentsTeam, array($this, 'notFeeded'));
                if (empty($avaiableStudentsFeed)) break;

                // Vyberieme nahodneho studenta
                $randomStudentKey = array_rand($avaiableStudentsFeed, 1);
                $randomStudentVal = $avaiableStudentsFeed[$randomStudentKey];
                $this->studentsArray[$key][$position] = $randomStudentVal['user'];
                ++$this->studentsArray[$randomStudentKey]['beFeeded'];
                break;
            }

            $this->actualStudent = array();
            $this->tempStudents = array();
        }
    }

    public function saveStudentsArray($pairs, $projectID) {

        $this->db->delete('student_connect', ['project' => $projectID]);

        foreach ($pairs as $pair) {

            $dbArray = array(
                'author'   => $pair['user'],
                'reviewer' => $pair['toRew1'],
                'project'  => $projectID
            );

            $this->db->insert('student_connect', $dbArray);

            $dbArray = array(
                'author'   => $pair['user'],
                'reviewer' => $pair['toRew2'],
                'project'  => $projectID
            );

            $this->db->insert('student_connect', $dbArray);
        }
    }



}