<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Classe
 */
class Classe extends CI_Controller {

	/**
	 * $userData variable.
	 * Stores session data for class
	 */
	private $userData;

	/**
	 * Classe constructor.
	 * Get session, set gettext and models
	 * Check if user is logged and project selected
	 */
    public function __construct() {

        parent::__construct();

	    $this->load->model("classeget");
	    $this->load->model("classesave");
	    $this->load->model("classefunc");
	    $this->load->model("welcomemodel");
	    $this->load->model("reviewsget");
	    $this->load->model("authorget");
	    $this->load->model("documentationget");
        $this->load->model("pairmodel");

	    $this->userData = $this->ss->userdata();
	    if (!array_key_exists('id', $this->userData) || !array_key_exists('selectedProject', $this->userData))
		    redirect('/welcome/index', 'location');
    }

	/**
	 * Main view of classe page
	 * Gets the data and save them in template
	 */
    public function index() {

    	// Get all students data at once
	    $users = $this->classeget->getStudents($this->userData);
	    // Get informations about project
	    $documentationData = $this->documentationget->getInfoById($this->userData['selectedProject']);

	    // Process data with Smarty
	    $this->sm->assign('project',$documentationData);
	    $this->sm->assign('usersdata',$users);
	    $this->sm->assign('user',$this->userData);
        $this->sm->assign('index',base_url('index.php/'));
        $this->sm->assign('assets',base_url('assets/'));
        $this->sm->display('pages/classe/classe.tpl');
    }

	/**
	 * Function to create student pairs
	 * Gets the all project users and then connect them
	 * @TODO - need to be random selected and if nobody take just one system
	 */
	public function createPairs() {

        $users = $this->welcomemodel->getUsersByProject($this->userData['selectedProject']);
        $this->pairmodel->setStudentsArray($users);
        $this->pairmodel->sorterPass();
        $this->pairmodel->sorterPass();
        $pairs = $this->pairmodel->getStudentsArray();
        $this->pairmodel->saveStudentsArray($pairs, $this->userData['selectedProject']);
	}

	/**
	 * Function to process team and users import
	 * Get the CVS input from the post data and save it to database
	 * This functionality also sends the emails
	 */
	public function addUsers() {

		$postData = $this->input->post();
		$this->classesave->saveTeams($this->userData, $postData['iTeams']);
		$this->classesave->saveUsers($this->userData, $postData['iUsers']);
		$this->classesave->informUsers($postData['iUsers']);
		redirect('/classe/index', 'location');
	}

	/**
	 * Function to create new favorite user in list
	 * Gets the user and save it into DB as favorite
	 * @param $user - integer : id of the user
	 */
	public function updateFavorite($user) {

		$result = $this->classesave->updateFavorite($this->userData, $user);
		echo json_encode($result);
	}

	/**
	 * Functionality to get the graph data for selected user
	 * The data contains informations for every week
	 * @param $user - integer : id of the user
	 */
	public function getGraphData($user) {

		$documentationData = $this->documentationget->getInfoById($this->userData['selectedProject']);
		$result = $this->classeget->getGraphDatas($documentationData, $user);
		echo json_encode($result);
	}

	/**
	 * Function to delete the classe cache
	 */
	public function regenerate() {
		$this->classefunc->regenerate();
		echo "OK";
	}
}
