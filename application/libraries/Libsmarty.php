<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once( APPPATH.'libraries/LibSmarty/libs/Smarty.class.php' );

class LibSmarty extends Smarty {

public function __construct()
    {
        parent::__construct();

        $this->setTemplateDir(APPPATH . "views/");
        $this->setCompileDir(APPPATH . "cache/templates");
        $this->setConfigDir(APPPATH . "config");
        $this->setCacheDir(APPPATH . "chache");
        log_message('debug', "Smarty Class Initialized");
    }
}