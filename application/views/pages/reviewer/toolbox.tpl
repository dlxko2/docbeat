<!-- SNAPSHOTS DATA 1-->
{if $targets > 0}
    <ul id="snapshotDropdown1" class="dropdown-content">

        <!-- DEFAULT -->
        <li onclick="updateSnaphot(
           '{$assets}',
            {$reviewersdata[0].id},
           '{$reviewersdata[0].login}',
            {$user.selectedProject}, '')">
            <a href="#!">{ci_language line="Current snapshot"}</a>
        </li>

        <!-- ANOTHER SNAPSHOTS -->
        {foreach from=$snapshotdata0 item=snapshot}
            <li onclick="updateSnaphot(
               '{$assets}',
                {$reviewersdata[0].id},
               '{$reviewersdata[0].login}',
                {$user.selectedProject},
               '{$snapshot}')">
                <a href="#!">{$snapshot}</a>
            </li>
        {/foreach}
    </ul>
{/if}

<!-- SNAPSHOTS DATA 2-->
{if $targets > 1}
    <ul id="snapshotDropdown2" class="dropdown-content">

        <!-- DEFAULT -->
        <li onclick="updateSnaphot(
           '{$assets}',
            {$reviewersdata[1].id},
           '{$reviewersdata[1].login}',
            {$user.selectedProject}, '')">
            <a href="#!">{ci_language line="Current snapshot"}</a>
        </li>

        <!-- ANOTHER SNAPSHOTS -->
        {foreach from=$snapshotdata1 item=snapshot}
            <li onclick="updateSnaphot(
               '{$assets}',
                {$reviewersdata[1].id},
               '{$reviewersdata[1].login}',
                {$user.selectedProject},
               '{$snapshot}')">
                <a href="#!">{$snapshot}</a>
            </li>
        {/foreach}
    </ul>
{/if}

<!-- TOOLBOX -->
<nav id="toolboxContainer">
    <div class="nav-wrapper">

        <!-- FIRST BLOCK -->
        {if $targets > 0}
            <div class="input-field col s5 left toolbarTopMargin">
                <ul>
                    <!-- TEAM -->
                    {foreach from=$teamdata0 item=teamer}
                    <li>
                        <i class="material-icons cursorPointer" title="{$teamer.login}"
                           {if $teamer.id == {$reviewersdata[0].id}}style="color: green"{/if}
                           onclick="aPreview(
                                '{$assets}',
                                 {$teamer.id},
                                '{$teamer.login}',
                                 {$user.selectedProject})">face</i>
                    </li>
                    {/foreach}

                    <!-- SNAPSHOT -->
                    <li>
                        <a class="dropdown-button" href="#!" data-activates="snapshotDropdown1">
                            {ci_language line="Snapshot"}<i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>

                    <!-- USER -->
                    <li>
                        <b>{$reviewersdata[0].login}</b>
                    </li>
                </ul>
            </div>
        {/if}

        <!-- REVIEWER MARK -->
        <div class="col s2 toolbarTopMargin center-align">
            <div class="chip" id="rewMark">
                {if isset($markdata.reviewer)}{$markdata.reviewer}%
                {else} N\A
                {/if}
            </div>
        </div>

        <!-- SECOND BLOCK -->
        {if $targets > 1}
            <div class="input-field col s5 right toolbarTopMargin">
                <ul class="right">

                    <!-- USER -->
                    <li>
                        <b>{$reviewersdata[1].login}</b>
                    </li>

                    <!-- SNAPSHOT -->
                    <li>
                        <a class="dropdown-button" href="#!" data-activates="snapshotDropdown2">
                            {ci_language line="Snapshot"}<i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>

                    <!-- TEAM -->
                    {foreach from=$teamdata1 item=teamer}
                    <li>
                        <i class="material-icons cursorPointer" title="{$teamer.login}"
                           {if $teamer.id == {$reviewersdata[1].id}}style="color: green"{/if}
                           onclick="aPreview(
                                '{$assets}',
                                 {$teamer.id},
                                '{$teamer.login}',
                                 {$user.selectedProject})">face</i>
                    </li>
                    {/foreach}
                </ul>
            </div>
        {/if}

    </div>
</nav>