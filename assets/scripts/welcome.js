/**
 * Create new project and insert it into the project table
 * @param index - string : path to index
 * @param user - int : id of the user
 */
function addNewProject(index, user, translation){

    $.ajax
    ({
        url: index + '/welcome/createProject',
        dataType: 'text',
        type: 'GET',
        error: function() {alert("Can't update preview!")},
        success: function(data)
        {
            var date = new Date();
            var projectName =
                'P ' +
                date.getDate() + '.' +
                date.getMonth() + '.' +
                date.getFullYear() + ' ' +
                date.getHours() + ':' +
                date.getMinutes();
            var projectDate =
                date.getDate() + '.' +
                date.getMonth() + '.' +
                date.getFullYear();

            $('#tProjects').dataTable().fnAddData( [
                projectName,
                user,
                projectDate,
                'N/A',
                0,
                '<a href="' + index + '/welcome/enterProject/' + data + '">' +
                '<i class="material-icons enterIcon">open_in_browser</i>' +
                '<span class="enterFont">' + translation + '</span></a>']
            );
        }
    });
}