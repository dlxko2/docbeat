{if isset($user.language)}
    {ci_language file="my" lang=$user.language}
{else}
    {ci_language file="my" lang="sk"}
{/if}

<!DOCTYPE html>
<html lang="cz">
<head>
<meta charset="utf-8">
<title>DocBeat2</title>
<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/base.css">
<script src="{$assets}/{$smarty.const.scripts}/base.js"></script>