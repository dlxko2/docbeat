/**
 * Function to preview PDF
 * @param index - index path
 * @param assets - assets path
 * @param userID - id of the user
 * @param userLogin - login of the user
 * @param projectID - id of the project
 */
function aPreview(assets, userID, userLogin, projectID) {

	// Get URL to pdf file
	var pdfUrl = assets + '/datas/users/' + userID + '/' + projectID + '/preview/main.pdf?' + new Date().getTime();

	// If object exists show it
	if (objectExists(pdfUrl)) {
		$('#pdfPreview').html('<object data="' + pdfUrl + '"type="application/pdf" ' + 'id="pdfObject"></object>');
		$('#resultsPreview').fadeOut(100);
		$('#errorCard').fadeOut(100);
		$('#pdfPreview').fadeIn(1);
	}

	// If not exists show message
	else
	{
		$('#previewName').text(userLogin);
		$('#pdfPreview').fadeOut(1);
		$('#resultPreview').fadeIn(1);
		$('#nothingCard').fadeIn(1);
	}
}

/**
 * Set visibility to review elements
 * @param from - date from
 * @param to - date to
 */
function aToogleVisibilityReview(from, to) {

	var date = new Date();
	var fromDate = Date.parse(from);
	var toDate = Date.parse(to);

	// Remove wait dialog, show questions
	if (date.getTime() > fromDate)
		$('.typeReview').fadeIn();
	else
		$("#waitForReview").fadeIn();

	if (date.getTime() > toDate) {
		$(".contentR").attr('readonly', true);
		$(".qualityR").attr('readonly', true);
		$(".quantityR").attr('readonly', true);
	}
}

/**
 * Set visibility to feedback elements
 * @param from - date from
 * @param to - date to
 */
function aToogleVisibilityFeedback(from, to) {

	var date = new Date();
	var toDate = Date.parse(to);

	// Remove wait dialog, show questions
	if (date.getTime() > toDate)
		$('.typeFeedback').fadeIn();
	else
		$("#waitForFeed").fadeIn();


}

/**
 * Set visibility to final elements
 * @param from - date from
 * @param to - date to
 */
function aToogleVisibilityFinal(from, to) {

	var date = new Date();
	var fromDate = Date.parse(from);
	var toDate = Date.parse(to);

	// Remove wait dialog, show questions
	if (date.getTime() > fromDate)
		$('.typeFinal').fadeIn();
	else
		$("#waitForFinal").fadeIn();

	if (date.getTime() > toDate) {
		$(".contentE").attr('readonly', true);
		$(".qualityE").attr('readonly', true);
		$(".quantityE").attr('readonly', true);
	}
}

/**
 * Function to preview snapshot PDF
 * @param assets - assets path
 * @param userID - id of the user
 * @param userLogin - login for the user
 * @param projectID - id of the project
 * @param snapshot - snapshot file name
 */
function updateSnaphot(assets, userID, userLogin, projectID, snapshot) {

	// Get the filename or use current one
	var pdfUrl = "";
	if (snapshot)
		pdfUrl = assets + '/datas/users/' + userID + '/' + projectID + '/snapshots/' + snapshot + '.pdf?' + new Date().getTime();
	else
		pdfUrl = assets + '/datas/users/' + userID + '/' + projectID + '/preview/main.pdf?' + new Date().getTime();

	// If snapshot exists show it
	if (objectExists(pdfUrl)) {
		$('#pdfPreview').html('<object data="' + pdfUrl + '"type="application/pdf" ' + 'id="pdfObject"></object>');
		$('#nothingCard').fadeOut(1);
		$('#resultPreview').fadeOut(1);
		$('#pdfPreview').fadeIn(1);
	}

	// Display error message
	else {
		$('#previewName').text(userLogin);
		$('#pdfPreview').fadeOut(1);
		$('#resultPreview').fadeIn(1);
		$('#nothingCard').fadeIn(1);
	}
}

/**
 * Check if object exists
 * @param url - url for the object
 * @returns {boolean} - result
 */
function objectExists(url){

	var http = new XMLHttpRequest();
	http.open('HEAD', url, false);
	http.send();
	return http.status != 404;
}

/**
 * Function to expand all collapsible
 */
function aExpand() {
	if ($("#expanded").val() == 0) {
		$(".collapsible-header").addClass("active");
		$(".collapsible").collapsible({accordion: false});
		$("#expanded").val(1);
	}
	else {
		$(".collapsible-header").removeClass(function(){return "active";});
		$(".collapsible").collapsible({accordion: true});
		$(".collapsible").collapsible({accordion: false});
		$("#expanded").val(0);
	}
}