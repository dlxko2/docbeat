<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Editor
 */
class Editor extends CI_Controller {

	/**
	 * $userData variable.
	 * Stores session data for class
	 */
	private $userData;

	/**
	 * Editor constructor.
	 * Get session, set gettext and models
	 * Check if user is logged and project selected
	 */
    public function __construct() {

        parent::__construct();

	    $this->load->model("documentationget");
	    $this->load->model("editorfunc");
	    $this->load->model("editorsave");
	    $this->load->model("editorget");

	    $this->userData = $this->ss->userdata();
	    if (!array_key_exists('id', $this->userData) || !array_key_exists('selectedProject', $this->userData))
		    redirect('/welcome/index', 'location');
    }

	/**
	 * Main view of editor page
	 * Gets the data and save them in template
	 */
    public function index() {

    	// Get chapter data for editor
	    $chaptersData = $this->documentationget->getChaptersByProject($this->userData['selectedProject']);
	    // Get all pictures for user
	    $picturesData = $this->editorfunc->getPicturesByUser($this->userData);
		// Get first shown chapter to user
	    $actualChapter = $this->documentationget->getActualChapterID($this->userData);

	    // Process by Smarty
	    $this->sm->assign('actual',$actualChapter);
	    $this->sm->assign('pictures',$picturesData);
	    $this->sm->assign('chapters',$chaptersData);
	    $this->sm->assign('user',$this->userData);
        $this->sm->assign('index',base_url('index.php/'));
        $this->sm->assign('assets',base_url('assets/'));
        $this->sm->display('pages/editor/editor.tpl');
    }

	/**
	 * Function to get chapter content from editor
	 * @param $chapterID - integer : id of the chapter
	 */
	public function getChapter($chapterID) {

    	// Try to get data from local user data structure
	    $chapterContent = $this->editorfunc->getChapterByID($chapterID, $this->userData);

		// If nothing select the datas from the database
	    if ($chapterContent == false) {
		    $chapterData = $this->documentationget->getChapterByID($chapterID);
		    $chapterContent =  (empty($chapterData)) ?  "NA" : $chapterData["content"];
	    }

	    // Process the output with JSON
	    echo json_encode(['content' => $chapterContent]);
    }

	/**
	 * Function to save chapter content
	 * @param $chapterID - integer : id of the chapter
	 */
    public function saveChapter($chapterID) {

    	// Get chapter data for counting percent done
	    $chapterData = $this->documentationget->getChapterByID($chapterID);

	    // Save the content of the chapter into dir structure
    	$this->editorfunc->saveUserChapter($this->userData, $chapterID, $this->input->post('content'));

	    // Calculate and save chapter done number as percents
	    $this->editorsave->saveChapterDone($this->userData, $chapterID, $this->input->post('content'), $chapterData);

	    // Update the total done number for the user
	    $this->editorsave->saveTotalDone($this->userData);
    }

	/**
	 * Function to generate the PDF from the LateX sources
	 * @param string $type - type of the generation (P=Preview)
	 */
    public function preview($type = 'P') {

    	// Get data requested for the generation process
	    $documentationData = $this->documentationget->getInfoById($this->userData['selectedProject']);
	    $chaptersData = $this->documentationget->getChaptersByProject($this->userData['selectedProject']);

	    // Generate the preview with the output as PDF file
	    $result = $this->editorfunc->generatePreview($this->userData, $chaptersData, $documentationData, $type);

	    // Encode the result
	    echo json_encode($result);
    }

	/**
	 * Get the chapter done number as percents
	 * @param $chapterID - integer : id of the chapter
	 */
    public function getChapterDone($chapterID) {

    	// Get the number and encode it aj JSON
    	$done = $this->editorget->getDoneByChapter($chapterID, $this->userData);
	    echo json_encode(['done' => $done]);
    }

	/**
	 * Function to calculate the finishing students
	 * @param $chapterID - integer : id of the chapter
	 */
    public function getFinishingDone($chapterID) {

    	// Calculate the finishing number and encode the result as JSON
	    $finishing = $this->editorget->getFinishingByChapter($chapterID, $this->userData);
	    echo json_encode(['finishing' => $finishing]);
    }

	/**
	 * Function to save the stamp as time or characters
	 * @param $type - char : type of the stamp (C=chars,T=time)
	 */
    public function saveStamp($type) {

    	// Save time stamp or char stamp and encode the result as JSON
    	if ($type == 'T') $this->editorsave->saveTimeStack($this->userData);
	    if ($type == 'C') $this->editorsave->saveCharStack($this->userData);
	    echo json_encode(['result' => true]);
    }

	/**
	 * Function to calculate the lead percents
	 */
    public function getLead() {

    	// Depending on user data calculate the lead and return as JSON
    	$percent = $this->editorget->getLeadPercents($this->userData);
	    echo json_encode(['lead' => $percent]);
    }

	/**
	 * Function to get the literature for logged user
	 */
	public function getLiterature() {

		// Get the literature content and return as JSON
		$literatureContent = $this->editorfunc->getLiteratureByUser($this->userData);
		echo json_encode(['content' => $literatureContent]);
	}

	/**
	 * Function to save the literature
	 */
	public function saveLiterature() {

		$this->editorfunc->saveLiterature($this->userData, $this->input->post('content'));
	}

	/**
	 * Function to import the files from the dialog into the system
	 */
	public function import() {

		$this->editorfunc->addFiles($_FILES, $this->userData);
		redirect('/editor/index', 'location');
	}

	/**
	 * Function to remove the image from the dialog
	 */
	public function removeImage() {

		$picture = $this->input->post('image');
		$this->editorfunc->removeImage($picture, $this->userData);
	}

	public function upload() {
        $this->editorfunc->addPDFFiles($_FILES, $this->userData);
        redirect('/editor/index', 'location');
    }
}
