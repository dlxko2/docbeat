
<!-- WHEN 2 PEOPLE -->
{if isset($targetdata[0]) && isset($targetdata[1])}
<div class="row">

    <div class="col s5 center-align loginTextTopMargin">
        <b>{$targetdata[0].login}</b>
    </div>

    <div class="col s2 center-align">
        <i class="small material-icons cursorPointer" onclick="aSaveChanges()">save</i>
        <i class="small material-icons cursorPointer" onclick="aExpand()">view_stream</i>
        <input type="hidden" value="0" id="expanded">
    </div>

    <div class="col s5 center-align loginTextTopMargin">
        <b>{$targetdata[1].login}</b>
    </div>

</div>

<!-- WHEN 1 PEOPLE -->
{elseif isset($targetdata[0]) && !isset($targetdata[1])}
    <div class="col s5 center-align loginTextTopMargin">
        <b>{$targetdata[0].login}</b>
    </div>
    <div class="col s7 left-align">
        <i class="small material-icons cursorPointer" onclick="aSaveChanges()">save</i>
        <i class="small material-icons cursorPointer" onclick="aExpand()">view_stream</i>
        <input type="hidden" value="0" id="expanded">
    </div>
{/if}