{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/page_content/settings.tpl'}

<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/deadlines.css">

<div class="row">
    <div class="col s12">

        <form role="form" id="fDeadlines" method="post" enctype="multipart/form-data"
              accept-charset="utf-8" action="{$index}/deadlines/save">

        {include file='pages/deadlines/pagination.tpl'}
        {include file='pages/deadlines/add.tpl'}

        </form>

    </div>
</div>

{include file='pages/deadlines/script.tpl'}
{include file='includes/page_content/finish.tpl'}