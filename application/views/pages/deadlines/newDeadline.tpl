var header =
`<li>
 <div class="collapsible-header">
 <i class="material-icons">font_download</i>
  <span id="name` + deadlineNumber + `">` + name + `</span>
 <span class="secondary-content">`;

var deleter =
`<i class="material-icons">block</i>`;

var secondary =
`<input type=hidden name="iID[]" value="` + id + `">
 <input name="iType` + deadlineNumber + `" type="radio"
    value="U" id="deadlineRadio` + deadlineNumber + `_4" />
 <label for="deadlineRadio` + deadlineNumber + `_4">{ci_language line="None"}</label>
 <input name="iType` + deadlineNumber + `" type="radio"
    value="R" id="deadlineRadio` + deadlineNumber + `_1" />
 <label for="deadlineRadio` + deadlineNumber + `_1">{ci_language line="Review"}</label>
 <input name="iType` + deadlineNumber + `" type="radio"
    value="F" id="deadlineRadio` + deadlineNumber + `_2" />
 <label for="deadlineRadio` + deadlineNumber + `_2">{ci_language line="Feedback"}</label>
 <input name="iType` + deadlineNumber + `" type="radio"
    value="E" id="deadlineRadio` + deadlineNumber + `_3" />
 <label for="deadlineRadio` + deadlineNumber + `_3">{ci_language line="Finish"}</label>
 </span>
 </div>`;

var bodyDate =
`<div class="collapsible-body">
 <div class="row deadlineTopPadding">
 <div class="col s3">
 <label for="deadlineName` + deadlineNumber + `">{ci_language line="Deadline name"}</label>
 <input id="deadlineName` + deadlineNumber + `" type="text" class="validate"
        placeholder="{ci_language line="Enter name"}" value="` + name + `" name="iDeadlineName[]">
 </div>
 <div class="col s3 datePadding">
 <label for="datepicker">{ci_language line="Date"}</label>
 <input name="iSnapshot[]" type="date" class="datepicker"
    id="deadlineDate` + deadlineNumber + `" value="` + date + `">
 </div>`;

var bodyChapters =
`<div class="col s3 deadlineChapter">
 <div class="input-field col s12">
 <select multiple name="iChapters` + deadlineNumber + `[]" id="iChapters` + deadlineNumber + `">
 <option value="" disabled selected>{ci_language line="Select chapters"}</option>
 {foreach from=$chaptersdata item=chapter}
 <option value="{$chapter.id}">{$chapter.name}</option>
 {/foreach}
 </select>
 <label>{ci_language line="Snapshot chapters"}</label>
 </div>
 </div>`;

var bodyReview =
`<div class="col s3">
 <label for="datepicker2">{ci_language line="Review date"}</label>
 <input name="iReviewDate[]" type="date" class="datepicker"
    id="reviewDate` + deadlineNumber + `" value="` + review + `">
 </div>
 </div>
 </div>
 </li>`;