{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/page_content/settings.tpl'}

<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/reviews.css">

    <form role="form" id="fReviews" method="post" enctype="multipart/form-data"
          accept-charset="utf-8" action="{$index}/reviews/save">

        {include file='pages/reviews/pagination.tpl'}
        {include file='pages/reviews/content.tpl'}
        {include file='pages/reviews/import.tpl'}

    </form>

{include file='pages/reviews/script.tpl'}
<script src="{$assets}/{$smarty.const.scripts}/review.js"></script>
{include file='includes/page_content/finish.tpl'}
{include file='includes/packages/pkg_datatables.tpl'}
