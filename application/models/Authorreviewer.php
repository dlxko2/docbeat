<?php

/**
 * Class AuthorReviewer
 */
class AuthorReviewer extends CI_Model
{
	/**
	 * AuthorReviewer constructor.
	 */
	function __construct() {

		parent::__construct();
		$this->load->model("logmodel");
	}

	/**
	 * Save reviews from post data
	 * @param $postData - array : form post data
	 * @return bool - result of the operation
	 */
	public function saveResponds($postData)
	{
		foreach ($postData['iQuestion'] as $key => $question) {

			// Check input datas
			if (!isset($postData['iType'][$key])
				|| !isset($postData['iConnect'][$key])
				|| !isset($postData['iQuestion'][$key])
				|| !isset($postData['iReview'][$key])
				|| !isset($postData['iQuality'][$key])
				|| !isset($postData['iQuantity'][$key])
				|| !isset($postData['iOwner'][$key])) {
				$this->logmodel->lE("Wrong input:" . implode("|",$postData));
				return false;
			}

			// Create informations array
			$dbArray1 = [
				'type'     => $postData['iType'][$key],
				'connect'  => $postData['iConnect'][$key],
				'question' => $postData['iQuestion'][$key],
				'content'  => $postData['iReview'][$key],
				'quality'  => $postData['iQuality'][$key],
				'quantity' => $postData['iQuantity'][$key],
				'owner'    => $postData['iOwner'][$key]
			];

			// Look if result already exists
			$this->db->where('type', $postData['iType'][$key]);
			$this->db->where('connect', $postData['iConnect'][$key]);
			$this->db->where('question', $postData['iQuestion'][$key]);

			// If data not exists insert them in database
			if ($this->db->count_all_results('student_results') == 0) {
				$this->db->insert('student_results', $dbArray1);
				if ($this->db->affected_rows() < 1) {
					$this->logmodel->lE("Cant insert:" . implode("|", $dbArray1));
					return false;
				}
			}

			// If nothing exists update data in database
			else {
				$this->db->where('type', $postData['iType'][$key]);
				$this->db->where('connect', $postData['iConnect'][$key]);
				$this->db->where('question', $postData['iQuestion'][$key]);
				$this->db->update('student_results', $dbArray1);
			}
		}
		return true;
	}

	/**
	 * Get the admin mark
	 * @param $target  - int : affected user
	 * @param $project - int : current project
	 * @param $points - int : how many percents to best classmates
	 * @return boolean - result of operation
	 */
	public function updateMarkAdmin($target, $project, $points) {

		// Look for marks data
		$this->db->where('user', $target);
		$this->db->where('project', $project);
		$markData = $this->db->get('student_marks')->result();
		if (empty($markData)) return true;

		// Try to get first reviewer data
		$this->db->where('user', current($markData)->rewier1);
		$this->db->where('project', $project);
		$reviewer = $this->db->get('student_marks')->result();

		// Get ratio bonus 1-2, if nothing use 1 as 100%
		if (!empty($reviewer[0]))  $ratio1 = 1 + (((100 - intval($reviewer[0]->reviewer))/100) * REVIEWER_MODIFIER);
		else $ratio1 = 1;

		// Try to get second reviewer data
		$this->db->where('user', current($markData)->rewier2);
		$this->db->where('project', $project);
		$reviewer = $this->db->get('student_marks')->result();

		// Get ratio bonus 1-2, if nothing use 1 as 100%
		if (!empty($reviewer[0])) $ratio2 = 1 + (((100 - intval($reviewer[0]->reviewer))/100) * REVIEWER_MODIFIER);
		else $ratio2 = 1;

		// Calculate total results from reviews as:
		// ResultX = authorMark * reviewerRatio1 * reviewerRatio2
		// We have first result - maximum is 100%
		$resultAuthor = intval(current($markData)->author) * $ratio1 * $ratio2;
		if ($resultAuthor > 100) $resultAuthor = 100;

		// Create result as Result = avg(resultAuthor, reviewerMark)
		// We have second result
		$resultReviewer = ($resultAuthor + intval(current($markData)->reviewer))/2;

		// Make ratio from points - this is bonus value 1-2
		if ($points > 100) $points = 100;
		$ratioPoints = 1 + (($points/100) * POINTS_MODIFIER);

		// Update the result with points as:
		// ResultX = resultReviewer * ratioPotins
		// We have third result
		$resultPoints = $resultReviewer * $ratioPoints;
		if ($resultPoints > 100) $resultPoints = 100;

		// Update the result with done anti bonus as:
		// ResultX = resultPoints * ratioDone
		$resultTotal = $resultPoints * (intval(current($markData)->done)/100);

		// Update the database
		if (!$this->saveAdminInfo($project, $target, round($resultTotal))) return false;
		return true;
	}

	/**
	 * Save specified mark into database
	 * @param $project - int : current project
	 * @param $target - int : affected user
	 * @param $admin - int : admin mark
	 * @return boolean - result of function
	 */
	private function saveAdminInfo($project, $target, $admin) {

		// Prepare array with informations
		$dbArray = [
			'user'     => $target,
			'project'  => $project,
			'admin'    => $admin
		];

		// Look if result already exists
		$this->db->where('user', $target);
		$this->db->where('project', $project);

		// If nothing exists insert into database
		if ($this->db->count_all_results('student_marks') == 0) {
			$this->db->insert('student_marks', $dbArray);
			if ($this->db->affected_rows() < 1) {
				$this->logmodel->lE("Cant insert" . implode("|", $dbArray));
				return false;
			}

		}

		// If exists update in database
		else {
			$this->db->where('user', $target);
			$this->db->where('project', $project);
			$this->db->update('student_marks', $dbArray);
		}
		return true;
	}

	/**
	 * Get mark from the author's feedback
	 * ReviewerX mark = avg(avg(quality),avg(quantity))
	 * @param $connection - int : connection between source and taget
	 * @param $type : char : type of the result (R,F,E)
	 * @return int|bool - mark from the feedback or false
	 */
	public function getMarkSpecified($connection, $type) {

		// Found feedback results, if nothing its probably error
		$this->db->where('connect', $connection);
		$this->db->where('type', $type);
		$query = $this->db->get('student_results');
		if (empty($query)) {
			$this->logmodel->lE("No result feedback but saved:" . $connection . '|' . $type);
			return false;
		}

		// Prepare variables
		$QCQuality = 0;
		$QCQuantity = 0;
		$TQuality = 0;
		$TQuantity = 0;

		// For the each result count quality and quantity parameters
		foreach ($query->result() as $result) {

			// Get question connected to the result, if nothing produce error
			$this->db->where('id', $result->question);
			$quantityChapter = $this->db->get('questions')->result();
			if (empty($quantityChapter)) {
				$this->logmodel->lE("Question missing for result:" . $result->question);
				return false;
			}

			// If any quantity information found
			if (current($quantityChapter)->quantity > 0) {

				// Get chapter connected to question
				$this->db->where('id', current($quantityChapter)->quantity);
				$expectedQuantity = $this->db->get('chapters')->result();
				if (empty($expectedQuantity)) {
					$this->logmodel->lE("Chapter missing for question:" . current($quantityChapter)->quantity);
					return false;
				}

				// Count the numbers for the calculation
				$calculatedQuantity = $result->quantity / current($expectedQuantity)->extra_value;
				$TQuantity += ($calculatedQuantity > 100) ? 100 : $calculatedQuantity;
				++$QCQuantity;
			}

			// If any quality info in question count it
			if (current($quantityChapter)->quality > 0) {
				$TQuality += $result->quality / 10;
				++$QCQuality;
			}
		}

		// If we calculate quality and quantity together
		if ($QCQuality > 0 && $QCQuantity > 0)
			return round((($TQuality / $QCQuality) + ($TQuantity / $QCQuantity)) / 2 * 100);

		// If only quality found
		if ($QCQuality > 0 && $QCQuantity < 1)
			return round($TQuality / $QCQuality * 100);

		// If only quantity found
		if ($QCQuality < 1 && $QCQuantity > 0)
			return round($TQuantity / $QCQuantity * 100);

		return 0;
	}
}