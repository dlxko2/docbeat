<?php

/**
 * Class DeadlinesSave
 */
class DeadlinesSave extends CI_Model
{
	/**
	 * DeadlinesSave constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->load->model("logmodel");
	}

	/**
	 * Save deadlines using form post datas
	 * @param $postData - array : data from the post
	 * @param $projectID - int : project ID
	 * @return boolean - the result of the operation
	 */
	public function saveDeadlines($postData, $projectID) {

		// Check the input
		if (empty($projectID) || empty($postData)) {
			$this->logmodel->lE('Wrong post or project id input:' . implode('|', $postData) . '/' . $projectID);
			return false;
		}

		// Check if any deadline in list and delete old data
		if (!array_key_exists('iSnapshot', $postData)){
			$this->db->delete('deadlines', ['project' => $projectID]);
			$this->db->delete('deadline_chapter', ['project' => $projectID]);
			return true;
		}

		// Delete old chapters which won't be updated
		$this->db->where_not_in('id', $postData["iID"]);
		$this->db->delete('deadlines', ['project' => $projectID]);
		$this->db->delete('deadline_chapter', ['project' => $projectID]);

		// For each deadline make DB processes
		foreach ($postData['iSnapshot'] as $key => $snapshot) {

			// Format dates
			$snapshotFormated = new DateTime($snapshot);
			$reviewEnd = new DateTime($postData['iReviewDate'][$key]);

			// Get the type value
			if (isset($postData['iType' . $key])) $type = $postData['iType' . $key];
			else $type = '';

			// Get the type value
			if (!isset($postData['iDeadlineName'][$key]) || empty($postData['iDeadlineName'][$key]))
				$name = 'Unkown';
			else
				$name = $postData['iDeadlineName'][$key];


			// Prepare array for deadline
			$deadlinesData = [
				'snapshot'    => $snapshotFormated->format('Y-m-d H:i:s'),
				'project'     => $projectID,
				'review_type' => $type,
				'review_end'  => $reviewEnd->format('Y-m-d H:i:s'),
				'name'        => $name
			];

			// Create or update deadline
			if (empty($postData['iID'][$key])) {
				$this->db->insert('deadlines', $deadlinesData);
				$deadlineDBID = $this->db->insert_id();
			}
			else {

				// Try to get the deadline from the ID
				$this->db->where('id', $postData["iID"][$key]);
				$res = $this->db->get('deadlines')->result();
				if (empty($res) || !is_array($res)) {
					$this->logmodel->lE("Can't find the deadline:" . $postData["iID"][$key]);
					return false;
				}

				// Update with the ID
				$deadlineDBID = current($res)->id;
				$this->db->where('id', $deadlineDBID);
				$this->db->update('deadlines', $deadlinesData);
			}

			// Insert chapter connections into DB
			if (isset($postData['iChapters' . $key])) {
				foreach ($postData['iChapters' . $key] as $chapter) {
					$this->db->insert('deadline_chapter', [
						'deadline' => $deadlineDBID,
						'chapter'  => $chapter,
						'project'  => $projectID
					]);
				}
			}
		}

		// Return the default result
		return true;
	}
}