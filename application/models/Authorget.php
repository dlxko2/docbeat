<?php

/**
 * Class AuthorGet
 */
class AuthorGet extends CI_Model
{
	/**
	 * AuthorGet constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->load->model("logmodel");
	}

	/**
	 * Get results depending on connection between students
	 * Select students from with connections and return result
	 * @param $connectionsArray - array : connection ids
	 * @return array - results data
	 */
	public function getResultByConnectionsArray($connectionsArray) {

		// Check if any connection array
		if (empty($connectionsArray)) {
			$this->logmodel->lW("No connection array for results");
			return [];
		}

		// Get results with the selected connection array
		$this->db->where_in('connect', $connectionsArray);
		$query = $this->db->get('student_results');
		$result = [];

		// Create the result and return it
		foreach ($query->result() as $row)
			array_push($result,(array)$row);
		return $result;
	}

	/**
	 * Get mark info for the selected user
	 * @param $userData - array : the data for the logged user
	 * @return array - result or default data
	 */
	public function getMarkInfoByUser($userData) {

		// Get mark data for the user
		$this->db->where('user', $userData['id']);
		$query = $this->db->get('student_marks')->result();

		// Check if any result and return it or default values
		if (!empty($query)) return (array)current($query);
		return [];
	}
}