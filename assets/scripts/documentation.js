/**
 * Preview current documentation as PDF
 * @param index - string : path to index page
 * @param assets - string : path to assets
 * @param project - int : project id
 */
function aPreview(index, assets, project) {

	// Create URL
    var pdfUrl = assets + '/datas/projects/' + project + '/preview/main.pdf?' + new Date().getTime();

	$.ajax
    ({
        url: index + '/documentation/preview',
        dataType: 'json',
        type: 'GET',
	    error: function() {alert("Can't update the preview!")},
	    success: function(data)
        {
        	// If preview ok
	        if (data.result == true) {
		        $('#pdfPreview').html('<object data="' + pdfUrl + '"type="application/pdf" ' + 'id="pdfObject"></object>');
		        $('#resultPreview').fadeOut(1);
		        $('#pdfPreview').fadeIn(1);
	        }

	        // If sth failed
	        else {
		        $('#nothingCard').fadeOut(1);
		        $('#pdfPreview').fadeOut(1);
		        $('#errorCard').fadeIn(1);
		        $('#resultPreview').fadeIn(1);
		        $('#errorText').html(data.content);
	        }
        }
    });
}

/**
 * Get list of project in system
 * @param index - string : path to index
 */
function getImportProjects(index) {

	// Clear the table before import
	$('#tDocProjects').dataTable().fnClearTable();

    $.ajax
    ({
        url: index + '/documentation/getImportProjects',
        dataType: 'json',
        type: 'GET',
        error: function () {alert("Can't get projects for import!")},
        success: function (data) {

        	// Add the rows into the table
            data.forEach(function (entry) {
                $('#tDocProjects').dataTable().fnAddData([
                    entry.name,
                    entry.user,
                    '<i class="material-icons" onclick="aShowChapters(\'' +
                    index + '\',' + entry.id + ')">open_in_browser</i>']
                );
            });
        }
    });
}

/**
 * Get all chapters for project for import
 * @param index - string : path to index
 * @param projectID - int : id of the project
 */
function getImportChapters (index, projectID) {

	// Clear table before import
	$('#tDocChapters').dataTable().fnClearTable();

	$.ajax
	({
		url: index + '/documentation/getImportChapters/' + projectID,
		dataType: 'json',
		type: 'GET',
		error: function() {alert(index + '/documentation/getImportChapters/' + projectID)},
		success: function(data)
		{
			// Add rows for each chapter
			data.forEach(function(entry) {
				$('#tDocChapters').dataTable().fnAddData( [
					entry.name,
                    '<i class="material-icons" onclick="aCreateChapter('
                    + entry.id + ',\''
                    + entry.name + '\','
                    + entry.words + ','
                    + entry.cites + ',\''
                    + entry.extra + '\','
                    + entry.extra_value + ',\''
                    + entry.content + '\','
                    + entry.hidden
                    + ')">open_in_browser</i>']
				);
			});
		}
	});
}

/**
 * Save documentation form
 */
function aSaveChanges() {
	$(":input").attr('disabled', false);
	$( "#fDocumentation" ).submit();
}