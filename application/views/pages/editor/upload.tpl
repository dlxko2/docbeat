<div id="mUpload" class="modal">
    <div class="modal-content">

        <form role="form" id="fUpload" method="post" enctype="multipart/form-data"
              accept-charset="utf-8" action="{$index}editor/upload">

            <div class="col s9">
            <div class="file-field input-field">
                <div class="btn fileMargin">
                    <span>{ci_language line="File"}</span>
                    <input type="file" name="iUploadFiles[]" multiple>
                </div>
                <div class="file-path-wrapper">

                        <input class="file-path validate" type="text" placeholder="{ci_language line="Upload one or more files"}">
                </div>
            </div>

            </div>
            <div class="col s3" style="margin-top: 23px">
            <button class="waves-effect waves-light btn" type="submit">Upload</button>
</div>

        </form>

    </div>
    </div>