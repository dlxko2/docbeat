<!-- DOCUMENT START INCLUDES -->
{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/page_content/settings.tpl'}

<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/documentation.css">

<!-- CONTENT -->
<div class="row" id="mainRow">

    <!-- EDIT VIEW -->
    <div class="col s6 editContainer">

        <form role="form" id="fDocumentation" method="post" enctype="multipart/form-data"
              accept-charset="utf-8" action="{$index}/documentation/save">

        {include file='pages/documentation/pagination.tpl'}
        {include file='pages/documentation/step1.tpl'}
        {include file='pages/documentation/step2.tpl'}
        {include file='pages/documentation/step3.tpl'}
        {include file='pages/documentation/import.tpl'}

        </form>

    </div>

    <!-- PDF VIEW -->
    <div class="col s6" id="pdfContainer" style="background-color: #525659">
        {include file='pages/documentation/preview.tpl'}
    </div>

</div>

<!-- DOCUMENT END INCLUDES -->
{include file='pages/documentation/script.tpl'}
<script src="{$assets}/{$smarty.const.scripts}/documentation.js"></script>
{include file='includes/page_content/finish.tpl'}
{include file='includes/packages/pkg_datatables.tpl'}