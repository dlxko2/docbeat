<?php

/**
 * Class ClasseFunc
 */
class ClasseFunc extends CI_Model
{

	/**
	 * ClasseFunc constructor.
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Function to pair students into the pairs
	 * @param $users - array : all the users
	 * @param $projectID - integer : id of the project
	 * @TODO - randomize selection of oponent
	 */
	public function createPairs($users, $projectID) {

		// Insert users into new variable
		$myUsers = $users;

		// For each user with update setting
		foreach ($myUsers as &$user) {

			// For each user for selected user with update setting
			$found = false;
			foreach ($myUsers as &$myUser) {

				// If something missing and not the same in more parameters
				if ($myUser['flag2'] == false
					&& $myUser['id'] != $user['id']
					&& $myUser['team'] != $user['team']
					&& $myUser['id'] != $user['r1']
					&& $myUser['id'] != $user['r2']) {

					// Connect
					$user['r1'] = $myUser['id'];
					if ($myUser['mr1'] == 0) $myUser['mr1'] = $user['id'];
					else $myUser['mr2'] = $user['id'];

					if ($myUser['mr1'] != 0 && $myUser['mr2'] != 0)
						$myUser['flag2'] = true;

					echo "Connection " . $user['id'] . "->" . $myUser['id'] . "<br>";
					$found = true; break;
				}
			}

			// Again for each user for selected user
			foreach ($myUsers as &$myUser) {

				// If something missing and not the same in more parameters
				if ($myUser['flag2'] == false
					&& $myUser['id'] != $user['id']
					&& $myUser['team'] != $user['team']
					&& $myUser['id'] != $user['r1']
					&& $myUser['id'] != $user['r2']) {

					$user['r2'] = $myUser['id'];
					if ($myUser['mr1'] == 0) $myUser['mr1'] = $user['id'];
					else $myUser['mr2'] = $user['id'];

					if ($myUser['mr1'] != 0 && $myUser['mr2'] != 0)
						$myUser['flag2'] = true;

					echo "Connection " . $user['id'] . "->" . $myUser['id']  . "<br>";
					$found = true; break;
				}
			}

			// Update the flag
			$user['flag1'] = $found;
		}

		// Prepare variables for not completed
		$notComplete1 = 0;
		$notComplete2 = 0;
		foreach ($myUsers as $compUser) {
			if ($compUser['flag1'] == false) $notComplete1 += 1;
			if ($compUser['flag2'] == false) $notComplete2 += 1;
		}

		// Write the result
		echo "Don't have someone to review:" . $notComplete1 . "<br>";
		echo "Don't have reviewer:" . $notComplete2 . "<br>";

		// Delete all connections already created and process again
		$this->db->delete('student_connect', ['project' => $projectID]);
		foreach ($myUsers as $conUser) {

			// If completed
			if ($conUser['flag1'] == true) {

				// If not exists insert first
				$this->db->where('author', $conUser['r1']);
				$this->db->where('reviewer', $conUser['id']);
				$this->db->where('project', $projectID);
				if ($this->db->count_all_results('student_connect') == 0)
					$this->db->insert('student_connect', [
						'author' => $conUser['r1'],
						'reviewer' => $conUser['id'],
						'project' => $projectID]);

				// If not exists insert second
				$this->db->where('author', $conUser['r2']);
				$this->db->where('reviewer', $conUser['id']);
				$this->db->where('project', $projectID);
				if ($this->db->count_all_results('student_connect') == 0)
					$this->db->insert('student_connect', [
						'author' => $conUser['r2'],
						'reviewer' => $conUser['id'],
						'project' => $projectID]);
			}

			// If completed
			if ($conUser['flag2'] == true) {

				// If not exists insert first
				$this->db->where('author', $conUser['id']);
				$this->db->where('reviewer', $conUser['mr1']);
				$this->db->where('project', $projectID);
				if ($this->db->count_all_results('student_connect') == 0)
					$this->db->insert('student_connect', [
						'author' => $conUser['id'],
						'reviewer' => $conUser['mr1'],
						'project' => $projectID]);

				// If not exists insert second
				$this->db->where('author', $conUser['id']);
				$this->db->where('reviewer', $conUser['mr2']);
				$this->db->where('project', $projectID);
				if ($this->db->count_all_results('student_connect') == 0)
					$this->db->insert('student_connect', [
						'author' => $conUser['id'],
						'reviewer' => $conUser['mr2'],
						'project' => $projectID]);
			}
		}

		// Write the result
		echo "<pre>";
		var_dump($myUsers);
		echo "</pre>";
	}

	/**
	 * Function to delete the cache for class SQL query
	 */
	public function regenerate() {
		$cacheDir = APPPATH . 'cache/classe+index';
		exec('rm -R -f ' . $cacheDir);
	}
}