<!-- PDF preview -->
<div id="pdfPreview"></div>

<!-- RESULT -->
<div id="resultPreview">

    <!-- NOTHING -->
    <div class="card blue-grey darken-1 cardMargin" id="nothingCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="No PDF was generated"}</span>
            <p>{ci_language line="Please run the preview function to show some!"}</p>
        </div>
    </div>

    <!-- ERROR -->
    <div class="card blue-grey darken-1 cardMargin" id="errorCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="Error while processing"}</span>
            <p id="errorText"></p>
        </div>
    </div>

</div>