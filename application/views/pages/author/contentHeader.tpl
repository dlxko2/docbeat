{if $question.type != 'F'}
<span class="secondary-content">

    <!-- FIRST RESULTS -->
    <div class="chip teal lighten-2">
        {if $quality1 == 'N/A' || $question.quality == 0} -
        {else} {math equation="round(z)" z=$quality1} {/if}
        /
        {if $quantity1 == 'N/A' || $quantity1 == null} -
        {elseif $question.quantity != 0 && $extraValue != 0}
            {math assign="quantityPercent" equation='round(o/p*10)' o=$quantity1 p=$extraValue}
            {if $quantityPercent > 10}{$quantityPercent = 10}{/if}
            {$quantityPercent}
        {else} - {/if}
    </div>

    <!-- SECOND RESULTS -->
    {if $targets == 2}
        <div class="chip teal accent-3">
            {if $quality2 == 'N/A' || $question.quality == 0} -
            {else} {math equation="round(z)" z=$quality2} {/if}
            /
            {if $quantity2 == 'N/A' || $quantity1 == null} -
            {elseif $question.quantity != 0 && $extraValue != 0}
                {math assign="quantityPercent" equation='round(o/p*10)' o=$quantity2 p =$extraValue}
                {if $quantityPercent > 10}{$quantityPercent = 10}{/if}
                {$quantityPercent}
            {else} - {/if}
        </div>
    {/if}

</span>
{/if}