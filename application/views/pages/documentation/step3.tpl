<div id="step3" class="step3Container">
    <div class="row">
        <ul class="collapsible" data-collapsible="accordion" id="chapterAppendContainer">

            <!-- ADDING ELEMENT -->
            <li>
                <div class="collapsible-header">
                    <span onclick="aCreateChapter(0, '{ci_language line="New chapter"}', 0, 0, '', 0, '', 0, 0)">
                        <i class="material-icons">plus_one</i>{ci_language line="Add chapter"}
                    </span>
                    <span class="secondary-content" onclick="aImportChapter('{$index}')">
                        <a class="waves-effect waves-light btn">{ci_language line="Import"}</a>
                    </span>
                </div>
            </li>

        </ul>
    </div>
</div>
