<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentation extends CI_Controller {

	/**
	 * $userData variable.
	 * Stores session data for class
	 */
    private $userData;

	/**
	 * Documentation constructor.
	 * Get session, set gettext and models
	 * Check if user is logged and project selected
	 */
    public function __construct() {

        parent::__construct();

        $this->load->model("documentationsave");
        $this->load->model("documentationget");
        $this->load->model("documentationfunc");
	    $this->load->model("deadlinesget");

	    $this->userData = $this->ss->userdata();
	    if (!array_key_exists('id', $this->userData) || !array_key_exists('selectedProject', $this->userData))
		    redirect('/welcome/index', 'location');
    }

	/**
     * Main view of documentation page
	 * Gets the data and save them in template
     */
    public function index() {

    	// Get informations about documentation
        $documentationData = $this->documentationget->getInfoById($this->userData['selectedProject']);
	    // Get informations about chapters
        $chaptersData = $this->documentationget->getChaptersByProject($this->userData['selectedProject']);
	    // Get informations about project files
        $projectFiles = $this->documentationfunc->getProjectFiles($this->userData['selectedProject']);
		// Get all chapters in deadlines
	    $chaptersInDeadlines = $this->deadlinesget->getDeadlinesChaptersAll();

	    // Process by Smarty
	    $this->sm->assign('deadchapters',$chaptersInDeadlines);
        $this->sm->assign('files',$projectFiles);
        $this->sm->assign('chapters',$chaptersData);
        $this->sm->assign('project',$documentationData);
        $this->sm->assign('user',$this->userData);
        $this->sm->assign('index', base_url('index.php/'));
        $this->sm->assign('assets', base_url('assets/'));
        $this->sm->display('pages/documentation/documentation.tpl');
    }

	/**
     * Save project function
	 * Get post data and save separated info
     */
    public function save() {

    	// Get the post data
        $postData = $this->input->post();

	    // Independenly save every kind of informations
        $this->documentationsave->saveInfo($postData);
	    $this->documentationsave->saveChapters($postData);
        $this->documentationfunc->addFiles($_FILES, $this->userData['selectedProject']);
        redirect('/documentation/index', 'location');
    }

	/**
     * Function when required preview
	 * Get info about project and create new PDF
     */
    public function preview() {

    	// Get the requested data for documentation a chapters
        $documentationData = $this->documentationget->getInfoById($this->userData['selectedProject']);
        $chaptersData = $this->documentationget->getChaptersByProject($this->userData['selectedProject']);

	    // Create the preview and return the result
	    $result = $this->documentationfunc->createPreview($this->userData, $documentationData, $chaptersData);
	    echo json_encode($result);
    }

	/**
     * Function when required project lock
	 * Update project lock info in database
     */
    public function lock() {

        $lockStatus = $this->documentationsave->updateLock($this->userData['selectedProject']);
	    echo json_encode($lockStatus);
    }

	/**
     * Import chapter - project table webservice
	 * Get all project in DB and create JSON
     */
    public function getImportProjects() {

        $projectsData = $this->documentationget->getProjectsAll();
        echo json_encode($projectsData);
    }

	/**
	 * Import chapter - chapter table webservice
	 * Get all chapters from database and create JSON output
     * @param $projectID - int : the id of the project
     */
    public function getImportChapters($projectID) {

        $chaptersData = $this->documentationget->getChaptersByProject($projectID);
        echo json_encode($chaptersData);
    }
}
