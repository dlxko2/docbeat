<div id="pdfPreview">
    <div class="card blue-grey darken-1 cardMargin" id="nothingCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="Please select the snapshot to preview"}</span>
        </div>
    </div>
</div>

<div id="resultPreview">
    <div class="card blue-grey darken-1 cardMargin" id="nothingCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="No PDF was generated for"} <span id="previewName">N/A</span> </span>
            <p>{ci_language line="Please wait for user to generate one!"}</p>
        </div>
    </div>
</div>