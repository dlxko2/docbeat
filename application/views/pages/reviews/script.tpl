{literal}
<script>

    /**
     * Variable to store review number
     * @type {number}
     */
    var reviewNumber = 0;

    /**
     * Webpage constructor function
     * Initialize tables and create reviews
     */
    $(document).ready(function() {

        // Update topbar
        $('#topReviews').css('color', 'red');

        // Initialize import tables
        $('.tRewProjects, .tRewQuestions').dataTable({
            "oLanguage": {
                "sStripClasses": "",
                "sSearch": "",
                "sSearchPlaceholder": "{/literal}{ci_language line="Enter name here"}{literal}",
                "sInfo": "_START_ -_END_ {/literal}{ci_language line="of"}{literal} _TOTAL_"
            },
            bAutoWidth: false,
            "pageLength": 5
        });

        // Update the lock
        updateLock();

        // Create questions
        {/literal}{foreach from=$questionsdata item=question}
         aCreateQuestion(
            {$question.id},
           '{$question.content}',
            {$question.quality},
            {$question.quantity},
           '{$question.type}',
            {$project.locked});
        {/foreach}{literal}
    });

    /**
     * Show import question dialog
     * @param index - string : path to index
     */
    function aImportQuestion(index) {
        $('#questionListContainer').fadeOut(1);
        $('#projectListContainer').fadeIn(300);
        $('#mImport').openModal();
        getImportProjects(index);
    }

    /**
     * Show questions to be selected
     * @param index - string : path to index
     * @param projectID - int : id of the project
     */
    function aShowQuestions(index, projectID){
        $('#projectListContainer').fadeOut(1);
        $('#questionListContainer').fadeIn(300);
        getImportQuestions(index, projectID);
    }

    /**
     * Delete question from the list
     * @param questionNumber - int : id of the question
     */
    function aDeleteQuestion(questionNumber){
        var divName = '#question' + questionNumber;
        $(divName).remove();
    }

    /**
     * Save reviews form
     */
    function aSaveChanges(){
        $( "#fReviews" ).submit();
    }

    /**
     * Create new question
     * @param id - int : id of the question
     * @param content - string : content of question
     * @param quality - int : quality number
     * @param quantity - int : quantity number
     */
    function aCreateQuestion(id, content, quality, quantity, type, locked) {

        // Process the HTML
        {/literal}{include file='pages/reviews/newReview.tpl'}{literal}

        // Save new element
        if (locked) $('#queRAppend').append(header + headerDelL + headerSec + bodyContent + bodyQuantity );
        else $('#queRAppend').append(header + headerDel + headerSec + bodyContent + bodyQuantity );

        // Create the trigers
        createQuestionTriggers(quality, quantity, type, locked);
    }

    /**
     * Function to create triggers to question
     * @param quality - quality value
     * @param quantity - quantity value
     * @param type - type
     * @param locked - if locked
     */
    function createQuestionTriggers (quality, quantity, type, locked) {

        // Trigger question content
        var iconElement = '#questionIcon' + reviewNumber;
        var nameElement = '#name' + reviewNumber;
        var qualityElement = '#quality' + reviewNumber;
        var qualityHElement = '#qualityH' + reviewNumber;
        var quantityElement = '#quantity' + reviewNumber;
        var areaElement = '#areaContent' + reviewNumber;
        var radio = 'input[name="iType' + reviewNumber + '"]';

        // Update ratio buttons
        if (type != 0) $(radio).val([type]);

        // Update quality
        $(qualityElement).change(function() {
            if(($(qualityElement).prop('checked'))) $(qualityHElement).val("1");
            else $(qualityHElement).val("0");
        });

        // Update quality
        if (quality != 0) $(qualityElement).attr('checked','checked');

        // Update quantity
        $(quantityElement).val(quantity).material_select();

        // Trigger question check - name
        if($(areaElement).val().length > 0) {
            $(iconElement).css('color', 'green');
            var text = $(areaElement).val();
            $(nameElement).text(text.substring(0,100) + '...');
        }
        else {
            $(iconElement).css('color', 'red');
            $(nameElement).text('{/literal}{ci_language line="New question"}{literal}');
        }

        // Trigger question check - content
        $('#areaContent' + reviewNumber).on('input', function () {
            if($(this).val().length > 0) {
                $(iconElement).css('color', 'green');
                $(nameElement).text($(this).val());
            }
            else {
                $(iconElement).css('color', 'red');
                $(nameElement).text('{/literal}{ci_language line="New question"}{literal}');
            }
        });

        // Update locked
        if (locked) {
            $(radio).attr('disabled', true);
            $(qualityElement).attr('disabled', true);
        }

        // Set number for next question
        ++reviewNumber;
    }

{/literal}
</script>