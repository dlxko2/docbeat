<!-- FOOTER -->
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <span id="copyRight">© 2016 Martin Pavelka</span>
            <span id="version">{ci_language line="Version"} {$smarty.const.VERSION}
                {if isset($commit)}
                    <span id="commitText">( {$commit} )</span>
                {/if}
            </span>
        </div>

    </div>
</footer>