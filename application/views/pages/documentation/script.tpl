<script>
{literal}

    /**
     * Variable stores chapter number counter
     */
    var chapterNumber = 0;

    /**
     * Webpage constructor function
     * Initialize tables, checker and create chapters elements from DB
     */
    $(document).ready(function() {

        // Update topbar
        $('#topDocumentation').css('color', 'red');

        // Initialize the table  for chapters and projects
        $('#tDocProjects, #tDocChapters').dataTable({
            "oLanguage": {
                "sStripClasses": "",
                "sSearch": "",
                "sSearchPlaceholder": "{/literal}{ci_language line="Enter name here"}{literal}",
                "sInfo": "_START_ -_END_ {/literal}{ci_language line="of"}{literal} _TOTAL_",
            },
            bAutoWidth: false,
            "pageLength": 5
        });

        // Create chapters from DB
        {/literal}{foreach from=$chapters item=chapter}

            // Check if not in deadlines
            {assign var="deadlined" value='0'}
            {foreach from=$deadchapters item=deaded}{if $deaded == $chapter.id} {$deadlined = 1} {/if}{/foreach}

            // Fix for the backslash character
            var content = String.raw`{$chapter.content}`;

            // Create new chapter
            aCreateChapter({$chapter.id}, '{$chapter.name}',{$chapter.words},{$chapter.cites}, '{$chapter.extra}',
                           {$chapter.extra_value}, content,{$chapter.hidden},{$project.locked},{$deadlined});
        {/foreach}{literal}

        // Initialize timer for checking correct datas
        /*saveDocumentation(chapterNumber);
        setInterval(function () {saveDocumentation(chapterNumber);}, 2000);*/
        updateLock();

        // Update container size for preview
        var window_size = $(window).height();
        $('#pdfContainer').height(window_size);

        // Project start picker
        $('.datepicker').pickadate({selectMonths: true, selectYears: 15});
    });

    /**
     * Information view
     * Reload data for info view
     */
    function aStep1() {
        $('#step3').fadeOut(1);
        $('#step2').fadeOut(1);
        $('#step1').fadeIn(500);
        $( "#pStep1" ).addClass( "active" ).removeClass("waves-effect" );
        $( "#pStep2" ).removeClass( "active" ).addClass( "waves-effect" );
        $( "#pStep3" ).removeClass( "active" ).addClass( "waves-effect" );
    }

    /**
     * Latex view
     * Reload data for latex view
     */
    function aStep2() {
        $('#step3').fadeOut(1);
        $('#step1').fadeOut(1);
        $('#step2').fadeIn(500);
        $("#pStep2").addClass("active").removeClass("waves-effect");
        $("#pStep1").removeClass("active").addClass("waves-effect");
        $("#pStep3").removeClass("active").addClass("waves-effect");
    }

    /**
     * Chapters view
     * Reload data for chapters view
     */
    function aStep3() {
        $('#step2').fadeOut(1);
        $('#step1').fadeOut(1);
        $('#step3').fadeIn(500);
        $( "#pStep3" ).addClass( "active" ).removeClass("waves-effect" );
        $( "#pStep1" ).removeClass( "active" ).addClass( "waves-effect" );
        $( "#pStep2" ).removeClass( "active" ).addClass( "waves-effect" );
    }

    /**
     * Open chapter import dialog
     * Get data as ajax request
     */
    function aImportChapter(index) {
        $('#chapterListContainer').fadeOut(1);
        $('#projectListContainer').fadeIn(300);
        $('#mImport').openModal();
        getImportProjects(index);
    }

    /**
     * Open chapter select dialog
     * Get data as ajax request
     */
    function aShowChapters(index, projectID){
        $('#projectListContainer').fadeOut(1);
        $('#chapterListContainer').fadeIn(300);
        getImportChapters(index, projectID);
    }

    /**
     * Delete the chapter from the list
     */
    function aDeleteChapter(chapterNumber){
        var divName = '#chapter' + chapterNumber;
        $(divName).remove();
    }

    /**
     * Create new chapter element in chapters view
     * @param dId - int : id of the chapter
     * @param dName - string : chapter name
     * @param dWords - int : number of expected words
     * @param dCites - int : number of expected cites
     * @param dExtra - string : extra parameter name
     * @param dExtraValue - int : extra parameter number
     * @param dContent - string : chapter content
     * @param dHidden - char : if hidden
     * @param dLocked - boolean : if project locked
     * @param deadlined - boolean : if in deadlines
     */
    function aCreateChapter(dId, dName, dWords, dCites, dExtra, dExtraValue, dContent, dHidden, dLocked, deadlined) {

        // Create the HTML part
        {/literal}{include file='pages/documentation/newChapter.tpl'}{literal}

        // Create the element from fragments
        if (dLocked || deadlined) $('#chapterAppendContainer').append(header + secHeaderL +bodyName + bodyExpect + bodyExtra + bodyContent);
        else $('#chapterAppendContainer').append(header + secHeader + bodyName + bodyExpect + bodyExtra + bodyContent);

        // Create the javascript part
        createChapterTriggers(dLocked);
    }

    /**
     * Create triggers and update elements on new chapter element
     */
    function createChapterTriggers (locked) {

        // Focus on elements
        var nameElement = '#name' + chapterNumber;
        var iconElement = '#chapterIcon' + chapterNumber;
        var inputNameElement = '#iChapterName' + chapterNumber;
        var contentElement = '#iAreaContent' + chapterNumber;
        var moodElement = '#mood' + chapterNumber;
        var hiddenElement = '#iHidden' + chapterNumber;
        var metric = '#iSliderMetric' + chapterNumber;
        var cites = '#iSliderCites' + chapterNumber;
        var words = '#iSliderWords' + chapterNumber;
        var metricname = '#iMetricName' + chapterNumber;


        // Trigger chapter name update
        $('#iChapterName' + chapterNumber).on('input', function () {
            if($(this).val().length > 0)
                $(nameElement).text($(this).val());
            else
                $(nameElement).text('{/literal}{ci_language line="New chapter"}{literal}');

            if($(this).val().length > 0 && $(contentElement).val().length > 0)
                $(iconElement).css('color', 'green');
        });

        // Trigger content update
        $('#iAreaContent' + chapterNumber).on('input', function () {
            if($(this).val().length > 0 && $(inputNameElement).val().length > 0)
                $(iconElement).css('color', 'green');
        });

        // Check already added data
        if($(inputNameElement).val().length > 0 && $(inputNameElement).val().length > 0)
            $(iconElement).css('color', 'green');

        // Update hidden
        if ($(hiddenElement).val() != '0')
            $(moodElement).attr('checked','checked');

        // Update hidden mood
        $(moodElement).change(function() {
            if (($(moodElement).prop('checked'))) $(hiddenElement).val("1");
            else $(hiddenElement).val("0");
        });

        // Update locked
        if (locked) {
            $(moodElement).attr('disabled', true);
            $(metric).attr('disabled', true);
            $(cites).attr('disabled', true);
            $(words).attr('disabled', true);
            $(metricname).attr('disabled', true);
            $(contentElement).attr('disabled', true);
        }

        // Set counter for next chapter
        ++chapterNumber;
    }

</script>
{/literal}