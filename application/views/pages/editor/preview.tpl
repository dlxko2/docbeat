<div id="pdfPreview">

</div>

<div id="resultPreview">
    <div class="card blue-grey darken-1 cardMargin" id="nothingCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="No PDF was generated"}</span>
            <p>{ci_language line="Please run the preview function to show some!"}</p>
        </div>
    </div>

    <div class="card blue-grey darken-1 cardMargin" id="errorCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="Error while processing"}</span>
            <p id="errorText">Lorem ipsum</p>
        </div>
</div>

