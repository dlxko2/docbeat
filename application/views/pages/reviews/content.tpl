<!-- QUESTION LIST -->
<div class="col s12">
    <div class="row">
        {if empty($questionsdata) && $project.locked == 1}
            <h2 class="center-align">{ci_language line="There isn't any review and project locked!"}</h2>
            <h3 class="center-align">{ci_language line="Please unlock the project to add chapter."}</h3>
        {else}
        <ul class="collapsible" data-collapsible="accordion" id="queRAppend">
            <li>
                {if $project.locked == 0}
                <div class="collapsible-header">
                    <span onclick="aCreateQuestion(0, '', 0, 0, 'R', 0)">
                        <i class="material-icons">plus_one</i>
                        {ci_language line="Add question"}
                    </span>
                    <span class="secondary-content" onclick="aImportQuestion('{$index}')">
                        <a class="waves-effect waves-light btn">{ci_language line="Import"}</a>
                    </span>
                </div>
                {/if}
            </li>
        </ul>
        {/if}
    </div>
</div>

