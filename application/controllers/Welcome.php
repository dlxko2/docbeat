<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Welcome
 */
class Welcome extends CI_Controller {

    /**
     * $userData variable.
     * Stores session data for class
     */
    private $userData;

	/**
     * Welcome constructor.
     * Get session, set gettext and models
     */
    public function __construct() {

        parent::__construct();
        $this->userData = $this->ss->userdata();

        $this->load->model("welcomemodel");
        $this->load->model("documentationsave");
        $this->load->model("documentationget");
    }

    /**
     * Main view of welcome page
     * Gets the data and save them in template
     */
    public function index() {

    	// For user logged in get his projects
    	$projects = false;
    	if (isset($this->userData['id'])) $projects = $this->welcomemodel->getProjectsByUser($this->userData['id']);

	    // Get commit info
    	$commit = $this->welcomemodel->getCommit();

	    // Get all project info
        $projectsData = $this->documentationget->getProjectsAll();

	    // Process by Smarty
	    $this->sm->assign('projects',$projects);
	    $this->sm->assign('commit',$commit);
        $this->sm->assign('projectsdata',$projectsData);
        $this->sm->assign('user',$this->userData);
        $this->sm->assign('index',base_url('index.php/'));
		$this->sm->assign('assets',base_url('assets/'));
		$this->sm->display('pages/welcome/welcome.tpl');
	}

	/**
     * Login form function
     * Receive post data and create session
     */
    public function login() {

        $postData = $this->input->post();
        $this->welcomemodel->doLogin($postData);
        redirect('/welcome/index', 'location');
    }

	/**
     * Logout form function
     * Destroys the session and refresh
     */
    public function logout() {

        $this->ss->sess_destroy();
        redirect('/welcome/index', 'location');
    }

	/**
     * Create project button function
     * Creates the new project with defined name
     */
    public function createProject(){

        $newID = $this->documentationsave->saveNew($this->userData['id']);
        echo $newID;
    }

	/**
     * Enter project function
     * Save selected project into session
     * @param $projectID - integer : id of the project
     */
    public function enterProject($projectID) {

	    $this->welcomemodel->selectProject($this->userData, $projectID);
	    if ($this->userData['permissions'] == 'S') redirect('/editor/index', 'location');
	    else redirect('/documentation/index', 'location');
    }

	/**
	 * Function to update settings
	 * Process input data and save changes
	 */
	public function settings() {

    	$postData = $this->input->post();
	    $this->welcomemodel->saveSettings($this->userData, $postData);
		if (isset($postData['language'])) $this->ss->set_userdata('language', $postData['language']);
		echo "OK";
    }
}
