<!-- PDF PREVIEW -->
<div id="pdfPreview"></div>

<!-- RESULTS PREVIEW -->
<div id="resultsPreview">
    <div class="card blue-grey darken-1 cardMargin" id="nothingCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="No PDF was generated"}</span>
            <p>{ci_language line="Please select preview for some student!"}</p>
        </div>
    </div>
    <div class="card blue-grey darken-1 cardMargin" id="errorCard">
        <div class="card-content white-text center-align">
            <span class="card-title">{ci_language line="User doesn't have any file created"}</span>
            <p>{ci_language line="Please select another user or wait!"}</p>
        </div>
    </div>
</div>

<!-- GRAPH PREVIEW -->
<div id="graphPreview">

    <!-- LEGEND -->
    <div id="legendBox">
        <span class="steelBlue"><b>{ci_language line="Time"}</b></span>
        <span class="lawnGreen"><b>{ci_language line="Characters"}</b></span>
        <span class="redColor"><b>{ci_language line="Done"}</b></span>
    </div>

    <!-- GRAPH -->
    <div id="graph" class="ct-chart ct-horizontal"></div>

    <!-- TABLE -->
    <div id="admin" class="col s12">
        <div class="card material-table">
            <table id="graphTable">
                <thead>
                <tr>
                    <th>{ci_language line="Week"}</th>
                    <th>{ci_language line="Time"}</th>
                    <th>{ci_language line="Characters"}</th>
                    <th>{ci_language line="Done"}</th>
                </tr>
                </thead>
                <tbody id="graphDetailsData"></tbody>
            </table>
        </div>
    </div>
</div>

