<div class="row center-align" id="paginationElem">
    <ul class="pagination center-align" >
        <li>
            <i class="small material-icons" onclick="aSaveChanges()">save</i>
            <i class="small material-icons cursorPointer" onclick="aExpand()">view_stream</i>
            <input type="hidden" value="0" id="expanded">
        </li>
    </ul>
</div>