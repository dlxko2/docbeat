<!-- SNAPSHOTS DATA-->
<ul id="snapshotDropdown" class="dropdown-content">

    <!-- DEFAULT -->
    <li onclick="updateSnaphot('{$assets}', {$user.id}, {$user.selectedProject}, '')">
        <a href="#!">{ci_language line="Current snapshot"}</a>
    </li>

    <!-- ANOTHER SNAPSHOTS -->
    {foreach from=$snapshotdata item=snapshot}
    <li onclick="updateSnaphot('{$assets}', {$user.id}, {$user.selectedProject}, '{$snapshot}')">
        <a href="#!">{$snapshot}</a>
    </li>
    {/foreach}

</ul>

<!-- TOOLBOX -->
<nav id="toolboxContainer">
    <div class="nav-wrapper">

        <!-- TITLE -->
        <ul class="left hide-on-med-and-down" id="toolbarText">
            <li><b>Team: {$user.teamName}</b></li>
        </ul>

        <!-- ANOTHER INFO -->
        <ul class="right hide-on-med-and-down" id="snapshotContainer">

            <!-- SNAPSHOT -->
            <li>
                <a class="dropdown-button" href="#!" data-activates="snapshotDropdown">
                    Select the snapshot
                    <i class="material-icons right">arrow_drop_down</i>
                </a>
            </li>

            <!-- TEAM -->
            {foreach from=$teamdata item=teamer}
            <li>
                <i class="material-icons" title="{$teamer.login}"
                   {if $teamer.id == $user.id}style="color: green"{/if}
                   onclick="aPreview(
                           '{$assets}',
                            {$teamer.id},
                           '{$teamer.login}',
                            {$user.selectedProject})">face</i>
            </li>
            {/foreach}

            <!-- MARK -->
            <li>
                <div class="chip right" id="toolbarResult">
                    {if isset($markdata.author)}{$markdata.author}%
                    {else} N\A
                    {/if}
                </div>
            </li>

        </ul>
    </div>
</nav>