<div class="row" id="step2">
    <div class="col s12">
        <ul class="collapsible" data-collapsible="expandable">

            {assign var="empty1" value=true}
            {if $targets > 0}

                <!-- QUESTION DATA -->
                {foreach from=$questionsdata item=question}
                {assign var="empty1" value=false}

                    <!-- CHAPTER DATA -->
                    {foreach from=$chaptersdata item=chapter}
                        {if $chapter.id == $question.quantity}
                            {assign var="extraValue" value=$chapter.extra_value}
                        {/if}
                    {/foreach}

                    <!-- RESULT DATA 1 -->
                    {if $targets > 0}
                        {assign var="content1" value=''}
                        {assign var="quality1" value='N/A'}
                        {assign var="quantity1" value='N/A'}
                        {foreach from=$resultsdata item=result}
                            {if $result.question == $question.id && $result.connect == $reviewersdata[0].connection}
                                {assign var="content1" value=$result.content}
                                {assign var="quality1" value=$result.quality}
                                {assign var="quantity1" value=$result.quantity}
                            {/if}
                        {/foreach}
                    {/if}

                    <!-- RESULT DATA 2 -->
                    {if $targets > 1}
                        {assign var="content2" value=''}
                        {assign var="quality2" value='N/A'}
                        {assign var="quantity2" value='N/A'}
                        {foreach from=$resultsdata item=result}
                            {if $result.question == $question.id && $result.connect == $reviewersdata[1].connection}
                                {assign var="content2" value=$result.content}
                                {assign var="quality2" value=$result.quality}
                                {assign var="quantity2" value=$result.quantity}
                            {/if}
                        {/foreach}
                    {/if}

                    <!-- SET BLOCK TYPE -->
                    {if $question.type == 'R'}<li class="typeReview" style="display: none">{/if}
                    {if $question.type == 'F'}<li class="typeFeedback" style="display: none">{/if}
                    {if $question.type == 'E'}<li class="typeFinal"  style="display: none">{/if}

                        <!-- HEADER -->
                        <div class="collapsible-header" style="line-height: 0rem;">
                            <b>{$question.type}: {$question.content}</b>
                            {include file='pages/reviewer/contentHeader.tpl'}
                        </div>

                        <!-- BODY -->
                        <div class="collapsible-body">
                            <div class="row">
                                {include file='pages/reviewer/contentFirst.tpl'}
                                {include file='pages/reviewer/contentSecond.tpl'}
                            </div>
                        </div>
                    </li>

                {/foreach}
            {/if}

            <!-- WAIT LI FOR REVIEW -->
            <li id="waitForReview" style="display: none">
                <div class="collapsible-header center-align">
                    <b>
                        {ci_language line="Review will begin on:"}
                        <span style="color: red">
                            {if isset($deadlinedata.review)}{$deadlinedata.review.from}{/if}
                        </span>
                    </b>
                </div>
            </li>

            <!-- WAIT LI FOR FEED-->
            <li id="waitForFeed" style="display: none">
                <div class="collapsible-header">
                    <b>
                        {ci_language line="Feedback will be shown on:"}
                        <span style="color: red">
                            {if isset($deadlinedata.feedback)}{$deadlinedata.feedback.to}{/if}
                        </span>
                    </b>
                </div>
            </li>

            <!-- WAIT LI FOR FINAL -->
            <li id="waitForFinal" style="display: none">
                <div class="collapsible-header">
                    <b>
                        {ci_language line="Final review will begin on:"}
                        <span style="color: red">
                            {if isset($deadlinedata.final)}{$deadlinedata.final.from}{/if}
                        </span>
                    </b>
                </div>
            </li>

            <!-- EMPTY LI -->
            {if $targets == 0}
                <li>
                    <div class="collapsible-header center-align">
                        <b>{ci_language line="No reviewers found!"}</b>
                    </div>
                </li>
            {elseif $empty1 == true}
                <li>
                    <div class="collapsible-header center-align">
                        <b>{ci_language line="No questions found!"}</b>
                    </div>
                </li>
            {/if}

        </ul>
    </div>
</div>