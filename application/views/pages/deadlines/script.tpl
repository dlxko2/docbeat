{literal}
<script>

    /**
     * Deadline counter variable
     */
    var deadlineNumber = 0;

    /**
     * Webpage constructor function
     * Get deadlines from DB and create elements
     */
    $(document).ready(function() {

        // Update topbar
        $('#topDeadlines').css('color', 'red');

        // Update the lock
        updateLock();

        // Create deadlines
        {/literal}{foreach from=$deadlinesdata item=deadline}
        aCreateDeadline(
            {$deadline.deadlineInfo.id},
           '{$deadline.deadlineInfo.name}',
           '{$deadline.deadlineInfo.review_type}',
           '{$deadline.deadlineInfo.snapshot}',
           '{$deadline.chapterString}',
           '{$deadline.deadlineInfo.review_end}',
            {$project.locked});
        {/foreach}{literal}
    });

    /**
     * Create new deadline element
     * @param id - int : id of the deadline
     * @param name - string : name for the deadline
     * @param type - char : type of the deadline
     * @param date - string : deadline date
     * @param chapters - string : chapters string
     * @param review - string : review date
     */
    function aCreateDeadline(id, name, type, date, chapters, review, locked) {

        // Process the HTML part
        {/literal}{include file='pages/deadlines/newDeadline.tpl'}{literal}

        // Put the element together from fragments and create it
        if (locked) $('#deadlineAppendContainer').append(header + secondary + bodyDate + bodyChapters + bodyReview);
        else $('#deadlineAppendContainer').append(header + deleter + secondary + bodyDate + bodyChapters + bodyReview);

        // Focus elements
        var selectElement = '#iChapters' + deadlineNumber;
        var radio = 'input[name="iType' + deadlineNumber + '"]';
        var deadDate = '#deadlineDate' + deadlineNumber;
        var revDate = '#reviewDate' + deadlineNumber;
        var selectAll = 'input[class="select-dropdown"]';
        var nameElement = '#name' + deadlineNumber;

        // Update ratio buttons
        if (type != 0) $(radio).val([type]);

        // Update chapters
        if (chapters) {
            $.each(chapters.split(","), function (i, e) {
                if (e) {$(selectElement + " option[value='" + e + "']").prop("selected", true);}
            });
        }

        // Initialize material elements
        $(selectElement).material_select();
        $('.datepicker').pickadate({selectMonths: true, selectYears: 15});

        // Update locked
        if (locked) {
            $(radio).attr('disabled', true);
            $(deadDate).attr('disabled', true);
            $(revDate).attr('disabled', true);
            $(selectAll).attr('disabled', true);
        }

        // Trigger deadline name update
        $('#deadlineName' + deadlineNumber).on('input', function () {
            if($(this).val().length > 0)
                $(nameElement).text($(this).val());
            else
                $(nameElement).text('{/literal}{ci_language line="Unkown"}{literal}');

        });

        // Increase deadline counter
        ++deadlineNumber;
    }

    /**
     * Save deadlines form
     */
    function aSaveChanges(){
        $( "#fDeadlines" ).submit();
    }

</script>
{/literal}

