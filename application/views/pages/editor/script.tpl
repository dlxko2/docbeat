<script>
{literal}

    /**
     * Initialize codemirror from textarea
     */
    var editor = CodeMirror.fromTextArea(editorArea, {lineNumbers: true});

    /**
     * Create trigger for character stamps
     */
    editor.on('change', function() {
        var characters = parseInt($('#characterStore').val());
        if (characters >= {/literal}{$smarty.const.CHAR_STUCK}{literal}) {
            saveStamp('{/literal}{$index}{literal}', 'C');
            $('#characterStore').val(0);
        }
        else $('#characterStore').val(characters + 1);
    });

    /**
     * Webpage constructor function
     */
    $(document).ready(function() {

        // Update topbar
        $('#topEditor').css('color', 'red');

        // Update slider size
        $('.slider').slider({height: 400});

        // Update PDF view size
        var window_size = $(window).height();
        $('#pdfContainer').height(window_size);

        // Stop scrolling function
        $('body').addClass('stop-scrolling')

        // Get first chapter
        aGetChapter({/literal}'{$index}'{literal}, {/literal}{$actual}{literal});
        $("#chapterSelector").val({/literal}{$actual}{literal});
        
        // Initialize materialize and caret
        $('select').material_select();
        $('.caret').text('');

        // Update codemirror size
        var height = $(document).height() - 40 - 47 - 56 - 10;
        $('.CodeMirror').css('height', height);

        // Append interval triggers and actual ones
        getLead({/literal}'{$index}', '{ci_language line="of best classmates"}'{literal});
        setInterval(function() {saveStamp({/literal}'{$index}'{literal}, 'T')}, {/literal}{$smarty.const.TIME_STUCK}{literal});
        setInterval(function() {getLead({/literal}'{$index}', '{ci_language line="of best classmates"}'{literal})}, {/literal}{$smarty.const.LEAD_RELOAD}{literal});

        // On chapter change trigger this events
        (function () {

            // Store old chapter number
            var previous;
            $("#chapterSelector").focus(function () {previous = this.value;}).change(function() {

                // Save old chapter, get new, get new done, get new finishing done
                aSaveChapter({/literal}'{$index}', '{ci_language line="of chapter already done"}'{literal}, previous); 
                aGetChapter({/literal}'{$index}'{literal}); 
                getChapterDone({/literal}'{$index}', '{ci_language line="of chapter already done"}'{literal}); 
                getFinishingDone({/literal}'{$index}', '{ci_language line="of classmates finishing"}'{literal});
                previous = this.value;
            });
        })();
    });

    /**
     * Function when blured
     */
    $(window).blur(function(){
        $('#activityStore').val('inactive');
        $('#prog1').addClass('progressRed');
        $('#prog2').addClass('progressRed');
        $('#prog3').addClass('progressRed');
    });

    /**
     * Function when focued
     */
    $(window).focus(function(){
        $('#activityStore').val('active');
        $('#prog1').removeClass('progressRed');
        $('#prog2').removeClass('progressRed');
        $('#prog3').removeClass('progressRed');
        });

</script>
{/literal}
