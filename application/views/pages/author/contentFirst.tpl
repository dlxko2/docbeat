{if $targets == 1} <div class="col s12">
{else} <div class="col s6"> {/if}

{if $question.type == 'F'}

        <!-- HIDDEN INPUTS -->
        <input type="hidden" name="iQuestion[]" value="{$question.id}">
        <input type="hidden" name="iType[]" value="{$question.type}">
        <input type="hidden" name="iConnect[]" value="{$targetdata[0].connection}">
        <input type="hidden" name="iOwner[]" value="{$targetdata[0].id}">

        <!-- CONTENT INPUT -->
        <textarea id="textarea1" class="materialize-textarea"
                      placeholder="{ci_language line="Write feedback for"} {$targetdata[0].login}"
                      name="iReview[]">{$content1}</textarea>

        <!-- SLIDERS -->
        <p class="range-field step2SliderContainer">

            <!-- QUALITY -->
            {if $question.quality == 1}
                <label>{ci_language line="Result"}</label>
                <input type="range" min="0" max="10" name="iQuality[]" value="{$quality1}"/>
            {else}
                <input type="hidden" name="iQuality[]" value="0"/>
            {/if}

            <!-- QUANTITY LABEL -->
            <label>
                {assign var="extra1" value=0}
                {foreach from=$chaptersdata item=chapter}
                    {if $chapter.id == $question.quantity}
                        {assign var="extra1" value=$chapter.extra_value}
                        {$chapter.extra}
                    {/if}
                {/foreach}
            </label>

            <!-- QUANTITY -->
            {if $extra1 != 0}
                <input type="range" min="0" max="{$extra1}" name="iQuantity[]" value="{$quantity1}"/>
            {else}
                <input type="hidden" name="iQuantity[]" value="0"/>
            {/if}
        </p>

    <!-- RESULT DATA VIEW -->
    {else}
        <div class="card blue-grey darken-1">
            <div class="card-content white-text minHeightContent">
                <p class="noContentPadding">
                    {$content1}
                </p>
            </div>
        </div>
    {/if}

</div>
