<div id="mImport" class="modal">
    <div class="modal-content">

        <!-- PROJECT LIST -->
        <div id="projectListContainer" class="col s12">
            <div class="card material-table">

                <div class="table-header">
                    <span class="table-title red-text darken-4"><b>{ci_language line="Projects list"}</b></span>
                    <div class="actions">
                        <a href="#" class="search-toggle waves-effect btn-flat nopadding">
                            <i class="material-icons">search</i>
                        </a>
                    </div>
                </div>

                <table class="tRewProjects">
                    <thead>
                    <tr>
                        <th>{ci_language line="Project name"}</th>
                        <th>{ci_language line="Teacher"}</th>
                        <th>{ci_language line="Enter"}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>

        <!-- QUESTION LIST -->
        <div id="questionListContainer" class="col s12" style="display: none">
            <div class="card material-table">

                <div class="table-header">
                    <span class="table-title red-text darken-4"><b>{ci_language line="Question list"}</b></span>
                    <div class="actions">
                        <a href="#" class="search-toggle waves-effect btn-flat nopadding">
                            <i class="material-icons">search</i>
                        </a>
                    </div>
                </div>

                <table class="tRewQuestions">
                    <thead>
                    <tr>
                        <th>{ci_language line="Question"}</th>
                        <th>{ci_language line="Type"}</th>
                        <th>{ci_language line="Enter"}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">{ci_language line="Agree"}</a>
    </div>

</div>