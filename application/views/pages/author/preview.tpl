<div id="pdfPreview">
    <object data="{$assets}/datas/users/{$user.id}/{$user.selectedProject}/preview/main.pdf"
            type="application/pdf"
            id="pdfObject"></object>
</div>

<div id="resultPreview">
    <div class="card blue-grey darken-1 cardMargin" id="nothingCard">
        <div class="card-content white-text center-align">
            <span class="card-title">
                {ci_language line="No PDF was generated for"}
                <span id="previewName"></span>
            </span>
            <p>
                {ci_language line="Please wait for user to generate one!"}
            </p>
        </div>
    </div>
</div>

