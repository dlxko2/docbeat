<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Author
 */
class Author extends CI_Controller {

	/**
	 * $userData variable.
	 * Stores session data for class
	 */
	private $userData;

	/**
	 * Author constructor.
	 * Get session, set gettext and models
	 * Check if user is logged and project selected
	 */
    public function __construct() {

        parent::__construct();

	    $this->load->model("documentationget");
	    $this->load->model("reviewsget");
	    $this->load->model("reviewsfunc");
	    $this->load->model("authorsave");
	    $this->load->model("editorget");
	    $this->load->model("authorget");
	    $this->load->model("welcomemodel");
	    $this->load->model("deadlinesget");

	    $this->userData = $this->ss->userdata();
	    if (!array_key_exists('id', $this->userData) || !array_key_exists('selectedProject', $this->userData))
		    redirect('/welcome/index', 'location');
    }

	/**
	 * Main view of author page
	 * Gets the data and save them in template
	 */
    public function index() {

    	// Data for snapshots - empty or snapshot file names
	    $snapshotData = $this->reviewsfunc->getSnapshotsByUser($this->userData);
	    // Data for reviews deadlines - optional deadline for each review
    	$deadlinesData = $this->deadlinesget->getReviewDeadlinesByProject($this->userData);
	    // Data for the chapters - empty or data for each chapter
	    $chaptersData = $this->documentationget->getChaptersByProject($this->userData['selectedProject']);
	    // Data for the questions - empty or data for each question
	    $questionsData = $this->reviewsget->getQuestionsByProject($this->userData['selectedProject']);
	    // Data for two reviewers - empty or data for each person
		$targetData = $this->reviewsget->getTargetsByUser($this->userData['id']);

	    // Create connection array from target data
	    $cArray = [];
	    if (!empty($targetData[0])) array_push($cArray, $targetData[0]['connection']);
	    if (!empty($targetData[1])) array_push($cArray, $targetData[1]['connection']);

	    // Data for the results - empty or result data
	    $resultsData = $this->authorget->getResultByConnectionsArray($cArray);
	    // Data for logged user team members - empty or data for reach member
		$teamData = $this->welcomemodel->getTeamMembersByUser($this->userData);
	    // Mark data - array with datas or default array as return
	    $markData = $this->authorget->getMarkInfoByUser($this->userData);

	    // Save datas to the Smarty processor
	    $this->sm->assign('targets', count($cArray));
	    $this->sm->assign('markdata',$markData);
	    $this->sm->assign('snapshotdata',$snapshotData);
	    $this->sm->assign('deadlinedata',$deadlinesData);
	    $this->sm->assign('teamdata',$teamData);
	    $this->sm->assign('chaptersdata',$chaptersData);
	    $this->sm->assign('resultsdata',$resultsData);
	    $this->sm->assign('targetdata',$targetData);
	    $this->sm->assign('questionsdata',$questionsData);
	    $this->sm->assign('user',$this->userData);
        $this->sm->assign('index',base_url('index.php/'));
        $this->sm->assign('assets',base_url('assets/'));
        $this->sm->display('pages/author/author.tpl');
    }

	/**
	 * Function to save author form
	 * Gets data from post input and save them into DB
	 * Also updates marks information for
	 */
	public function save() {

		// Shorten variables
		$project = $this->userData['selectedProject'];
		$source = $this->userData['id'];

		// Save feedback data from the post into database
		$postData = $this->input->post();
		if (!$this->authorsave->saveResponds($postData)) return;

		// Get targets
		$targetData = $this->reviewsget->getTargetsByUser($this->userData['id']);
		if (empty($targetData)) return;

		// Update marks for reviewer 1
    	if (isset($targetData[0]['id'])) {
    		$points1 = $this->editorget->getLeadPercents(['selectedProject' => $project, 'id' => $targetData[0]['id']]);
		    if (!$this->authorsave->updateMarks($source, $targetData[0]['id'], $project)) return;
		    if (!$this->authorsave->updateMarkAdmin($targetData[0]['id'], $project, $points1)) return;
	    }

		// Update marks for reviewer 2
		if (isset($targetData[1]['id'])) {
			$points2 = $this->editorget->getLeadPercents(['selectedProject' => $project, 'id' => $targetData[1]['id']]);
			if (!$this->authorsave->updateMarks($source, $targetData[1]['id'], $project)) return;
			if (!$this->authorsave->updateMarkAdmin($targetData[1]['id'], $project, $points2)) return;
		}

		// Also update admin mark for current user with new feedback
		$points = $this->editorget->getLeadPercents(['selectedProject' => $project, 'id' => $source]);
		if (!$this->authorsave->updateMarkAdmin($source, $project, $points)) return;

		// Show author default page
		redirect('/author/index', 'location');
    }
}
