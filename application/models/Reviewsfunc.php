<?php

/**
 * Class ReviewsFunc
 */
class ReviewsFunc extends CI_Model
{
	/**
	 * ReviewsFunc constructor.
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Function to get snapshots for the user
	 * @param $userData - array : data for the logged user
	 * @return array|false - result with the snapshots names
	 */
	public function getSnapshotsByUser($userData) {

		// Prepare the dir
		$dir = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/snapshots/';

		// If not exists create the one
		if (!file_exists($dir)) {
			mkdir($dir, DATA_PERMISSIONS, true);
			exec ("chmod -R " . CHMOD_PERMISSIONS . " " . $dir);
		}

		// Search for the files
		$files = glob($dir . '*');
		$result = [];

		// Update the result for each snapshot
		foreach($files as $file){
			if(is_file($file))
				$path = pathinfo($file);
			array_push($result, $path['filename']);
		}

		// Return the result
		return $result;
	}

}