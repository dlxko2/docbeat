<?php

class Logmodel extends CI_Model
{
	/**
	 * Logmodel constructor.
	 */
	function __construct() {

		parent::__construct();
	}

	public function lE($comment) {

		$debug = @debug_backtrace();
		$log = $debug[1];

		if (!file_exists(LOGDATA))
			mkdir(LOGDATA, DATA_PERMISSIONS, true);

		$userData = $this->ss->userdata();

		$fileName = date('Y-m-d') . '.log';
		$message = '[E]' . '[FILE]' . basename($log['file']) . '[LINE]' . $log['line'] . '[FUNC]' . $log['function'];
		$message .= '[ID]' . $userData['id'] . '[PROJ]' . $userData['selectedProject'] . '[COM]' . $comment . "\n";

		file_put_contents(LOGDATA . $fileName, $message, FILE_APPEND);

		if (HARD_DEBUG == 1) {
			echo $message;
			exit;
		}
	}

	public function lW($comment) {

		$debug = debug_backtrace();
		$log = $debug[1];

		if (!file_exists(LOGDATA))
			mkdir(LOGDATA, DATA_PERMISSIONS, true);

		$userData = $this->ss->userdata();

		$fileName = date('Y-m-d') . '.log';
		$message = '[W]' . '[FILE]' . basename($log['file']) . '[LINE]' . $log['line'] . '[FUNC]' . $log['function'];
		$message .= '[ID]' . $userData['id'] . '[PROJ]' . $userData['selectedProject'] . '[COM]' . $comment . "\n";

		file_put_contents(LOGDATA . $fileName, $message, FILE_APPEND);

		if (HARD_DEBUG == 1) {
			echo  $message;
			exit;
		}
	}
}