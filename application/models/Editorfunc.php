<?php

/**
 * Class EditorFunc
 */
class EditorFunc extends CI_Model
{
	/**
	 * EditorFunc constructor.
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Function to save user chapter
	 * @param $userData - array : logged user data
	 * @param $chapterID - integer : id of the chapter
	 * @param $content - string : content of the chapter
	 * @return boolean - the result of the function
	 */
	public function saveUserChapter($userData, $chapterID, $content) {

		// Prepare dirs to save
		$dir = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/chapters/';
		$file = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/chapters/' . $chapterID . '.tex';

		// Repair not existing dir
		if (!file_exists($dir)) {
			mkdir($dir, DATA_PERMISSIONS, true);
			exec ("chmod -R " . CHMOD_PERMISSIONS . " " . USERDATA . $userData['id']);
		}

		// Save the content
		if (!file_put_contents($file, $content)) return false;
		return true;

	}

	/**
	 * Function to generate the whole PDF file
	 * @param $uD - array : logged user data
	 * @param $chD - array : data for the chapter
	 * @param $pD - array : data for the project
	 * @param $type - char : type of generation process
	 * @return array - result of the function
	 */
	public function generatePreview($uD, $chD, $pD, $type) {

		// Prepare the directories
		$picFol = USERDATA . $uD['id'] . '/' . $uD['selectedProject'] . '/pictures/';
		$filFol = PROJECTDATA . $uD['selectedProject'] . '/files/';
		$datFol = USERDATA . $uD['id'] . '/' . $uD['selectedProject'] . '/preview/';
		$chFol = USERDATA . $uD['id'] . '/' . $uD['selectedProject'] . '/chapters/';

		// Call the process functions
		if (!$this->genPMain($datFol, $chD, $pD, $uD)) return ['result' => false, 'content' => "Can't generate main.tex!"];
		if (!$this->genPChap($chD, $chFol, $datFol)) return ['result' => false, 'content' => "Can't create chapter files!"];
		if (!$this->genPFiles($filFol, $datFol)) return ['result' => false, 'content' => "Can't create base files!"];
		if (!$this->genPPics($picFol, $datFol)) return ['result' => false, 'content' => "Can't create picture files!"];

		// Generate the final
		if ($type == 'P') $result = $this->generatePreviewPDF($datFol);
		else if ($type == 'B') $result = $this->generatePreviewBIB($datFol);
		else $result = false;

		// Update the permissions
		exec ("chmod -R " . CHMOD_PERMISSIONS . " " . USERDATA);

		// Return the result of the function
		return $result;
	}

	/**
	 * Function to cleanup the LaTeX generation files
	 * @param $dataFolder - string : path to data folder
	 * @TODO : user should manually delete the cache - problem with BIB
	 * @return array - the result of the function
	 */
	private function cleanupPreviewFiles ($dataFolder) {

		// If not exists data folder create it
		if (!file_exists($dataFolder)) {
			mkdir($dataFolder, DATA_PERMISSIONS, true);
			return ['result' => true, 'content' => ''];
		}

		// Delete all files
		$files = glob($dataFolder . '*');
		foreach($files as $file){
			if(is_file($file))
				if (!unlink($file))
					return ['result' => false, 'content' => "Can't delete cache!"];
		}

		// Return default result
		return ['result' => true, 'content' => ''];
	}

	/**
	 * Generate the main tex file
	 * @param $dataFolder - string : path to data folder
	 * @param $chaptersData - array : data for the chapter
	 * @param $projectData - array : data for the project
	 * @param $userData - array : data of logged user
	 * @return boolean - the result of the function
	 */
	private function genPMain ($dataFolder, $chaptersData, $projectData, $userData) {

		// Repair not existing dir
		if (!file_exists($dataFolder))
			mkdir($dataFolder, DATA_PERMISSIONS, true);

		// Import chapters
		$replaceChapters = "";
		foreach ($chaptersData as $chapter)
			$replaceChapters .= "\input{" . $chapter['id'] . "}";

		// Import keywords
		$withTitle = preg_replace("/{TITLE}/", $projectData['name'], $projectData['base']);
		$withAuthor = preg_replace("/{AUTHOR}/", $userData['login'], $withTitle);
		$withChapters = preg_replace("/{INCLUDES}/", $replaceChapters, $withAuthor);

		// Save the file
		if (!file_put_contents($dataFolder . 'main.tex', $withChapters))
			return false;

		// Return default result
		return true;
	}

	/**
	 * Generate the chapter files for the preview
	 * @param $chaptersData - array : data for the chapter
	 * @param $chapterFolder - string : path to chapter folder
	 * @param $dataFolder - string : path to data folder
	 * @return boolean - the result of the function
	 */
	private function genPChap ($chaptersData, $chapterFolder, $dataFolder) {

		// For each chapter from DB
		foreach ($chaptersData as $chapter) {

			// Prepare the file names
			$oldChapter = $chapterFolder . $chapter['id'] . '.tex';
			$newChapter = $dataFolder . $chapter['id'] . '.tex';

			// If exists copy from the directory
			if (file_exists($oldChapter)) {
				$res = copy($oldChapter, $newChapter);
				if ($res == false) return false;
			}

			// If not exists use the database value
			else {
				$res = file_put_contents($newChapter, $chapter['content']);
				if ($res < 1) return false;
			}
		}

		// Also contain the literature file
		if (file_exists($chapterFolder . 'literature.bib')) {
			if (!copy($chapterFolder . 'literature.bib', $dataFolder . 'literature.bib'))
				return false;
		}
		else file_put_contents($dataFolder . 'literature.bib', '');

		// Return the default result
		return true;
	}

	/**
	 * Function to generate the files for the preview
	 * @param $filesFolder - string : path to files
	 * @param $dataFolder - string : path to data folder
	 * @return boolean - the result of the function
	 */
	private function genPFiles ($filesFolder, $dataFolder) {

		// Get all files from project and copy to user
		$files = glob($filesFolder . "*.*");
		foreach($files as $file)
		{
			$NewFile = str_replace($filesFolder,$dataFolder,$file);
			if (!file_exists($NewFile))
				if (!copy($file, $NewFile))
					return false;
		}
		return true;
	}

	/**
	 * Function to generate the pictures for preview
	 * @param $picsFolder - string : path to pictures folder
	 * @param $dataFolder - string : path to data folders
	 * @return boolean - the result of the function
	 */
	private function genPPics ($picsFolder, $dataFolder) {

		// Copy all pictures from the picture dir to data dir
		$files = glob($picsFolder . "*.*");
		foreach($files as $file)
		{
			$NewFile = str_replace($picsFolder,$dataFolder,$file);
			if (!file_exists($NewFile))
				if (!copy($file, $NewFile))
					return false;
		}
		return true;
	}

	/**
	 * Core function to generate the PDF
	 * @param $dataFolder - string : path to data folder
	 * @return array - the result of the function
	 */
	private function generatePreviewPDF ($dataFolder) {

		// Prepare file names
		$texFot = TEXBIN . 'texfot ';
		$pdfLatex = TEXBIN . 'pdflatex ';
		$logFile = $dataFolder . 'main.log';
		$test = "";

		// Run the pdfLatex depending on TexFot option
		if (TEXFOT) exec('cd ' . $dataFolder . ' && ' . $texFot . $pdfLatex . 'main.tex', $test);
		else exec('cd ' . $dataFolder . ' && ' . $pdfLatex . 'main.tex', $test);

		// Check if something produced and if error
		if (!file_exists($logFile)) return ['result' => false, 'content' => "Can't find any output!"];
		$LogFile = file_get_contents($logFile);
		$ErrorCheck = preg_match("/Fatal error occurred, no output PDF file produced!/", $LogFile);

		// Send the result
		if ($ErrorCheck)
		{
			$TestOut = implode("<br>", $test);
			return ['result' => false, 'content' => $TestOut];
		}

		// Return the default result
		return ['result' => true, 'content' => ''];
	}

	/**
	 * Function to regenerate the BIB
	 * @param $dataFolder - string : path to data folder
	 * @return array - the result of the function
	 */
	private function generatePreviewBIB($dataFolder) {

		// Prepare the file names
		$bibTex = TEXBIN . 'bibtex ';
		$bibFile1 = $dataFolder . 'main.blg';
		$bibFile2 = $dataFolder . 'main.bbl';
		$test = "";

		// Run the bib
		exec('cd ' . $dataFolder . ' && ' . $bibTex . 'main', $test);

		// Check for errors
		if (!file_exists($bibFile1))
			return ['result' => false, 'content' => "Can't find output blg file!\n" . current($test)];
		if (!file_exists($bibFile2))
			return ['result' => false, 'content' => "Can't find output bbl file!\n" . current($test)];

		// Return
		return ['result' => true, 'content' => ''];
	}

	/**
	 * Function to get chapter by it ID
	 * @param $chapterID - integer : id of the chapter
	 * @param $userData - array : user data
	 * @return string - the result or the chapter
	 */
	public function getChapterByID($chapterID, $userData) {

		// Prepare the path
		$chapterFile = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/chapters/' . $chapterID . '.tex';

		// Try to find the file and return the result
		if (!file_exists($chapterFile)) return "";
		return file_get_contents($chapterFile);
	}

	/**
	 * Function to get the content of the literature file
	 * @param $userData - array : data of the user
	 * @return string - the literature content
	 */
	public function getLiteratureByUser($userData) {

		// Prepare the path
		$literatureFile = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/chapters/literature.bib';

		// Try to find the file and return the result
		if (!file_exists($literatureFile)) return "";
		return file_get_contents($literatureFile);
	}

	/**
	 * Function to save the literature
	 * @param $userData - array : data of the user
	 * @param $content - content to save
	 * @return boolean - the result of the operation
	 */
	public function saveLiterature($userData, $content) {

		// Prepare the paths
		$literatureDir = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/chapters/';
		$literatureFile = 'literature.bib';

		// Try to find the file or create it
		if (!file_exists($literatureDir . $literatureFile)) {
			mkdir($literatureDir, DATA_PERMISSIONS, true);
			exec ("chmod -R " . CHMOD_PERMISSIONS . " " . USERDATA);
		}

		// Save the literature
		file_put_contents($literatureDir . $literatureFile, $content);
		return true;
	}

	/**
	 * Function to add the files from the user dialog
	 * @param $files - files : the files
	 * @param $userData - array : the user data
	 * @return boolean - the result of the function;
	 */
	public function addFiles($files, $userData)
	{
		// Check if any file in list
		if (empty(current($files['iPictures']['name'])))
			return true;

		// Set up upload path
		$uploadPath = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/pictures/';
		if (!file_exists($uploadPath)) mkdir($uploadPath, DATA_PERMISSIONS, true);

		// Load library to uplaod and count the result
		$this->load->library('upload');
		$cpt = count($_FILES['iPictures']['name']);

		// Process every file
		for($i=0; $i<$cpt; $i++)
		{
			// Modify format of input file
			$_FILES['userfile']['name']= $files['iPictures']['name'][$i];
			$_FILES['userfile']['type']= $files['iPictures']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['iPictures']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['iPictures']['error'][$i];
			$_FILES['userfile']['size']= $files['iPictures']['size'][$i];

			// Try to upload new file
			$this->upload->initialize($this->setUploadOptions($uploadPath));
			if ($this->upload->do_upload() == false)
				$this->CIcallError($this->upload->display_errors());
		}

		// Update the permissions, return the default result
		exec ("chmod -R " . CHMOD_PERMISSIONS . " " . USERDATA);
		return true;
	}

	/**
	 * Function to set the upload options
	 * @param $uploadPath - string : path to upload
	 * @return array - the options array
	 */
	private function setUploadOptions($uploadPath)
	{
		$config = [];
		$config['upload_path'] = $uploadPath;
		$config['allowed_types'] = 'png|jpg|jpeg';
		$config['max_size']      = '10000';
		$config['overwrite']     = TRUE;
		return $config;
	}

	/**
	 * Function to get all user pictures
	 * @param $userData - array : data for the user
	 * @return array - the array with pictures
	 * @return array - the result of the function
	 */
	public function getPicturesByUser($userData) {

		// Prepare the path
		$directory =  USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/pictures/';

		// Check file or create
		if (!file_exists($directory))
			mkdir($directory, DATA_PERMISSIONS, true);

		// Return without trash
		return array_diff(scandir($directory), ['..', '.']);
	}

	/**
	 * Function to remove the image
	 * @param $picture - string : the picture to remove
	 * @param $userData - array : data for the user
	 * @return boolean - the result of the function
	 */
	public function removeImage($picture, $userData) {

		// Prepare the path
		$pictureLink =  USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/pictures/' . $picture;

		// Try to unlink
		if (file_exists($pictureLink))
			if (!unlink($pictureLink))
				return false;

		return true;
	}

    public function addPDFFiles($files, $userData)
    {
        // Check if any file in list
        if (empty(current($files['iUploadFiles']['name'])))
            return true;

        // Set up upload path
        $uploadPath = USERDATA . $userData['id'] . '/' . $userData['selectedProject'] . '/preview/';
        if (!file_exists($uploadPath)) mkdir($uploadPath, DATA_PERMISSIONS, true);

        // Load library to uplaod and count the result
        $this->load->library('upload');
        $cpt = count($_FILES['iUploadFiles']['name']);

        // Process every file
        for($i=0; $i<$cpt; $i++)
        {
            // Modify format of input file
            $_FILES['userfile']['name']= "main.pdf";
            $_FILES['userfile']['type']= $files['iUploadFiles']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['iUploadFiles']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['iUploadFiles']['error'][$i];
            $_FILES['userfile']['size']= $files['iUploadFiles']['size'][$i];

            // Try to upload new file
            $this->upload->initialize($this->setUploadOptionsPDF($uploadPath));
            if ($this->upload->do_upload() == false)
                $this->CIcallError($this->upload->display_errors());
        }

        // Update the permissions, return the default result
        exec ("chmod -R " . CHMOD_PERMISSIONS . " " . USERDATA);
        return true;
    }

    /**
     * Function to set upload options
     * @param $uploadPath - string : path for upload files
     * @return array - the config for upload
     */
    private function setUploadOptionsPDF($uploadPath)
    {
        $config = [];
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = '*';
        $config['max_size']      = '10000';
        $config['overwrite']     = FALSE;
        return $config;
    }
}