{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/packages/pkg_datatables.tpl'}
{include file='includes/page_content/settings.tpl'}

<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/welcome.css">

<main>
<div class="container">
    <div class="row">

        {include file='pages/welcome/projects.tpl'}
        {include file='pages/welcome/login.tpl'}

    </div>
</div>
</main>

<script src="{$assets}/{$smarty.const.scripts}/welcome.js"></script>
{include file='pages/welcome/script.tpl'}
{include file='includes/page_content/footer.tpl'}
{include file='includes/page_content/finish.tpl'}
