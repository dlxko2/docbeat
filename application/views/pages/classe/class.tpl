<div id="admin" class="col s12">
    <div class="card material-table">

        <!-- HEADER -->
        <div class="table-header">
            <span class="table-title red-text darken-4"><b>{ci_language line="Student list"}</b></span>

            <!-- FUNCTIONS -->
            {if $project.locked == 0}
                <a class="waves-effect waves-light btn" id="addUsers" onclick="aCreateUsers()">
                    {ci_language line="Add"}
                </a>&nbsp;&nbsp;
                <a class="waves-effect waves-light btn" id="newProject" href="{$index}/classe/createPairs/">
                    {ci_language line="Pair"}
                </a>
            {/if}

            <!-- SEARCH -->
            <div class="actions">
                <a href="#" class="search-toggle waves-effect btn-flat nopadding">
                    <i class="material-icons">search</i>
                </a>
            </div>

            <!-- HELP -->
            <i class="material-icons cursorPointer" id="showHelp">info</i>  &nbsp;

            <!-- REGENERATE -->
            <i class="material-icons cursorPointer"
               onclick="regenerate('{$index}', '{ci_language line="Cache regenerated - please wait for page reload"}')">
                refresh
            </i>
        </div>

        <!-- HEAD -->
        <table id="studentsTable">
            <thead>
            <tr>
                <th class="center-align">{ci_language line="Subject"}</th>
                <th class="center-align">{ci_language line="Mark"}</th>
                <th class="center-align">{ci_language line="Review 1"}</th>
                <th class="center-align">{ci_language line="Review 2"}</th>
                <th>{ci_language line="Progress"}</th>
            </tr>
            </thead>
            <tbody>

            <!-- LIST USERS -->
            {foreach from=$usersdata item=localUser}
                {if $user.permissions == 'A' || $localUser.base.visible == 1 || $localUser.base.id == $user.id}
                    <!-- PROCESS INPUT DATA -->
                    {assign var="points" value="X"}
                    {assign var="adm" value="X"}{assign var="done" value="X"}
                    {assign var="rew1" value="X"}{assign var="rew2" value='X'}
                    {assign var="auth" value="X"}{assign var="rewer" value="X"}
                    {assign var="feed1" value='X'}{assign var="feed2" value='X'}
                    {assign var="fin1" value='X'}{assign var="fin2" value='X'}
                    {assign var="rew1id" value=0}{assign var="rew2id" value=0}
                    {assign var="rew1name" value='unkown'}{assign var="rew2name" value='unkown'}
                    {if $localUser.base.points != 0}{$points = $localUser.base.points}{/if}
                    {if $localUser.base.done != NULL}{$done = $localUser.base.done}{/if}
                    {if $localUser.base.admin != NULL}{$adm = $localUser.base.admin}{/if}
                    {if $localUser.base.rew1 != NULL}{$rew1 = $localUser.base.rew1}{/if}
                    {if $localUser.base.rew2 != NULL}{$rew2 = $localUser.base.rew2}{/if}
                    {if $localUser.base.feed1 != NULL}{$feed1 = $localUser.base.feed1}{/if}
                    {if $localUser.base.feed2 != NULL}{$feed2 = $localUser.base.feed2}{/if}
                    {if $localUser.base.fin1 != NULL}{$fin1 = $localUser.base.fin1}{/if}
                    {if $localUser.base.fin2 != NULL}{$fin2 = $localUser.base.fin2}{/if}
                    {if $localUser.base.author != NULL}{$auth = $localUser.base.author}{/if}
                    {if $localUser.base.reviewer != NULL}{$rewer = $localUser.base.reviewer}{/if}

                    <!-- PROCESS REVIEWERS -->
                    {if $localUser.rew1.connect > $localUser.rew2.connect}
                        {$rew1id = $localUser.rew1.id}{$rew2id = $localUser.rew2.id}
                        {$rew1name = $localUser.rew1.login}{$rew2name = $localUser.rew2.login}
                    {else}
                        {$rew1id = $localUser.rew2.id}{$rew2id = $localUser.rew1.id}
                        {$rew1name = $localUser.rew2.login}{$rew2name = $localUser.rew1.login}
                    {/if}

                    <!-- TABLE ROW -->
                    <tr>
                        <!-- NAMES -->
                        <td class="center-align">
                            <b>{$localUser.base.login}</b><br>
                            {$localUser.base.team_name}<br>
                            <b>{$localUser.base.team_project}</b>
                        </td>

                        <!-- TOTAL MARKS -->
                        <td  class=" center-align">
                            <div class="chip pink accent-1">{$auth}</div>
                            <div class="chip pink accent-2">{$rewer}</div><br>
                            <div class="chip pink darken-2">{$adm}</div><br>
                            <div class="chip chipBotMargin purple lighten-3">{$points}</div>
                            <div class="chip chipBotMargin purple lighten-1">{$done}</div>
                        </td>

                        <!-- REVIEW 1 -->
                        <td class="chipsPadding center-align">
                            <div id="rewShowed_{$localUser.base.id}_0">
                                <b>{$rew1name}</b> <br>
                                <div class="chip teal accent-3 chipBotMargin">{$rew1}</div>
                                <div class="chip teal lighten-2">{$localUser.feed1}</div>
                               <!-- <div class="chip teal lighten-2">{$feed1}</div> -->
                                <div class="chip teal chipBotMargin">{$fin1}</div>
                            </div>
                        </td>

                        <!-- REVIEW 2 -->
                        <td class="chipsPadding center-align">
                            <div id="rewShowed_{$localUser.base.id}_1">
                                <b>{$rew2name}</b> <br>
                                <div class="chip teal accent-3 chipBotMargin">{$rew2}</div>
                                <div class="chip teal lighten-2">{$localUser.feed2}</div>
                                <!--<div class="chip teal lighten-2">{$feed2}</div> -->
                                <div class="chip teal chipBotMargin">{$fin2}</div>
                            </div>
                        </td>

                        <!-- ACTIONS -->
                        <td>
                            <i class="material-icons"
                               onclick="aPreview(
                                   '{$assets}',
                                    {$localUser.base.id},
                                    {$user.selectedProject})">find_in_page</i>

                            <i class="material-icons"
                               onclick="aGraph(
                                    {$localUser.base.id},
                                   '{$index}',
                                    {$smarty.const.TIME_STUCK},
                                    {$smarty.const.CHAR_STUCK})">trending_up</i>

                            <i class="material-icons"
                               {if $localUser.fav == true}style="color: red;"{/if}
                               id="fav_{$localUser.base.id}"
                               onclick="aUpdateFavorite(
                                   '{$index}',
                                    {$localUser.base.id})">favorite</i>
                        </td>
                    </tr>
                {/if}
            {/foreach}
            </tbody>
        </table>
    </div>
</div>