<script>
{literal}

    /**
     * Webpage constructor function
     * Initialize elements
     */
    $(document).ready(function() {

        // Update topbar
        $('#topReviewer').css('color', 'red');

        // Initialize elements
        $('select').material_select();
        $('.caret').text('');

        // Update pdf container size for background
        var window_size = $(window).height();
        $('#pdfContainer').height(window_size);

        // Update deadlines visibilities
        {/literal}
        aToogleVisibilityReview('{$deadlinedata.review.from}', '{$deadlinedata.review.to}');
        aToogleVisibilityFeedback('{$deadlinedata.feedback.from}', '{$deadlinedata.feedback.to}');
        aToogleVisibilityFinal('{$deadlinedata.final.from}', '{$deadlinedata.final.to}');
        {literal}

    });

    /**
     * Save reviewer form
     */
    function aSaveChanges(){
        $( "#fReviewer" ).submit();
    }

{/literal}
</script>
