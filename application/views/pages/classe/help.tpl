<!-- MODAL WITH USER IMPORT -->
    <div id="mHelp" class="modal" style="width: 70% !important">
        <div class="modal-content">

            <div class="col s6">
                <b>{ci_language line="Mark"}</b>
                <ul>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #FD82AB">label</i>
                        <span><b>{ci_language line="Author"}</b></span>
                         &nbsp;- {ci_language line="Total mark for user as author"}
                    </li>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #FC4482">label</i>
                        <span><b>{ci_language line="Reviewer"}</b></span>
                        &nbsp;- {ci_language line="Total mark for user as reviewer"}
                    </li>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #C01E5C">label</i>
                        <span><b>{ci_language line="Admin"}</b></span>
                        &nbsp;- {ci_language line="Total mark for user together"}
                    </li>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #CD95D6">label</i>
                        <span><b>{ci_language line="Points"}</b></span>
                        &nbsp;- {ci_language line="Total points for the user"}
                    </li>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #AA4CBA">label</i>
                        <span><b>{ci_language line="Done"}</b></span>
                        &nbsp;- {ci_language line="Total done for all chapters"}
                    </li>
                </ul>
            </div>
            <div class="col s6">
                <b>{ci_language line="Review"}</b>
                <ul>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #32E8B7">label</i>
                        <span><b>{ci_language line="Review"}</b></span>
                        &nbsp;- {ci_language line="Review for the author"}
                    </li>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #52B6AC">label</i>
                        <span><b>{ci_language line="Feedback"}</b></span>
                        &nbsp;- {ci_language line="Feedback given by author"}
                    </li>
                    <li class="collection-item valign-wrapper">
                        <i class="material-icons small valign" style="color: #159588">label</i>
                        <span><b>{ci_language line="Final"}</b></span>
                        &nbsp;- {ci_language line="Final review for author"}
                    </li>
                </ul>
            </div>



        </div>

        <!-- FOOTER -->
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">{ci_language line="Close"}</a>
        </div>
    </div>

