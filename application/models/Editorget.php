<?php

/**
 * Class EditorGet
 */
class EditorGet extends CI_Model
{
	/**
	 * EditorGet constructor.
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Function to get chapter done
	 * @param $chapterID - integer : id of the chapter
	 * @param $userData - array : data of the user
	 * @return int - chapter done
	 */
	public function getDoneByChapter($chapterID, $userData) {

		// Select conditions and try to get the result
		$this->db->where('project', $userData['selectedProject']);
		$this->db->where('user', $userData['id']);
		$this->db->where('chapter', $chapterID);
		$result = $this->db->get('chapter_done')->result();
		if (empty($result) || !is_array($result)) return 0;
		return current($result)->done;
	}

	/**
	 * Function to get finishing students by chapter
	 * @param $chapterID - integer : id of the chapter
	 * @param $userData - array : data of the user
	 * @return int - the finishing percents
	 */
	public function getFinishingByChapter($chapterID, $userData) {

		// Get total users number
		$this->db->where('project', $userData['selectedProject']);
		$totalUsers = $this->db->get('user_project')->num_rows();

		// Get done 75 < done users
		$this->db->where('project', $userData['selectedProject']);
		$this->db->where('chapter', $chapterID);
		$this->db->where('done >=', FINISHING_POINT);
		$finishing = $this->db->get('chapter_done')->num_rows();

		// Calculate the result
		if ($totalUsers < 1 || $finishing < 1) return 0;
		return round($finishing / $totalUsers * 100);
	}

	/**
	 * Function to get lead percents
	 * @param $userData - array : data of the user
	 * @return int - the lead percents
	 */
	public function getLeadPercents($userData) {

		// Get the golder average lines
		$this->db->select("points as max");
		$this->db->order_by("points", "desc");
		$this->db->limit(STAT_GOLD_AVERAGE);
		$Tops = $this->db->get('user_project')->result();

		// Calculate maximum
		$sum = $num = 0;
		foreach ($Tops as $row)
		{
			$sum = $sum + $row->max;
			$num = $num + 1;
		}
		$maximum = round($sum / $num);

		// Calculate current users
		$this->db->select("points");
		$this->db->where('project', $userData['selectedProject']);
		$this->db->where('user', $userData['id']);
		$points = $this->db->get('user_project')->result();

		// Calculate the result
		if (empty($points) || !is_array($points)) return 0;
		if (current($points)->points < 1 || $maximum < 1) return 0;
		return round(current($points)->points / $maximum * 100);
	}
}