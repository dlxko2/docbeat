<?php

/**
 * Class EditorSave
 */
class EditorSave extends CI_Model
{
	/**
	 * EditorSave constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->load->model("logmodel");
	}

	/**
	 * Function to save the chapter done information
	 * @param $userData - array : data of the user
	 * @param $chapterID - integer : id of the chapter
	 * @param $content - string : content
	 * @param $chapterData - array : data of the chapter
	 * @return boolean - the result of the function
	 */
	public function saveChapterDone($userData, $chapterID, $content, $chapterData) {

		// Percent characters
		$expCh = $chapterData['words'];
		$curCh = strlen($content);
		$pctCh =  ($expCh != 0 && $curCh != 0) ?  ($curCh / $expCh * 100) : 0;
		if ($pctCh > 100) $pctCh = 100;

		// Percent cites
		$expCi = $chapterData['cites'];
		preg_match_all("/(cite)/", $content, $matches);
		$curCi = count($matches[0]);
		$pctCi =  ($expCi != 0 && $curCi != 0) ? ($curCi / $expCi * 100) : 0;
		if ($pctCi > 100) $pctCi = 100;

		// Calculate total
		$totalPercent = round(($pctCh + $pctCi) / 2);

		// Create data array
		$dbArray1 = [
			'chapter'   => $chapterID,
			'user'      => $userData['id'],
			'done'      => $totalPercent,
			'project'   => $userData['selectedProject']
		];

		// Try to find any record in DB
		$this->db->where('project', $userData['selectedProject']);
		$this->db->where('user', $userData['id']);
		$this->db->where('chapter', $chapterID);
		$result = $this->db->get('chapter_done')->result();

		// Insert or update the entry
		if (empty($result))
			$this->db->insert('chapter_done', $dbArray1);
		else {
			$this->db->where('id', current($result)->id);
			$this->db->update('chapter_done', $dbArray1);
		}

		// Check the result
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Cant regenerate chapter done:" . implode("|", $dbArray1));
			return false;
		}

		// Return the default results
		return true;
	}

	/**
	 * Function to get information about intime of stuck
	 * @param $userID - integer : id of the user
	 * @param $type - char : stamp type
	 * @param $projectID - integer : id of the project
	 * @param $time - integer : miliseconds allowed
	 * @return bool - the result of the function
	 */
	private function getInTime($userID, $type, $projectID, $time) {

		// Get the latest time stamp
		$this->db->select('date');
		$this->db->where('user', $userID);
		$this->db->where('type', $type);
		$this->db->where('project', $projectID);
		$this->db->order_by('date', 'DESC')->limit(1);
		$result = $this->db->get('student_stucks')->result();

		// Check if in time
		if (!empty($result) && is_array($result)){

			// Calculate the times
			$timeOld = strtotime(current($result)->date);
			$expire = strtotime('+' . ($time/1000) . ' seconds', $timeOld);
			$now = strtotime("now");

			// If not allowed debug and return
			if ($now < $expire)
			{
				$logNow = date("Y-m-d H:i:s", $now);
				$logExp = date("Y-m-d H:i:s", $expire);
				$this->logmodel->lW("Timestamp sooner:" . $logNow . '/' . $logExp . '/' . $type);
				return false;
			}
		}
		return true;
	}

	/**
	 * Function to save the time stack
	 * @param $userData - array : user data
	 * @return boolean - the result of function
	 */
	public function saveTimeStack($userData) {

		// Check intime
		if (!$this->getInTime($userData['id'], 'T', $userData['selectedProject'], TIME_STUCK))
			return true;

		// Save stamp into database
		$this->db->insert('student_stucks', [
				'user'    => $userData['id'],
				'type'    => 'T',
				'project' => $userData['selectedProject']
			]);

		// Check the result
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Can't insert timestamp!");
			return false;
		}

		// Return the default result
		return true;
	}

	/**
	 * Function to save the char stack
	 * @param $userData - array : user data
	 * @return boolean - the result of function
	 */
	public function saveCharStack($userData) {

		// Check intime
		if (!$this->getInTime($userData['id'], 'C', $userData['selectedProject'], CHAR_INTIME))
			return true;
		// Save stamp into database
		$this->db->insert('student_stucks', [
			'user'    => $userData['id'],
			'type'    => 'C',
			'project' => $userData['selectedProject']
		]);

		// Check the result
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Can't insert charstamp!");
			return false;
		}

		// Return the default result
		return true;
	}

	/**
	 * Function to save the total chapter done calculated
	 * @param $userData - array : the data of user
	 * @return boolean - the result of the function
	 */
	public function saveTotalDone($userData) {

		// Select all chapters ids for the project
		$this->db->select('id');
		$this->db->where('project', $userData['selectedProject']);
		$chapterQuery = $this->db->get('chapters');

		// For each chapter found
		$totalCount = $totalSum = 0;
		foreach ($chapterQuery->result() as $chapter) {

			// Get done for this chapter in chapter_done table
			$this->db->select('done');
			$this->db->where('project', $userData['selectedProject']);
			$this->db->where('chapter', $chapter->id);
			$this->db->where('user', $userData['id']);
			$result = $this->db->get('chapter_done')->result();

			// Update the variables
			if (empty($result)) $totalSum += 0;
			else $totalSum += current($result)->done;
			++$totalCount;
		}

		// Calculate the total done average
		if($totalCount < 1 || $totalSum < 1) $totalDone = 0;
		else $totalDone = round($totalSum/$totalCount);

		// Save the mark and the stamp
		$this->saveMarkDone($userData, $totalDone);
		$this->saveStampDone($userData, $totalDone);

		// Return the default result
		return true;
	}

	/**
	 * Save the done mark into student marks
	 * @param $userData - array : user data
	 * @param $done - integer : total done
	 * @return boolean - the result of function
	 */
	private function saveMarkDone($userData, $done) {

		// Prepare the array
		$dbArray1 = [
			'done'    => $done,
			'user'    => $userData['id'],
			'project' => $userData['selectedProject']
		];

		// Look if result already exists
		$this->db->where('user', $userData['id']);
		$this->db->where('project', $userData['selectedProject']);

		// Update or insert data into DB
		if ($this->db->count_all_results('student_marks') == 0) {
			$this->db->insert('student_marks', $dbArray1);
		} else {
			$this->db->where('user', $userData['id']);
			$this->db->where('project', $userData['selectedProject']);
			$this->db->update('student_marks', $dbArray1);
		}

		// Check the result
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Can't regenerate mark:" . implode("|", $dbArray1));
			return false;
		}

		// Return the default result
		return true;
	}

	/**
	 * Function to save the done stamp
	 * @param $userData - array : user data
	 * @param $done - integer : percent done
	 * @return boolean - the result of function
	 */
	private function saveStampDone($userData, $done) {

		// Prepare the array and save it into DB
		$this->db->insert('student_stucks', [
			'user'    => $userData['id'],
			'type'    => 'D',
			'value'   => $done,
			'project' => $userData['selectedProject']
		]);

		// Check the result
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Can't insert timestamp!");
			return false;
		}

		// Return the default result
		return true;
	}
}