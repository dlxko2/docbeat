var header =
`<li id="question` + reviewNumber + `">
 <div class="collapsible-header">
 <i class="material-icons" id="questionIcon` + reviewNumber + `" style="color:red">font_download</i>
 <span id="name` + reviewNumber + `">{ci_language line="Question"}` + (reviewNumber+1) + `</span>
 <span class="secondary-content">`;

var headerDel =
`<i class="material-icons" onclick="aDeleteQuestion(` + reviewNumber + `)">block</i>`;

var headerDelL =
`<i class="material-icons" style="color:gray;">block</i>`;

var headerSec =
`<input name="iType` + reviewNumber + `" type="radio"
    value="R" id="reviewRadio` + reviewNumber + `_1"/>
 <label for="reviewRadio` + reviewNumber + `_1">{ci_language line="Review"}</label>
 <input name="iType` + reviewNumber + `" type="radio"
    value="F" id="reviewRadio` + reviewNumber + `_2" />
 <label for="reviewRadio` + reviewNumber + `_2">{ci_language line="Feedback"}</label>
 <input name="iType` + reviewNumber + `" type="radio"
    value="E" id="reviewRadio` + reviewNumber + `_3" />
 <label for="reviewRadio` + reviewNumber + `_3">{ci_language line="Finish"}</label>
 &nbsp;<input type="checkbox" id="quality` + reviewNumber + `"/>
 <label for="quality` + reviewNumber + `">Quality</label>
 <input type="hidden" name="iQuality[]" value="` + quality + `" id="qualityH` + reviewNumber + `">
 </span>
 </div>`;

var bodyContent =
`<div class="collapsible-body">
 <div class="row">
 <div class="input-field col s6">
 <input type="hidden" name="iID[]" value="` + id + `">
 <textarea placeholder="{ci_language line="Question text"}" name="iAreaContent[]" id="areaContent`
 + reviewNumber + `" class="materialize-textarea questionTextBox">` + content + `</textarea>
 </div>
`;

var bodyQuantity =
`<div class="input-field col s6 quantitySelectBox">
 <select name="iQuantity[]" id="quantity` + reviewNumber + `">
 <option value="0" selected>{ci_language line="Don't use quantity"}</option>
 {foreach from=$chapterdata item=chapter}
 <option value="{$chapter.id}">{$chapter.name} - {$chapter.extra} ({$chapter.extra_value})</option>
 {/foreach}
 </select>
 <label>{ci_language line="Quantity"}</label>
 </div></div></li>`;