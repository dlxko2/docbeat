<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Reviewer
 */
class Reviewer extends CI_Controller {

	/**
	 * $userData variable.
	 * Stores session data for class
	 */
	private $userData;

	/**
	 * Reviewer constructor.
	 * Get session, set gettext and models
	 * Check if user is logged and project selected
	 */
    public function __construct()
    {
        parent::__construct();

	    $this->load->model("reviewsget");
	    $this->load->model("reviewsfunc");
	    $this->load->model("documentationget");
	    $this->load->model("authorget");
	    $this->load->model("editorget");
	    $this->load->model("reviewersave");
	    $this->load->model("welcomemodel");
	    $this->load->model("deadlinesget");

	    $this->userData = $this->ss->userdata();
	    if (!array_key_exists('id', $this->userData) || !array_key_exists('selectedProject', $this->userData))
		    redirect('/welcome/index', 'location');
    }

	/**
	 * Main view of reviewer page
	 * Gets the data and save them in template
	 */
    public function index() {

    	// Shorten variables
	    $myProj = $this->userData['selectedProject'];
	    $snapshotData = [[],[]];
	    $teamData = [[],[]];

	    // Data for reviews deadlines - optional deadline for each review
		$deadlinesData = $this->deadlinesget->getReviewDeadlinesByProject($this->userData);
	    // Data for the chapters - empty or data for each chapter
    	$chaptersData = $this->documentationget->getChaptersByProject($myProj);
	    // Data for the questions - empty or data for each question
	    $questionsData = $this->reviewsget->getQuestionsByProject($myProj);
	    // Data for two reviewers - empty or data for each person
	    $targetData = $this->reviewsget->getTargetsByUser($this->userData['id']);
	    // Data for two authors - empty or data for each person
	    $reviewersData = $this->reviewsget->getReviewersByUser($this->userData['id']);
	    // Mark data - array with datas or default array as return
	    $markData = $this->authorget->getMarkInfoByUser($this->userData);

	    // Create connection array from reviewers data
	    $cArray = $iArray = [];
	    if (isset($reviewersData[0]) && $reviewersData[0]['id'] != 0) {
	    	array_push($cArray, $reviewersData[0]['connection']);
		    array_push($iArray, $reviewersData[0]['id']);
	    }
	    if (isset($reviewersData[1]) && $reviewersData[1]['id'] != 0) {
	    	array_push($cArray, $reviewersData[1]['connection']);
		    array_push($iArray, $reviewersData[1]['id']);
	    }

	    // Data for the results - empty or result data
	    $resultsData = $this->authorget->getResultByConnectionsArray($cArray);

	    // Data for teams - empty or team members data
	    // Data for snapshots - empty or snapshot file names
	    if (isset($cArray[0])) {
		    $teamData[0] = $this->welcomemodel->getTeamMembersByUser(['id' => $iArray[0], 'selectedProject' => $myProj]);
		    $snapshotData[0] = $this->reviewsfunc->getSnapshotsByUser(['id' => $iArray[0], 'selectedProject' => $myProj]);
	    }
	    if (isset($cArray[1])) {
		    $teamData[1] = $this->welcomemodel->getTeamMembersByUser(['id' => $iArray[1], 'selectedProject' => $myProj]);
		    $snapshotData[1] = $this->reviewsfunc->getSnapshotsByUser(['id' => $iArray[1], 'selectedProject' => $myProj]);
	    }

		// Save datas to the Smarty processor
	    $this->sm->assign('markdata',$markData);
	    $this->sm->assign('targets', count($cArray));
	    $this->sm->assign('snapshotdata0',$snapshotData[0]);
	    $this->sm->assign('snapshotdata1',$snapshotData[1]);
	    $this->sm->assign('deadlinedata',$deadlinesData);
	    $this->sm->assign('teamdata0',$teamData[0]);
	    $this->sm->assign('teamdata1',$teamData[1]);
	    $this->sm->assign('chaptersdata',$chaptersData);
	    $this->sm->assign('resultsdata',$resultsData);
	    $this->sm->assign('targetdata',$targetData);
	    $this->sm->assign('reviewersdata',$reviewersData);
	    $this->sm->assign('questionsdata',$questionsData);
	    $this->sm->assign('user',$this->userData);
        $this->sm->assign('index',base_url('index.php/'));
        $this->sm->assign('assets',base_url('assets/'));
        $this->sm->display('pages/reviewer/reviewer.tpl');
    }

	/**
	 * Function to save reviewer form
	 * Gets data from post input and save them into DB
	 * Also updates the marks information
	 */
    public function save() {

	    // Shorten variables
	    $myProj = $this->userData['selectedProject'];
	    $source = $this->userData['id'];

	    // Save reviews and finals data from the post into database
    	$postData = $this->input->post();
	    $this->reviewersave->saveResponds($postData);

	    // Get reviewers
	    $reviewersData = $this->reviewsget->getReviewersByUser($this->userData['id']);

	    // Update marks for author 1
	    if (isset($reviewersData[0]['id'])) {
	    	$rew1ID = $reviewersData[0]['id'];
		    $points1 = $this->editorget->getLeadPercents(['selectedProject' => $myProj, 'id' => $rew1ID]);
		    $this->reviewersave->updateMarks($source, $rew1ID, $myProj, $points1);
		    $this->reviewersave->updateMarkAdmin($rew1ID, $myProj, $points1);
	    }

	    // Update marks for author 2
	    if (isset($reviewersData[1]['id'])) {
		    $rew2ID = $reviewersData[1]['id'];
		    $points2 = $this->editorget->getLeadPercents(['selectedProject' => $myProj, 'id' => $rew2ID]);
		    $this->reviewersave->updateMarks($source, $rew2ID, $myProj, $points2);
		    $this->reviewersave->updateMarkAdmin($rew2ID, $myProj, $points2);
	    }

	    // Also update reviewer mark for current user with new responds
	    $points = $this->editorget->getLeadPercents(['selectedProject' => $myProj, 'id' => $source]);
	    $this->reviewersave->updateMarkAdmin($source, $myProj, $points);

	    // Show reviewer default page
	    redirect('/reviewer/index', 'location');
    }
}
