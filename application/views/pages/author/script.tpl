<script>
{literal}

    /**
     * Webpage constructor function
     * Initialize some elements
     */
    $(document).ready(function() {

        // Update topbar
        $('#topAuthor').css('color', 'red');

        // Initialize elements
        $('select').material_select();
        $('.caret').text('');

        // Update pdf container size for background
        window_size = $(window).height();
        $('#pdfContainer').height(window_size);

        // Update deadlines visibilities
        {/literal}
        aToogleVisibilityReview('{$deadlinedata.review.from}', '{$deadlinedata.review.to}');
        aToogleVisibilityFeedback('{$deadlinedata.feedback.from}', '{$deadlinedata.feedback.to}');
        aToogleVisibilityFinal('{$deadlinedata.final.from}', '{$deadlinedata.final.to}');
        {literal}
    });

    /**
     * Function to save author form
     */
    function aSaveChanges(){
        $( "#fAuthor" ).submit();
    }

{/literal}
</script>

