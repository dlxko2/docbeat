<div id="mPictures" class="modal">
    <div class="modal-content">

        <div class="slider">
            <ul class="slides">
                {foreach from=$pictures item=picture}
                <li>
                    <img src="{$assets}/datas/users/{$user.id}/{$user.selectedProject}/pictures/{$picture}">
                </li>
                {/foreach}


            </ul>
        </div>

        <!-- FOOTER -->
        <div class="modal-footer">
            <div class="col s10">
            <form role="form" id="fPictures" method="post" enctype="multipart/form-data"
                  accept-charset="utf-8" action="{$index}/editor/import">
                <div class="file-field input-field">
                    <div class="btn" id="pictureButton">
                        <span>{ci_language line="Picture"}</span>
                        <input type="file" multiple name="iPictures[]">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="{ci_language line="Upload pictures"}">
                    </div>
                </div>
            </form>
                </div>
            <div class="col s1">


                <div class="fixed-action-btn">
                    <a class="btn-floating btn-large red">
                        <i class="large material-icons">insert_photo</i>
                    </a>
                    <ul>
                        <li onclick="aPublishPicture()"><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
                        <li onclick="aSavePictures()"><a class="btn-floating orange darken-1"><i class="material-icons">cloud_done</i></a></li>
                        <li onclick="aRemoveImage('{$index}')"><a class="btn-floating red"><i class="material-icons">cloud_off</i></a></li>
                    </ul>
                </div>

        </div>
            </div>

    </div>