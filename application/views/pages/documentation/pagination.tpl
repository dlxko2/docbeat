<div class="row center-align">

    <!-- STEPS -->
    <ul class="pagination left">
        <li class="active"       onclick="aStep1()" id="pStep1">{ci_language line="INFO"}</li>
        <li class="waves-effect" onclick="aStep2()" id="pStep2">{ci_language line="BASE"}</li>
        <li class="waves-effect" onclick="aStep3()" id="pStep3">{ci_language line="CHAPTERS"}</li>
    </ul>

    <!-- FUNCTIONS -->
    <ul class="pagination right">

        <!-- PREVIEW -->
        <li onclick="aPreview('{$index}','{$assets}', {$project.id})">
            <i class="small material-icons cursorPointer">visibility</i>
        </li>

        <!-- SAVE -->
        <li onclick="aSaveChanges()">
            <i class="small material-icons cursorPointer">save</i>
        </li>

    </ul>

</div>