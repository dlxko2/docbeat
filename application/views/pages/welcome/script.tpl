{literal}<script>

    /**
     * Run script on page load
     * Initialize table and login button event
     */
    $(document).ready(function() {

        $('#tProjects').dataTable({
            bAutoWidth: false,
            "order": [[ 2, "desc" ]],
            "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": {/literal}"{ci_language line="Enter name here"}"{literal},
            "sInfo": "_START_ -_END_ {/literal}{ci_language line="of"}{literal} _TOTAL_",
            },
            "pageLength": 8
        });

        $( "#loginButton" ).click(function() {$( "#form1" ).submit();});
    });

    /**
     * Open modal after login request
     */
    function aLoginOpen() {
        $('#mLogin').openModal();
    }


{/literal}</script>