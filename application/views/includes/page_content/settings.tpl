<!-- MODAL WITH USER IMPORT -->
{if isset($user.permissions)}
<div id="mSettings" class="modal">
    <div class="modal-content">

            <!-- TITLE -->
            <h4>{ci_language line="Settings menu"}</h4>
            <div class="row" style="margin-bottom: 50px">
                <div class="row">

                    <!-- LANGUAGE -->
                    {if $user.permissions == 'A' || !isset($user.selectedProject)}
                    <div class="input-field col s12">
                    {else}
                    <div class="input-field col s4">
                    {/if}
                        <p><b>{ci_language line="Choose the language:"}</b></p>
                        <input class="with-gap" name="language" type="radio" value="sk"
                               id="slovak" {if $user.language == 'sk'}checked{/if}/>
                        <label for="slovak">{ci_language line="Slovak"}</label><br>
                        <input class="with-gap" name="language" type="radio" value="en"
                               id="english" {if $user.language == 'en'}checked{/if}/>
                        <label for="english">{ci_language line="English"}</label>
                    </div>

                    <!-- VISIBILITY -->
                    {if $user.permissions == 'S' && isset($user.selectedProject)}
                    <div class="input-field col s8">
                        <p><b>{ci_language line="Choose the visibility:"}</b></p>
                        <input class="with-gap" name="visibility" type="radio" value="1"
                               id="visible" checked/>
                        <label for="visible">{ci_language line="Visible"}</label><br>
                        <input class="with-gap" name="visibility" type="radio" value="0"
                               id="invisible"/>
                        <label for="invisible">{ci_language line="Invisible"}</label>
                    </div>
                    {/if}

                </div>
            </div>
    </div>

    <!-- FOOTER -->
    <div class="modal-footer">
        <a href="#!" class=" modal-action waves-effect waves-green btn-flat"
           onclick="aSendSettings('{$index}','{ci_language line="Settings will change after page reload"}')">
            {ci_language line="OK"}
        </a>
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">{ci_language line="Close"}</a>
    </div>
</div>
{/if}