-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Hostiteľ: localhost:3306
-- Čas generovania: Po 12.Sep 2016, 22:12
-- Verzia serveru: 5.7.13
-- Verzia PHP: 7.0.8

--
-- Version 0.8.0
--
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `docbeat`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `chapters`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `words` int(11) NOT NULL,
  `cites` int(11) NOT NULL,
  `extra` varchar(50) DEFAULT NULL,
  `extra_value` int(11) NOT NULL,
  `content` text NOT NULL,
  `hidden` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `chapters`:
--   `project`
--       `projects` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `chapter_done`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `chapter_done` (
  `id` int(11) NOT NULL,
  `chapter` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `done` int(11) NOT NULL,
  `project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `chapter_done`:
--   `chapter`
--       `chapters` -> `id`
--   `project`
--       `projects` -> `id`
--   `user`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `deadlines`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `deadlines` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `snapshot` date NOT NULL,
  `review_type` char(1) DEFAULT NULL,
  `review_end` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `deadlines`:
--   `project`
--       `projects` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `deadline_chapter`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `deadline_chapter` (
  `id` int(11) NOT NULL,
  `deadline` int(11) NOT NULL,
  `chapter` int(11) NOT NULL,
  `project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `deadline_chapter`:
--   `chapter`
--       `chapters` -> `id`
--   `deadline`
--       `deadlines` -> `id`
--   `project`
--       `projects` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `projects`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `user` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `base` text,
  `locked` tinyint(1) DEFAULT '0',
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `projects`:
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `questions`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `type` char(1) NOT NULL,
  `content` text NOT NULL,
  `quality` tinyint(1) DEFAULT '0',
  `quantity` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `questions`:
--   `project`
--       `projects` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `student_connect`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `student_connect` (
  `id` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `reviewer` int(11) NOT NULL,
  `project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `student_connect`:
--   `author`
--       `users` -> `id`
--   `reviewer`
--       `users` -> `id`
--   `project`
--       `projects` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `student_favorites`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `student_favorites` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `favorite_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `student_favorites`:
--   `user`
--       `users` -> `id`
--   `project`
--       `projects` -> `id`
--   `favorite_user`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `student_marks`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `student_marks` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0',
  `project` int(11) NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  `reviewer` int(11) NOT NULL DEFAULT '0',
  `done` int(11) NOT NULL DEFAULT '0',
  `rewier1` int(11) DEFAULT NULL,
  `rewier2` int(11) DEFAULT NULL,
  `author1` int(11) DEFAULT NULL,
  `author2` int(11) DEFAULT NULL,
  `rew1` int(11) NOT NULL DEFAULT '0',
  `rew2` int(11) NOT NULL DEFAULT '0',
  `feed1` int(11) NOT NULL DEFAULT '0',
  `feed2` int(11) NOT NULL DEFAULT '0',
  `fin1` int(11) NOT NULL DEFAULT '0',
  `fin2` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `student_marks`:
--   `user`
--       `users` -> `id`
--   `project`
--       `projects` -> `id`
--   `rewier1`
--       `users` -> `id`
--   `rewier2`
--       `users` -> `id`
--   `author1`
--       `users` -> `id`
--   `author2`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `student_results`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `student_results` (
  `Id` int(11) NOT NULL,
  `connect` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `type` char(1) NOT NULL,
  `question` int(11) NOT NULL,
  `content` text NOT NULL,
  `quality` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `student_results`:
--   `connect`
--       `student_connect` -> `id`
--   `owner`
--       `users` -> `id`
--   `question`
--       `questions` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `student_stucks`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `student_stucks` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `type` char(1) NOT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  `project` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `student_stucks`:
--   `user`
--       `users` -> `id`
--   `project`
--       `projects` -> `id`
--

--
-- Spúšťače `student_stucks`
--
DELIMITER $$
CREATE TRIGGER `updatePoints` AFTER INSERT ON `student_stucks` FOR EACH ROW UPDATE user_project
    SET points = points + 1
    WHERE user = NEW.user
    AND project = NEW.project
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `team_info`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `team_info` (
  `id` int(11) NOT NULL,
  `ref_project` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `project` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `team_info`:
--   `ref_project`
--       `projects` -> `id`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(10) NOT NULL,
  `password` varchar(64) NOT NULL,
  `permissions` char(1) NOT NULL,
  `language` varchar(2) NOT NULL DEFAULT 'sk'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `users`:
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `user_project`
--
-- Vytvorené: Po 12.Sep 2016, 20:11
--

CREATE TABLE `user_project` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- PREPOJENIA PRE TABUĽKU `user_project`:
--   `user`
--       `users` -> `id`
--   `project`
--       `projects` -> `id`
--   `team`
--       `team_info` -> `id`
--

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `project` (`project`);

--
-- Indexy pre tabuľku `chapter_done`
--
ALTER TABLE `chapter_done`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `chapter` (`chapter`),
  ADD KEY `user` (`user`),
  ADD KEY `project` (`project`);

--
-- Indexy pre tabuľku `deadlines`
--
ALTER TABLE `deadlines`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `project` (`project`);

--
-- Indexy pre tabuľku `deadline_chapter`
--
ALTER TABLE `deadline_chapter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `deadline` (`deadline`),
  ADD KEY `chapter` (`chapter`),
  ADD KEY `project` (`project`);

--
-- Indexy pre tabuľku `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexy pre tabuľku `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `project` (`project`);

--
-- Indexy pre tabuľku `student_connect`
--
ALTER TABLE `student_connect`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `author` (`author`),
  ADD KEY `reviewer` (`reviewer`),
  ADD KEY `project` (`project`);

--
-- Indexy pre tabuľku `student_favorites`
--
ALTER TABLE `student_favorites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `project` (`project`),
  ADD KEY `favorite_user` (`favorite_user`);

--
-- Indexy pre tabuľku `student_marks`
--
ALTER TABLE `student_marks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `project` (`project`),
  ADD KEY `rewier1` (`rewier1`),
  ADD KEY `rewier2` (`rewier2`),
  ADD KEY `author1` (`author1`),
  ADD KEY `author2` (`author2`);

--
-- Indexy pre tabuľku `student_results`
--
ALTER TABLE `student_results`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id` (`Id`),
  ADD KEY `question` (`question`),
  ADD KEY `connect` (`connect`),
  ADD KEY `owner` (`owner`);

--
-- Indexy pre tabuľku `student_stucks`
--
ALTER TABLE `student_stucks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `project` (`project`);

--
-- Indexy pre tabuľku `team_info`
--
ALTER TABLE `team_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `ref_project` (`ref_project`);

--
-- Indexy pre tabuľku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexy pre tabuľku `user_project`
--
ALTER TABLE `user_project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `project` (`project`),
  ADD KEY `team` (`team`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `chapter_done`
--
ALTER TABLE `chapter_done`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `deadlines`
--
ALTER TABLE `deadlines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `deadline_chapter`
--
ALTER TABLE `deadline_chapter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `student_connect`
--
ALTER TABLE `student_connect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `student_favorites`
--
ALTER TABLE `student_favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `student_marks`
--
ALTER TABLE `student_marks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `student_results`
--
ALTER TABLE `student_results`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `student_stucks`
--
ALTER TABLE `student_stucks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `team_info`
--
ALTER TABLE `team_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `user_project`
--
ALTER TABLE `user_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `chapters`
--
ALTER TABLE `chapters`
  ADD CONSTRAINT `chapters_ibfk_1` FOREIGN KEY (`project`) REFERENCES `projects` (`id`);

--
-- Obmedzenie pre tabuľku `chapter_done`
--
ALTER TABLE `chapter_done`
  ADD CONSTRAINT `chapter_done_ibfk_1` FOREIGN KEY (`chapter`) REFERENCES `chapters` (`id`),
  ADD CONSTRAINT `chapter_done_ibfk_2` FOREIGN KEY (`project`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `chapter_done_ibfk_3` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Obmedzenie pre tabuľku `deadlines`
--
ALTER TABLE `deadlines`
  ADD CONSTRAINT `deadlines_ibfk_1` FOREIGN KEY (`project`) REFERENCES `projects` (`id`);

--
-- Obmedzenie pre tabuľku `deadline_chapter`
--
ALTER TABLE `deadline_chapter`
  ADD CONSTRAINT `deadline_chapter_ibfk_1` FOREIGN KEY (`chapter`) REFERENCES `chapters` (`id`),
  ADD CONSTRAINT `deadline_chapter_ibfk_2` FOREIGN KEY (`deadline`) REFERENCES `deadlines` (`id`),
  ADD CONSTRAINT `deadline_chapter_ibfk_3` FOREIGN KEY (`project`) REFERENCES `projects` (`id`);

--
-- Obmedzenie pre tabuľku `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`project`) REFERENCES `projects` (`id`);

--
-- Obmedzenie pre tabuľku `student_connect`
--
ALTER TABLE `student_connect`
  ADD CONSTRAINT `student_connect_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_connect_ibfk_2` FOREIGN KEY (`reviewer`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_connect_ibfk_3` FOREIGN KEY (`project`) REFERENCES `projects` (`id`);

--
-- Obmedzenie pre tabuľku `student_favorites`
--
ALTER TABLE `student_favorites`
  ADD CONSTRAINT `student_favorites_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_favorites_ibfk_2` FOREIGN KEY (`project`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `student_favorites_ibfk_3` FOREIGN KEY (`favorite_user`) REFERENCES `users` (`id`);

--
-- Obmedzenie pre tabuľku `student_marks`
--
ALTER TABLE `student_marks`
  ADD CONSTRAINT `student_marks_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_marks_ibfk_2` FOREIGN KEY (`project`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `student_marks_ibfk_3` FOREIGN KEY (`rewier1`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_marks_ibfk_4` FOREIGN KEY (`rewier2`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_marks_ibfk_5` FOREIGN KEY (`author1`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_marks_ibfk_6` FOREIGN KEY (`author2`) REFERENCES `users` (`id`);

--
-- Obmedzenie pre tabuľku `student_results`
--
ALTER TABLE `student_results`
  ADD CONSTRAINT `student_results_ibfk_1` FOREIGN KEY (`connect`) REFERENCES `student_connect` (`id`),
  ADD CONSTRAINT `student_results_ibfk_2` FOREIGN KEY (`owner`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_results_ibfk_3` FOREIGN KEY (`question`) REFERENCES `questions` (`id`);

--
-- Obmedzenie pre tabuľku `student_stucks`
--
ALTER TABLE `student_stucks`
  ADD CONSTRAINT `student_stucks_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_stucks_ibfk_2` FOREIGN KEY (`project`) REFERENCES `projects` (`id`);

--
-- Obmedzenie pre tabuľku `team_info`
--
ALTER TABLE `team_info`
  ADD CONSTRAINT `team_info_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `projects` (`id`);

--
-- Obmedzenie pre tabuľku `user_project`
--
ALTER TABLE `user_project`
  ADD CONSTRAINT `user_project_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_project_ibfk_2` FOREIGN KEY (`project`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `user_project_ibfk_3` FOREIGN KEY (`team`) REFERENCES `team_info` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
