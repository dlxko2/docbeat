{include file='includes/page_content/head.tpl'}
{include file='includes/page_content/topbar.tpl'}
{include file='includes/packages/pkg_jquery.tpl'}
{include file='includes/packages/pkg_materialize.tpl'}
{include file='includes/page_content/settings.tpl'}

<link rel="stylesheet" href="{$assets}/{$smarty.const.styles}/author.css">

<div class="row" id="mainRow">

    <div class="col s6" id="editContainer">

        <form role="form" id="fAuthor" method="post" enctype="multipart/form-data"
              accept-charset="utf-8" action="{$index}/author/save">

            {include file='pages/author/toolbox.tpl'}
            {include file='pages/author/pagination.tpl'}
            {include file='pages/author/content.tpl'}

        </form>

    </div>

    <div class="col s6" id="pdfContainer">
        {include file='pages/author/preview.tpl'}
    </div>

</div>

{include file='pages/author/script.tpl'}
<script src="{$assets}/{$smarty.const.scripts}/author.js"></script>
{include file='includes/page_content/finish.tpl'}
