<?php

/**
 * Class DocumentationFunc
 */
class DocumentationFunc extends CI_Model
{
	/**
     * DocumentationFunc constructor.
     */
    function __construct() {
        parent::__construct();
	    $this->load->model("logmodel");
    }

	/**
	 * Create PDF preview for admin
	 * @param $uD - array : user data from session
	 * @param $pD - array : project data from database
	 * @param $chD - array :  chapters data from database
	 * @return array - the result of the function
	 */
	public function createPreview($uD, $pD, $chD){

		// Check the input
		if (empty($uD) || empty($pD) || empty($chD)) {
			$this->logmodel->lE("Wrong input parameters:" . implode("|", $uD) . implode("|", $pD) . implode("|", $chD));
			return ['result' => false, 'content' => "Internal error!"];
		}

		// Prepare path for data folder
        $dF = PROJECTDATA . $uD['selectedProject'] . '/preview/';

		// Cleanup preview files
		if (!$this->cleanPFiles($dF)) return ['result' => false, 'content' => "Can't delete files!"];

		// Generate the main.tex file
	    if (!$this->genPMain($dF, $chD, $pD, $uD)) return ['result' => false, 'content' => "Can't create main file!"];

		// Generate the chapters for process
        if (!$this->genPChapters($chD, $uD)) return ['result' => false, 'content' => "Can't create chapter!"];

		// Generate preview files
        if (!$this->genPFiles($uD)) return ['result' => false, 'content' => "Can't create files!"];

		// Create the PDF from the Latex and return the result
		$result = $this->genPPDF($dF);
		return $result;
    }

	/**
	 * Remove all files from preview directory
	 * @param $dataFolder - string : path to preview data path
	 * @return boolean - the result of function
	 */
	private function cleanPFiles ($dataFolder) {

		// Check if data folder already exists
		if (!file_exists($dataFolder)) {
			mkdir($dataFolder, DATA_PERMISSIONS, true);
			exec ("chmod -R " . CHMOD_PERMISSIONS . " " . $dataFolder);
		}

		// Delete all files inside
		$files = glob($dataFolder . '*');
		foreach($files as $file)
			if (is_file($file))
				if (!unlink($file)) {
					$this->logmodel->lE("Can't delete preview file:" . $file);
					return false;
				}

		// Return the default result
		return true;
	}

	/**
	 * Generate chapters files for preview
	 * @param $chaptersData - array : chapters data from DB
	 * @param $userData - array : user data from session
	 * @return boolean - the result of the operation
	 */
	private function genPChapters ($chaptersData, $userData) {

		// For each db entry generate the file
		$proj = $userData['selectedProject'];
		foreach ($chaptersData as $chapter)
			if (!file_put_contents(PROJECTDATA . $proj . '/preview/' . $chapter['id'] . '.tex', $chapter['content'])) {
				$this->logmodel->lE("Can't create chapter file:" . $chapter['id']);
				return false;
			}

		// Return the default result
		return true;
	}

	/**
	 * Generate PDF with pdflatex
	 * @param $dataFolder - string : data folder for preview
	 * @return array - the result of the function
	 */
	private function genPPDF ($dataFolder) {

		// Prepare file names
		$texFot = TEXBIN . 'texfot ';
		$pdfLatex = TEXBIN . 'pdflatex ';
		$test = "";
		$logFile = $dataFolder . 'main.log';

		// Depending on texfot run the pdflatex
		if (TEXFOT) exec('cd ' . $dataFolder . ' && ' . $texFot . $pdfLatex . 'main.tex', $test);
		else exec('cd ' . $dataFolder . ' && ' . $pdfLatex . 'main.tex', $test);

		// Update the permissions
		exec ("chmod -R " . CHMOD_PERMISSIONS . " " . $dataFolder);

		// Check for errors
		if (!file_exists($logFile)) return ['result' => false, 'content' => "Can't find any output!"];
		$LogFile = file_get_contents($logFile);
		$ErrorCheck = preg_match("/Fatal error occurred, no output PDF file produced!/", $LogFile);

		// Send the result
		if ($ErrorCheck) {
			$TestOut = implode("<br>", $test);
			return ['result' => false, 'content' => $TestOut];
		}

		// Return the default result
		return ['result' => true, 'content' => ''];
	}

	/**
	 * Copy project files into preview folder
	 * @param $userData - array : user data from session
	 * @return boolean - the result of operation
	 */
	private function genPFiles ($userData) {

		// Prepare the paths
		$projectData = PROJECTDATA . $userData['selectedProject'] . "/files/";
		$userLocation = PROJECTDATA . $userData['selectedProject'] . "/preview/";

		// For each file copy
		$files = glob($projectData . "*.*");
		foreach($files as $file)
		{
			$NewFile = str_replace($projectData,$userLocation,$file);
			if (!file_exists($NewFile))
				if (!copy($file, $NewFile)) {
					$this->logmodel->lE("Can't create base file:" . $NewFile);
					return false;
				}
		}

		// Return the base result
		return true;
	}

	/**
	 * Generate main.tex for preview
	 * @param $dataFolder - string : data folder for preview
	 * @param $chaptersData - array : chapters data from DB
	 * @param $projectData - array : project data from DB
	 * @param $userData - array : user data from session
	 * @return boolean - the result of operation
	 */
	private function genPMain ($dataFolder, $chaptersData, $projectData, $userData) {

		// Append chapters
		$replaceChapters = "";
		foreach ($chaptersData as $chapter)
			$replaceChapters .= "\input{" . $chapter['id'] . "}";

		// Update keywords
		$withTitle = preg_replace("/{TITLE}/", $projectData['name'], $projectData['base']);
		$withAuthor = preg_replace("/{AUTHOR}/", $userData['login'], $withTitle);
		$withChapters = preg_replace("/{INCLUDES}/", $replaceChapters, $withAuthor);

		// Save the main file
		if (!file_put_contents($dataFolder . 'main.tex', $withChapters)) {
			$this->logmodel->lE("Can't create main file");
			return false;
		}

		// Return the default result
		return true;
	}

	/**
	 * Save files from project creation form
	 * @param $files - array : $_FILES
	 * @param $projectID - int : id of the project
	 * @return boolean - the result of the operation
	 */
	public function addFiles($files, $projectID)
    {
    	// Check input files
    	if (empty($files)) {
		    $this->logmodel->lW("No files but requested upload");
		    return true;
	    }

	    // Check input project
	    if (empty($projectID)) {
		    $this->logmodel->lE("Wrong input project argument:" . $projectID);
		    return false;
	    }

	    // Check if any file in list
        if (empty(current($files['iProjectFiles']['name']))) {
	        return true;
        }

        // Set up upload path
        $uploadPath = PROJECTDATA . $projectID . "/files/";
        if (!file_exists($uploadPath)) {
	        mkdir($uploadPath, DATA_PERMISSIONS, true);
	        exec ("chmod -R " . CHMOD_PERMISSIONS . " " . PROJECTDATA . $projectID);
        }

        // Load input
        $this->load->library('upload');
        $cpt = count($_FILES['iProjectFiles']['name']);

        // Process every file
        for($i=0; $i<$cpt; $i++)
        {
            // Modify format of input file
            $_FILES['userfile']['name']= $files['iProjectFiles']['name'][$i];
            $_FILES['userfile']['type']= $files['iProjectFiles']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['iProjectFiles']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['iProjectFiles']['error'][$i];
            $_FILES['userfile']['size']= $files['iProjectFiles']['size'][$i];

            // Try to upload new file
            $this->upload->initialize($this->setUploadOptions($uploadPath));
            if ($this->upload->do_upload() == false) {
	            $result = $this->CIcallError($this->upload->display_errors());
	            $this->logmodel->lE("Can't upload files:" . $result);
            }
        }

        // Return the default result
	    return true;
    }

	/**
	 * Function to set upload options
	 * @param $uploadPath - string : path for upload files
	 * @return array - the config for upload
	 */
	private function setUploadOptions($uploadPath)
    {
        $config = [];
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = '*';
        $config['max_size']      = '10000';
        $config['overwrite']     = FALSE;
        return $config;
    }

	/**
	 * Get all project files from dir
	 * @param $projectID - int : id of the project
	 * @return string - uploaded files string
	 */
	public function getProjectFiles($projectID) {

		// Create or check the project file path
        $directory = PROJECTDATA . $projectID . "/files/";
		if (!file_exists($directory)) {
			mkdir($directory, DATA_PERMISSIONS, true);
			exec ("chmod -R " . CHMOD_PERMISSIONS . " " . PROJECTDATA . $projectID);
		}

		// Try to find the files
        $outputArray = array_diff(scandir($directory), ['..', '.']);

		// Generate the result and return it
        $result = "";
        foreach ($outputArray as $file) $result .= $file . " ";
        return $result;
    }
}