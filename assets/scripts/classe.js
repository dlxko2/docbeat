/**
 * Function to preview user PDF file
 * @param assets - path to assets
 * @param userID - id of the user
 * @param projectID - id of the project
 */
function aPreview(assets, userID, projectID) {

	// Produce PDF url
	var pdfUrl = assets + '/datas/users/' + userID + '/' + projectID + '/preview/main.pdf?' + new Date().getTime();

	// If object exists
	if (objectExists(pdfUrl)) {
		$('#pdfPreview').html('<object data="' + pdfUrl + '"type="application/pdf" ' + 'id="pdfObject"></object>');
		$('#resultsPreview').fadeOut(100);
		$('#graphPreview').fadeOut(100);
		$('#errorCard').fadeOut(100);
		$('#pdfPreview').fadeIn(1);
	}

	// If not exist show warning message
	else
	{
		$('#pdfPreview').fadeOut(1);
		$('#graphPreview').fadeOut(100);
		$('#nothingCard').fadeOut(1);
		$('#resultsPreview').fadeIn(1);
		$('#errorCard').fadeIn(1);
	}
}

/**
 * Function to change favorite state
 * Sends the data to controller function and get color as result
 * @param index - path to index
 * @param user - id of the user
 */
function aUpdateFavorite(index, user) {

	$.ajax
	({
		url: index + '/classe/updateFavorite/' + user,
		dataType: 'json',
		type: 'GET',
		error: function() {alert("Can't get favorite data!")},
		success: function(data)
		{
			if (data.result == true) {
				if (data.state == true)
					$('#fav_' + user).css('color', 'red');
				else
					$('#fav_' + user).css('color', 'black');
			}
		}
	});
}

/**
 * Function to produce the graph preview in front
 * @param userID - id of the user
 * @param index - path to index
 * @param timeStuck - stucks for time point
 * @param charStuck - stucks for char point
 */
function aGraph(userID, index, timeStuck, charStuck) {

	$('#resultsPreview').fadeOut(1);
	$('#pdfPreview').fadeOut(1);
	$('#errorCard').fadeOut(1);
	$('#graphPreview').fadeIn(100);
	showG(userID, index, timeStuck, charStuck);
}


/**
 * Create the graph preview elements
 * @param User - id of the user
 * @param index - path to index
 * @param timeStuck - stucks for time point
 * @param charStuck - stucks for char point
 */
function showG (User, index, timeStuck, charStuck)
{
	$.ajax
	({
		type: 'get',
		url: index + '/classe/getGraphData/' + User,
		data: '',
		error: function() {alert("Can't get graph data!")},
		success: function (data) {

			// Get data from the controller
			data = JSON.parse(data);

			// Create chart parts
			CreateChartData(data);
			CreateChartDetail(data,timeStuck,charStuck);

			// Show hidden elements
			$('#graphLegend').fadeIn();
			$('#graph').fadeIn();
			$('#graphDetails').fadeIn();
		}
	});
}

/**
 * Funtion to fill the table with the results
 * @param data - input data
 * @param timeStuck - stucks for time point
 * @param charStuck - stucks for char point
 */
function CreateChartDetail(data,timeStuck,charStuck)
{
	$('#graphTable').dataTable().fnClearTable();
	for (var i = 0; i < 13; i++) {
		$('#graphTable').dataTable().fnAddData([
			(i+1),
			Math.round(data[i].time_stucks * timeStuck / 1000 / 60),
			(data[i].char_stucks * charStuck),
			 data[i].done_stucks
			]
		);
	}
}

/**
 * Function to fill the graph with the data and create it
 * @param data - input data for the graph which we get from ajax
 */
function CreateChartData(data)
{
	// Create labels and maximums
	var Labels = [];
	var TimeMax = 1;
	var TextMax = 1;
	for (var i = 1; i < 14; i++) {Labels.push(i);}

	// Find the local maximums
	for (var i = 0; i < 13; i++) {
		if (data[i].time_stucks > TimeMax) TimeMax = data[i].time_stucks;
		if (data[i].char_stucks > TextMax) TextMax = data[i].char_stucks;
	}

	// Apply maximums to the results of time
	var TimeSeries = [];
	var TextSeries = [];
	for (var x = 0; x < 13; x++) {
		if (data.length < x + 1) TimeSeries.push(0);
		else TimeSeries.push(data[x].time_stucks / TimeMax);
	}

	// Apply maximums to the results of characters
	for (var y = 0; y < 13; y++) {
		if (data.length < y+1) TextSeries.push(0);
		else TextSeries.push(data[y].char_stucks/TextMax);
	}

	// Get done results with 100 as maximum
	var DoneSeries = [];
	for (var z = 0; z < 13; z++) {
		if (data.length < z+1) DoneSeries.push(0);
		else DoneSeries.push(data[z].done_stucks/100);
	}

	// Create the new chart as Chartist
	var chart = new Chartist.Line('.ct-chart', {
			labels: Labels,
			series:[TimeSeries,TextSeries,DoneSeries]
		}, {
			low: 0, showArea: true, showPoint: false, fullWidth: true,
			width: 620, height: 200, axisY: {offset: -10}
		});

	// Append element
	chart.on('draw', function(data)  {

		if(data.type === 'line' || data.type === 'area') {
			data.element.animate({
				d: {begin: 400 * data.index,
					dur: 400,
					from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
					to: data.path.clone().stringify(),
					easing: Chartist.Svg.Easing.easeOutQuint
				}
			});
		}
	});
}

/**
 * Function to open the modal when we need to import users
 */
function aCreateUsers() {$('#mAddUsers').openModal();}

/**
 * Function to send the modal form for importing users
 */
function aProcess() {$( "#fClasse" ).submit();}

/**
 * Check if object exists
 * @param url - url for the object
 * @returns {boolean} - result
 */
function objectExists(url){

	var http = new XMLHttpRequest();
	http.open('HEAD', url, false);
	http.send();
	return http.status != 404;
}

/**
 * Open modal for pictures
 */
function aHelp() {
	preventDefault();
	$('#mHelp').openModal();
}

/**
 * Update help button with event
 */
$( "#showHelp" ).click(function( event ) {
	event.preventDefault();
	$('#mHelp').openModal();
});

/**
 * Function to regenerate the cache of classe
 * @param index - string : path to index
 */
function regenerate(index, translation) {

	$.ajax ({
		type: 'get',
		url: index + '/classe/regenerate/',
		data: '',
		error: function() {alert("Can't regenerate!")},
		success: function (data) {
			alert(translation);
			setTimeout(function () {location.reload();}, 500);
		}
	});
}