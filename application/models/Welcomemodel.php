<?php

/**
 * Class Welcomemodel
 */
class Welcomemodel extends CI_Model
{
	/**
     * Welcomemodel constructor.
     */
    function __construct()
    {
        parent::__construct();
	    $this->load->model("logmodel");
    }

	/**
     * Get user for login and set session
     * @param $postData - array : login post data
     */
    function doLogin($postData){

        $userData = $this->getUser($postData);
        $this->ss->set_userdata($userData);
    }

	/**
     * Get user from database
     * @param $loginData - array : login and password
     * @return array|bool : data for user or false
     */
    private function getUser($loginData){

        $sha = hash('sha256', $loginData['iPassword']);
        $query = $this->db->get_where('users', ['login' => $loginData['iLogin'], 'password' => $sha]);
        $resultQuery = current($query->result());

        if ($resultQuery == false) return false;
        return (array)$resultQuery;
    }

	/**
	 * Get all users depending on project
	 * @param $projectID - int : project id
	 * @return array : data for users
	 */
	public function getUsersByProject($projectID) {

		// Get users for project
    	$this->db->where('project', $projectID);
	    $query = $this->db->get('user_project');
	    $result = [];

		// Create array for pair function
	    foreach ($query->result() as $row) {

		    $userData = $this->getUserById($row->user);
		    if ($userData->permissions == 'S') {
			    $result[$row->id]['user'] = $row->user;
			    $result[$row->id]['team'] = $row->team;
			    $result[$row->id]['toRew1'] = null;
			    $result[$row->id]['toRew2'] = null;
			    $result[$row->id]['beFeeded'] = 0;
		    }
	    }

	    // Return the default
	    return $result;
    }

	/**
	 * Get all project for user
	 * @param $userID - integer : id of the user
	 * @return array - the projects
	 */
	public function getProjectsByUser($userID) {

		// Get projects from DB
		$this->db->select('project');
		$this->db->where('user', $userID);
		$query = $this->db->get('user_project');

		// Create and return the result
		$result = [];
		foreach ($query->result() as $row) array_push($result, (array)$row);
		return $result;
	}

	/**
	 * Get users depending on project
	 * @param $projectID - integer : id of the project
	 * @return array - the users data
	 */
	public function getUsersInfoByProject($projectID) {

		// Get all users
		$this->db->where('project', $projectID);
		$query = $this->db->get('user_project');
		$result = [];

		// Get user info
		foreach ($query->result() as $row) {
			$this->db->where('id', $row->user);
			$user = current($this->db->get('users')->result());

			// Get team info
			$this->db->where('team', $user->team);
			$team = current($this->db->get('team_info')->result());
			array_push($result,['user' => (array)$user, 'team' => (array)$team]);
		}

		// Return the data
		return $result;
	}

	/**
	 * Get info about user by ID
	 * @param $userID - integer : user id
	 * @return array - the data for user
	 */
	private function getUserById ($userID) {

		// Look for user
	    $this->db->where('id', $userID);
	    $userData = $this->db->get('users')->result();

		// If not found
		if (empty($userData)) {
			$this->logmodel->lE("Can't find user with ID:" . $userID);
			return [];
		}

		// Return correct data
	    return current($userData);
    }

	/**
	 * Function to select the project in list
	 * @param $userData - array : data for the user
	 * @param $project - integer : project ID
	 */
	public function selectProject($userData, $project) {

		// Save selected project into seesion
	    $this->ss->set_userdata('selectedProject', $project);

		// Get info from user_project
	    $team = $this->db->get_where('user_project', ['user' => $userData['id'], 'project' => $project])->result();
	    if (!empty($team)) {

	    	// Get info from team_info
		    $teamInfo = current($this->db->get_where('team_info', ['id' => current($team)->team])->result());
		    if (!empty($teamInfo)) {

		    	// Save additional data
			    $this->ss->set_userdata('teamName', $teamInfo->name);
			    $this->ss->set_userdata('teamProject',  $teamInfo->project);
		    }
	    }
    }

	/**
	 * Function to generate commit info
	 * @return string - the commit info
	 */
	public function getCommit() {

		// Run the git command
    	exec('cd ' . FCPATH . ' && ' . GITPATH . ' log --pretty="%ci" -n1 HEAD', $output);

		// Parse the output and return
	    if (!empty($output)) {
	        preg_match("/(.*\s.*)\s+.*/", current($output), $report);
	        return $report[1];
	    }

	    // Return the default
	    else return "N/A";
    }

	/**
	 * Function to get team members for user
	 * @param $userData - array : logged user data
	 * @return array - team members data
	 */
	public function getTeamMembersByUser($userData) {

    	// Get the team
	    $this->db->where('user', $userData['id']);
	    $this->db->where('project', $userData['selectedProject']);
	    $team = current($this->db->get('user_project')->result())->team;

		// Get the members
	    $this->db->where('team', $team);
	    $this->db->where('project', $userData['selectedProject']);
	    $query = $this->db->get('user_project');

		// Get data for each member
	    $result = [];
	    foreach ($query->result() as $row) {

	    	// Save the data
		    $this->db->where('id', $row->user);
		    $user = current($this->db->get('users')->result());
		    array_push($result, ['id' => $row->user, 'login' => $user->login]);
	    }

	    // Return the result
	    return $result;
	}

	/**
	 * Function to save the settings
	 * @param $userData - array : data for the logged user
	 * @param $postData - array : data from the post
	 * @return bool - the result of function
	 */
	public function saveSettings($userData, $postData) {

		// Check input data - no error (no data selected)
		if (empty($userData) || empty($postData)) {
			$this->logmodel->lE("Called settings but no input:");
			return false;
		}

		// Update the visibility if student with project
		if ($userData['permissions'] == 'S' && !empty($userData['selectedProject']) && isset($postData['visibility'])) {
				$this->db->where('user', $userData['id']);
				$this->db->where('project', $userData['selectedProject']);
				$this->db->update('user_project', ['visible' => $postData['visibility']]);
		}

		// Update the language for everyone
		if (isset($postData['language']) && isset($userData['id'])) {
			if (!empty($postData['language'])) {
				$this->db->where('id', $userData['id']);
				$this->db->update('users', ['language' => $postData['language']]);
			}
		}

		// Return the default
		return true;
	}
}