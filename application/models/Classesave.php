<?php

/**
 * Class ClasseSave
 */
class ClasseSave extends CI_Model
{
	/**
	 * ClasseSave constructor.
	 * Also loads the email library for sending mails
	 */
	function __construct() {
		parent::__construct();
		$this->load->library('email');
		$this->load->model("logmodel");
	}

	/**
	 * Function to save teams into database
	 * @param $userData - array : data of the logged user
	 * @param $teamsCSV - CSV : CSV with teams
	 * @return boolean - the result of the operation
	 */
	public function saveTeams($userData, $teamsCSV) {

		// If we don't have any data just skip else process
		if (empty($teamsCSV)) return true;
		$data = str_getcsv($teamsCSV, "\n");

		// From the data parse every row
		foreach($data as $row) {

			// Get parameters and check if everything is all right
			$row = str_getcsv($row, ",");
			if (!isset($row[0]) || !isset($row[1]) || !isset($row[2]) ||
				empty($row[0]) || empty($row[1]) || empty($row[2])) {
				$this->logmodel->lE("No team data found:" . implode("|", $row));
				return false;
			}

			// Prepare the row for the database operations
			$dataArray = [
				'team'        => $row[0],
				'name'        => $row[1],
				'project'     => $row[2],
				'ref_project' => $userData['selectedProject']
			];

			// Check if team already exists
			$this->db->where('team', $row[0]);
			$this->db->where('ref_project', $userData['selectedProject']);

			// If exists update the informations
			if ($this->db->get('team_info')->num_rows() > 0) {
				$this->db->where('team', $row[0]);
				$this->db->where('ref_project', $userData['selectedProject']);
				$this->db->update('team_info', $dataArray);
			}

			// If not exists insert into database
			else {
				$this->db->insert('team_info', $dataArray);
				if ($this->db->affected_rows() < 1) {
					$this->logmodel->lE("Can't insert:" . implode("|", $dataArray));
					return false;
				}
			}
		}

		// Rebuild cache
		$this->db->cache_delete('classe', 'index');
		return true;
	}

	/**
	 * Function to save the users into database
	 * @param $userData - array : data for the logged user
	 * @param $usersCSV - CSV : data for the users as CSV
	 * @return boolean - the result of the operation
	 */
	public function saveUsers($userData, $usersCSV) {

		// If no data skip else process
		if (empty($usersCSV)) return true;
		$data = str_getcsv($usersCSV, "\n");

		// For each row of datas
		foreach($data as $row) {

			// Get the datas for user and check them
			$row = str_getcsv($row, ",");
			if (!isset($row[0]) || empty($row[0]) || !isset($row[1]) || empty($row[1])) {
				$this->logmodel->lE("No student data found:" . implode("|", $row));
				return false;
			}

			// Prepare the array for the DB operations
			$dataArray = [
				'login'       => $row[0],
				'password'    => "",
				'permissions' => "S"
			];

			// Check if already exists in DB
			$this->db->where('login', $row[0]);
			$this->db->where('permissions', 'S');
			$user = $this->db->get('users')->result();

			// If exists then update into DB
			if (!empty($user)) {
				$this->db->where('id', current($user)->id);
				$this->db->update('users', $dataArray);
				$userID =  current($user)->id;
			}

			// If not exists create and get the new ID
			else {
				$this->db->insert('users', $dataArray);
				$userID = $this->db->insert_id();
			}

			// Try to look for the team for the user
			$this->db->where('team', $row[1]);
			$this->db->where('ref_project', $userData['selectedProject']);
			$team = $this->db->get('team_info')->result();

			// Check the result of the team
			if (empty($team)) {
				$this->logmodel->lE("Team for the user not found:" . $row[1]);
				return false;
			}

			// Try to look for the project
			$this->db->where('id', $userData['selectedProject']);
			$project = $this->db->get('projects')->result();

			// Check the result of the team
			if (empty($project)) {
				$this->logmodel->lE("Project for the user not found:" . $userData['selectedProject']);
				return false;
			}

			// Prepare the array for the database
			$dataArray2 = [
				'user'    => $userID,
				'project' => $userData['selectedProject'],
				'team'    => current($team)->id,
				'visible' => current($project)->visible
			];

			// Look if there is any record in users project table
			$this->db->where('user', $userID);
			$this->db->where('project', $userData['selectedProject']);

			// If already exists then update
			if ($this->db->get('user_project')->num_rows() > 0) {
				$this->db->where('user', $userID);
				$this->db->where('project', $userData['selectedProject']);
				$this->db->update('user_project', $dataArray2);
			}

			// If not exists then create the new record
			else {
				$this->db->insert('user_project', $dataArray2);
				if ($this->db->affected_rows() < 1) {
					$this->logmodel->lE("Can't insert" . implode("|", $dataArray2));
					return false;
				}
			}
		}

		// Rebuild cache
		$this->db->cache_delete('classe', 'index');
		return true;
	}

	/**
	 * The function to inform users about creation
	 * @param $usersCSV - CSV : array with the CSV user data
	 * @return bool - result of the operation
	 */
	public function informUsers($usersCSV) {

		// If no data skip else parse CSV file
		if (empty($usersCSV)) return true;
		$data = str_getcsv($usersCSV, "\n");

		// For each data in CSV
		foreach($data as $row) {

			// Get the data and check them
			$row = str_getcsv($row, ",");
			if (!isset($row[0]) || empty($row[0])) {
				$this->logmodel->lW("No student data found to inform:" . implode("|", $row));
				return false;
			}

			// Generate the password, save it and send the user email
			$pass = $this->GeneratePassword(8);
			if (!$this->savePassword($row[0], hash("sha256", $pass))) return false;
			if (!$this->SendUserMail($row[0], $pass)) return false;
		}
		return true;
	}

	/**
	 * The function to save the password into DB
	 * @param $login - string : the login name
	 * @param $pass - string : the password sha
	 * @return boolean - the result of function
	 */
	private function savePassword($login, $pass) {
		$this->db->where('login', $login);
		$this->db->where('permissions', 'S');
		$this->db->update('users', ['password' => $pass]);
		if ($this->db->affected_rows() < 1) {
			$this->logmodel->lE("Can't save password for the user:" . $login . '|' . $pass);
			return false;
		}
		return true;
	}

	/** The function to create random string as password
	 * @param $lengh - integer : the lenght of generated string
	 * @return string - the password
	 */
	private function GeneratePassword($lengh)
	{
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyz_?!-0123456789";
		$charArray = str_split($chars);

		for($i = 0; $i < $lengh; $i++)
		{
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}

	/**
	 * Function to send the user mail
	 * @param $login - string : informations about the user
	 * @param $pass - string : password for this user
	 * @return boolean - the result of the function
	 */
	private function SendUserMail($login, $pass)
	{
		// Create the informations in header
		$subject = '[DEV] Vitajte v ProjektCreator';
		$sendto = $login . "@stud.fit.vutbr.cz";

		// Put datas into template
		$this->sm->assign('charset', strtolower(config_item('charset')));
		$this->sm->assign('subject', html_escape($subject));
		$this->sm->assign('login', $login);
		$this->sm->assign('password', $pass);
		$body = $this->sm->fetch('mails/welcome.tpl');

		// Send the email
		$Result = $this->email
			->from('noreply@me.com')
			->reply_to('noreply@me.com')
			->to('dlxxko@gmail.com')
			->subject($subject)
			->message($body)
			->send();

		// Check the result and return
		if (!$Result) {
			$this->logmodel->lE("Can't send the user email:" . $login);
			return false;
		}
		else return true;
	}

	/**
	 * The function to save the information about favorite
	 * @param $userData - array : the data of logged user
	 * @param $user - integer : the id of the favorite
	 * @return array - the array result of function
	 */
	public function updateFavorite($userData, $user) {

		// Try to find the favorite user in DB
		$this->db->where('user', $userData['id']);
		$this->db->where('project', $userData['selectedProject']);
		$this->db->where('favorite_user', $user);
		$favorite = $this->db->get('student_favorites')->result();

		// If not exists create the record and return the result
		if (empty($favorite)) {
			$this->db->insert('student_favorites', [
				'user'          => $userData['id'],
				'project'       => $userData['selectedProject'],
				'favorite_user' => $user
			]);
			if ($this->db->affected_rows() < 1) {
				$this->logmodel->lE("Can't insert favorite:" . $userData['id'] . '|' . $user);
				return ['result' => false, 'state' => true];
			}
			return ['result' => true, 'state' => true];
		}

		// If exists delete it and send the result
		else {
			$this->db->delete('student_favorites', ['id' => current($favorite)->id]);
			if ($this->db->affected_rows() < 1) {
				$this->logmodel->lE("Can't delete favorite:" . current($favorite)->id);
				return ['result' => false, 'state' => false];
			}
			return ['result' => true, 'state' => false];
		}
	}
}