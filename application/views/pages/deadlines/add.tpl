<!-- ELEMENT TO ADD NEW DEADLINE -->
<div class="row">
    {if empty($deadlinesdata) && $project.locked == 1}
        <h2 class="center-align">{ci_language line="There isn't any deadline and project locked!"}</h2>
        <h3 class="center-align">{ci_language line="Please unlock the project to add deadline."}</h3>
    {else}
        <ul class="collapsible" data-collapsible="accordion" id="deadlineAppendContainer">
            {if $project.locked == 0}
            <li>
                <div class="collapsible-header">
                    <span onclick="aCreateDeadline(0, '{ci_language line="New deadline"}', 'U', '', '', '')">
                        <i class="material-icons">plus_one</i>
                        {ci_language line="Add deadline"}
                    </span>
                    <span class="secondary-content" onclick="aImportChapter()"></span>
                </div>
            </li>
            {/if}
        </ul>
    {/if}
</div>
