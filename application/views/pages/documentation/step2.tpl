<div id="step2" class="step2Container">

    <!-- KEYWORDS -->
    <div id="titleBox">
        {ci_language line="Keywords"}: {literal}{TITLE}, {AUTHOR}, {INCLUDES}{/literal}
    </div>

    <!-- INPUT -->
    <div class="row baseContainer">
        <div class="input-field col s12 baseInputContainer">
            <textarea id="iProjectBase" class="materialize-textarea scrollArea projectBase"
                name="iProjectBase">{if !empty($project.base)}{$project.base}{else}{$smarty.const.DEFAULT_BASE}{/if}</textarea>
            <label for="iProjectBase">{ci_language line="Project base"}</label>
        </div>
    </div>

</div>