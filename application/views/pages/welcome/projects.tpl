<div id="admin" class="col s12">
    <div class="card material-table">

        <!-- TABLE HEADER CONTENT -->
        <div class="table-header">
            <span class="table-title red-text darken-4"><b>{ci_language line="Project list"}</b></span>
            {if 'login'|array_key_exists:$user}
                {if $user.permissions == 'A'}
                    <a class="waves-effect waves-light btn" id="newProject"
                       onclick="addNewProject('{$index}', '{$user.login}', '{ci_language line="Enter"}')">
                        {ci_language line="New project"}
                    </a>
                {/if}
            {/if}
            <div class="actions">
                <a href="#" class="search-toggle waves-effect btn-flat nopadding">
                    <i class="material-icons">search</i>
                </a>
            </div>
        </div>

        <!-- PROJECT TABLE -->
        <table id="tProjects">
            <thead>
                <tr>
                    <th>{ci_language line="Project name"}</th>
                    <th>{ci_language line="Teacher"}</th>
                    <th>{ci_language line="Created"}</th>
                    <th>{ci_language line="Start"}</th>
                    <th>{ci_language line="Students"}</th>
                    {if isset($user.id)}
                        <th class="right-align paddingFunctionTitle">{ci_language line="Functions"}</th>
                    {/if}
                </tr>
            </thead>
            <tbody>
            {foreach from=$projectsdata item=project}
                <tr>
                    <td class="nameFontSize"><b>{$project.name}</b></td>
                    <td>{$project.login}</td>
                    <td>{$project.created}</td>
                    <td>{$project.started}</td>
                    <td>{$project.students}</td>
                    {if isset($user.id)}
                    <td>

                        <!-- FOR ADMIN -->
                        {if $project.user == $user.id}
                        <a href="{$index}/welcome/enterProject/{$project.id}">
                            <i class="material-icons enterIcon">open_in_browser</i>
                            <span class="enterFont">{ci_language line="Enter"}</span>
                        </a>
                        {/if}

                        <!-- FOR STUDENTS -->
                        {assign var="found" value=false}
                        {if $projects != false}
                        {foreach from=$projects item=projectx}
                            {if $projectx.project == $project.id}
                                {$found = true}
                                <a href="{$index}/welcome/enterProject/{$project.id}">
                                    <i class="material-icons enterIcon">open_in_browser</i>
                                    <span class="enterFont">{ci_language line="Enter"}</span>
                                </a>
                            {/if}
                        {/foreach}
                        {/if}

                    </td>
                    {/if}
                </tr>
            {/foreach}
            </tbody>
        </table>

    </div>
</div>