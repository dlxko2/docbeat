<?php

/**
 * Class ReviewsGet
 */
class ReviewsGet extends CI_Model
{
	/**
	 * ReviewsGet constructor.
	 */
	function __construct(){
		parent::__construct();
		$this->load->model("logmodel");
	}

	/**
	 * Get all questions depending on project
	 * @param $projectID - int : id of the project
	 * @return array : questions data array
	 */
	public function getQuestionsByProject($projectID) {

		// Get questions for selected project
		$this->db->where('project', $projectID);
		$this->db->order_by('type', 'DESC');
		$query = $this->db->get('questions');
		$result = [];

		// Return result or empty result
		foreach ($query->result() as $row)
			array_push($result,(array)$row);

		// Return the default
		return $result;
	}

	/**
	 * Get users who review selected author
	 * @param $userID - int : author id
	 * @return array : data for reviewers
	 */
	public function getTargetsByUser($userID) {

		// Get connections where author is user
		$this->db->where('author', $userID);
		$query = $this->db->get('student_connect');
		$result = [];

		// Get datas for this authors
		foreach ($query->result() as $row) {

			// If nobody just continue
			if (isset($row->reviewer) && $row->reviewer == 0)
				continue;

			// Try to get datas from users or return error
			$userData = $this->db->where('id', $row->reviewer)->get('users')->result();
			if (empty($userData)) {
				$this->logmodel->lE("Can't find data for user in connect:" . $row->reviewer);
				return [];
			}

			// Append data for the result
			array_push($result, [
				'id' => current($userData)->id,
				'login' => current($userData)->login,
				'connection' => $row->id]);
		}

		// Return the default
		return $result;
	}

	/**
	 * Get users who person review
	 * @param $userID - int : id of the reviewer
	 * @return array|boolean : data for the authors
	 */
	public function getReviewersByUser($userID) {

		// Try to get users from the connections where user is reviewer
		$this->db->where('reviewer', $userID);
		$query = $this->db->get('student_connect');
		$result = [];

		// Get datas for reviewers
		foreach ($query->result() as $row) {

			// If nobody just continue
			if (isset($row->author) && $row->author == 0)
				continue;

			// Try to get datas from users or return error
			$userData = $this->db->where('id', $row->author)->get('users')->result();
			if (empty($userData)) {
				$this->logmodel->lE("Can't find data for user in connect:" . $row->author);
				return false;
			}

			// Append data for the result
			array_push($result, [
				'id' => current($userData)->id,
				'login' => current($userData)->login,
				'connection' => $row->id]);
		}

		// Return the default
		return $result;
	}

	/**
	 * Function to get question by ID
	 * @param $questionID - integer : question id
	 * @return array|false - question data
	 */
	public function getQuestionByID($questionID) {

		// Try to get data
		$this->db->where('id', $questionID);
		$result = $this->db->get('questions')->result();

		// Check result
		if (empty($result)) {
			$this->logmodel->lE("Question not found:" . $questionID);
			return false;
		}

		// Return result
		return (array)$result;
	}
}