<div id="mLogin" class="modal bottom-sheet">

    <!-- LOGIN FORM -->
    <div class="modal-content">
        <h4>{ci_language line="Please log in"}</h4>
        <form role="form" id="form1" method="post" enctype="multipart/form-data"
              accept-charset="utf-8" action="{$index}/welcome/login">
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="iLogin" type="text" class="validate" name="iLogin">
                    <label for="iLogin">{ci_language line="Login name"}</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">vpn_key</i>
                    <input id="iPassword" type="password" class="validate" name="iPassword">
                    <label for="iPassword">{ci_language line="Password"}</label>
                </div>
            </div>
        </form>
    </div>

    <!-- SEND BUTTON -->
    <div class="modal-footer">
        <a class="waves-effect waves-light btn" id="loginButton">
            <i class="material-icons left">done</i>{ci_language line="Submit"}
        </a>
    </div>
</div>