<?php

/**
 * Class ReviewsSave
 */
class ReviewsSave extends CI_Model
{
	/**
	 * ReviewsSave constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->load->model("logmodel");
	}

	/**
	 * Save reviews into database
	 * @param $postData - array : data from the post
	 * @param $projectID - int : id of the project
	 * @return boolean - the result of function
	 */
	public function saveReviews($postData, $projectID) {

		// Check if any chapter in list
		if (!array_key_exists('iID', $postData)){
			$this->db->delete('questions', ['project' => $projectID]);
			return true;
		}

		// Delete existing old chapters
		$this->db->where_not_in('id', $postData["iID"]);
		$this->db->delete('questions', ['project' => $projectID]);

		// For every question
		foreach ($postData['iID'] as $key => $iid) {

			// Define the type
			if (isset($postData['iType' . $key])) $type = $postData['iType' . $key];
			else $type = 'U';

			// Look for quantity
			if (isset($postData['iQuantity'][$key])) $quantity = $postData['iQuantity'][$key];
			else $quantity = 0;

			// Produce the array
			$arrayData = [
				'type'     => $type,
				'project'  => $projectID,
				'content'  => $postData['iAreaContent'][$key],
				'quality'  => $postData['iQuality'][$key],
				'quantity' => $quantity
			];

			// Decide if replace or insert
			if ($postData['iID'][$key] == 0)
				$this->db->insert('questions', $arrayData);
			else {
				$this->db->where('id', $postData["iID"][$key]);
				$this->db->update('questions', $arrayData);
			}
		}

		// Return the default
		return true;
	}
}