/**
 * Function to get chapter
 * @param index - path to index
 */
function aGetChapter(index, actual = 0) {

	// Get the selected chapter
	var chapter = 0;
	if (actual == 0) chapter = $('#chapterSelector').val();
	else chapter = actual;

	// If any chapter number
	if (chapter > 0) {
		$.ajax
		({
			url: index + '/editor/getChapter/' + chapter,
			dataType: 'json',
			type: 'GET',
			error: function () {alert("Can't get chapter content!")},

			// Update the editor and display toast
			success: function (data) {
				editor.setValue(data.content);
				Materialize.toast('Chapter loaded!', 3000, 'rounded');
			}
		});
	}

	// If literature
	else if (chapter == -1) {
		$.ajax
		({
			url: index + '/editor/getLiterature/',
			dataType: 'json',
			type: 'GET',
			error: function () {alert("Can't get literature content!")},

			// Update the editor with literature
			success: function (data) {
				editor.setValue(data.content);
				Materialize.toast('Literature loaded!', 3000, 'rounded');
			}
		});
	}
}

/**
 * Function to save the chapter
 * @param index - path to index
 * @param text - translation
 */
function aSaveChapter(index, text, previous = 0) {

	// If previous
	var chapter = 0;
	if (previous != 0) chapter = previous;
	else chapter = $('#chapterSelector').val();

	var chapterName = $("#chapterSelector").find("option[value='" + chapter + "']").text();
	var content = editor.getValue();

	// Gey the url
	if (chapter > 0) var url = index + '/editor/saveChapter/' + chapter;
	else if (chapter == -1) var url = index + '/editor/saveLiterature/';

	// If no chapter please select
	if (chapter == null) alert('Please select the chapter!');

	// Send request to save the chapter
	else {
		$.post(url, { 'content': content });
		getChapterDone(index, text);
		Materialize.toast(chapterName + ' saved!', 3000, 'rounded');
	}
}

/**
 * Function to make the preview
 * @param index - path to index
 * @param assets - path to assets
 * @param userID - id of the user
 * @param projectID - id of the project
 * @param type - type of generation
 */
function aPreview(index, assets, userID, projectID, type, text) {

	// Save the chapter first
	aSaveChapter(index, text);

	// Delay preview waiting for save
	setTimeout(function() {

		// Get the right PDF url
		var pdfUrl = assets + '/datas/users/' + userID + '/' + projectID + '/preview/main.pdf?' + new Date().getTime();

		$.ajax
		({
			url: index + '/editor/preview/' + type,
			dataType: 'json',
			type: 'GET',
			error: function () {alert("Can't get result of preview!")},
			success: function (data) {

				// Check if datas are correct or error
				if (data.result == true) {

					// If preview or bibtext
					if (type == 'P') {
						$('#pdfPreview').html('<object data="' + pdfUrl + '"type="application/pdf" ' + 'id="pdfObject"></object>');
						$('#resultPreview').fadeOut(1);
						$('#pdfPreview').fadeIn(1);
						Materialize.toast('Preview reloaded!', 3000, 'rounded');
					}
					else Materialize.toast('BibTex reloaded!', 3000, 'rounded');
				}
				else {
					$('#nothingCard').fadeOut(1);
					$('#pdfPreview').fadeOut(1);
					$('#errorCard').fadeIn(1);
					$('#resultPreview').fadeIn(1);
					$('#errorText').html(data.content);
				}
			}
		});

	}, 1000);
}

/**
 * Function to get chapter done
 * @param index - path to index
 * @param text - translation
 */
function getChapterDone(index, text) {

	// Get the chapter
	var chapter = $('#chapterSelector').val();

	$.ajax
	({
		url: index + '/editor/getChapterDone/' + chapter,
		dataType: 'json',
		type: 'GET',
		error: function() {alert("Can't get chapter percent done!")},
		success: function(data)
		{
			// Get the percents
			var percent = 0;
			if (data.done > 100) percent = 100;
			else percent = data.done;

			// Update the progress bar
			$('#prog2').css('width', percent + '%');
			$('#prog2Text').text(percent + '% ' + text);
		}
	});
}

/**
 * Function to get the finishing
 * @param index - path to index
 * @param text - translation
 */
function getFinishingDone(index, text) {

	// Get the chapter
	var chapter = $('#chapterSelector').val();

	$.ajax
	({
		url: index + '/editor/getFinishingDone/' + chapter,
		dataType: 'json',
		type: 'GET',
		error: function() {alert("Can't get chapter finishing done!")},
		success: function(data)
		{
			// Get the percents
			var percent = 0;
			if (data.finishing > 100) percent = 100;
			else percent = data.finishing;

			// Update the element
			$('#prog1').css('width', percent + '%');
			$('#prog1Text').text(percent + '% ' + text);
		}
	});
}

/**
 * Function to save the stamp
 * @param index - path to index
 * @param type - stamp type
 */
function saveStamp(index, type) {

	// Get blured
	var blured = $('#activityStore').val();
	if (blured == 'inactive') return;

	$.ajax
	({
		url: index + '/editor/saveStamp/' + type,
		dataType: 'json',
		type: 'GET',
		error: function() {console.log("Can't save stuck data!")},
		success: function(data) {
			if (data.result != true) console.log("Can't save stuck!");
		}
	});
}

/**
 * Function to get the lead
 * @param index - path to index
 * @param text - translation
 */
function getLead(index, text) {

	$.ajax
	({
		url: index + '/editor/getLead/',
		dataType: 'json',
		type: 'GET',
		error: function() {console.log("Can't get lead!")},
		success: function(data)
		{
			// Get the percents
			var percent = 0;
			if (data.lead > 100) percent = 100;
			else percent = data.lead;

			// Update the element
			$('#prog3').css('width', percent + '%');
			$('#prog3Text').text(percent + '% ' + text);
		}
	});
}

/**
 * Open modal for pictures
 */
function aPictures() {
	$('#mPictures').openModal();
}

function aUpload() {
	$('#mUpload').openModal();
}

/**
 * Function to upload the image
 */
function aSavePictures() {
	$( "#fPictures" ).submit();
}

/**
 * Function to remove the image
 * @param index - path to index
 */
function aRemoveImage(index) {

	// Get the element with image
	var element = $("li[style*='opacity: 1']");
	var child = element.children();
	var image = child.css( "background-image");

	// Get the image name
	var pattern = "url.*\\/(.*)\\)";
	var match = image.match(pattern);
	var picture = match[1];

	// Remove the image
	var url = index + '/editor/removeImage/';
	$.post(url, { 'image': picture });
	location.reload();
}

/**
 * Function to insert the picture
 */
function aPublishPicture() {

	// Get the picture
	var a = $("li[style*='opacity: 1']").children().css( "background-image");
	var pattern = "url.*\\/(.*)\\)";
	var match = a.match(pattern);
	var picture = match[1];

	// Get the data to append
	var appendData =
		"\n\\begin{figure} [H] \n" +
		"\\centering \n" +
		"\\includegraphics[width=\\textwidth]{" + picture + "} \n" +
		"\\caption{Popis} \n" +
		"\\label{pic:nazov} \n" +
		"\\end{figure} \n";

	// Append into editor
	editor.replaceRange(appendData,{line: Infinity});
	$('#mPictures').closeModal();
	Materialize.toast('Picture inserted!', 3000, 'rounded');
}